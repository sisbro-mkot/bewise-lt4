<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Struktur extends Model
{
    protected $connection = 'dbhrm';
    protected $table = 'ref_struktur';

    public function pegawai()
    {
        return $this->hasOne(PegawaiSQL::class, 's_kd_jabdetail', 's_kd_jab_detail')->select(['niplama', 'jabatan', 'nip', 'nipbaru']);
    }
}
