<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\adminpemda\Role;
use App\Models\adminpemda\User;
use DB; 
use App\Models\adminpemda\UserHrm;
use App\Models\utils\Functions;
 
class AuthCtrl extends Controller
{
    //
    public function __construct(){
        // $this->middleware('guest:web')->except('logout');
        // $this->middleware('guest:web1')->except('logout');
    }
    public function getLogin()
    {   
        $roles = DB::table('tbl_role')->orderBy('id', 'desc')->get();
    	return view('auth.login_fresh', compact('roles'));
    }

    public function postLogin(Request $request)
    {
        $ldap = Functions::LoginLDAP($request->get('username'),$request->get('password'));

        if($ldap!=null){
            $userFromLDAP = UserHrm::where('user_nip',$ldap->niplama)->first();
            // dd($request->get('peran'));
            if($request->get('peran')!='0'){
                $userFromLDAP->role_id = $request->get('peran');
                $userFromLDAP->save();
            }

            // Auth::guard()->login($userFromLDAP, true);
            Auth::login($userFromLDAP, true);
//            if (Auth::user()->hasRole('Admin BPKP')) {
            if (Auth::user()->role_id == 1|Auth::user()->role_id == 2|Auth::user()->role_id == 3|Auth::user()->role_id == 8) {
              return redirect()->route('peta');
            }

//            if (Auth::user()->hasRole('User BPKP 2')) {
            if (Auth::user()->role_id == 4|Auth::user()->role_id == 5|Auth::user()->role_id == 6|Auth::user()->role_id == 7|Auth::user()->role_id == 10) {
                return redirect()->route('heatmapunit');
            }


        }else{
            if (Auth::guard('web1')->attempt(['username' => $request['username'], 'password' => $request['password']])) {
//                if (Auth::guard('web1')->user()->hasRole('Admin BPKP')) {
                if (Auth::guard('web1')->user()->hasRole(1)) {
                  return redirect()->route('adminpemda');
                }

//                if (Auth::guard('web1')->user()->hasRole('User BPKP 1')) {
                if (Auth::guard('web1')->user()->hasRole(2)) {
                    return redirect()->route('userpemda1');
                }

//                if (Auth::guard('web1')->user()->hasRole('User BPKP 2')) {
                if (Auth::guard('web1')->user()->hasRole(3)) {
                    return redirect()->route('userpemda2');
                }

//                if (Auth::guard('web1')->user()->hasRole('Admin PWK')) {
                if (Auth::guard('web1')->user()->hasRole(4)) {
                    return redirect()->route('adminopd');
                }

//                if (Auth::guard('web1')->user()->hasRole('User PWK 1')) {
                if (Auth::guard('web1')->user()->hasRole(5)) {
                    return redirect()->route('useropd1');
                }

//                if (Auth::guard('web1')->user()->hasRole('User PWK 2')) {
                if (Auth::guard('web1')->user()->hasRole(6)) {
                    return redirect()->route('useropd2');
                }

//                if (Auth::guard('web1')->user()->hasRole('User PWK Kegiatan 1')) {
                if (Auth::guard('web1')->user()->hasRole(7)) {
                    return redirect()->route('opdkegiatan1');
                }

//                if (Auth::guard('web1')->user()->hasRole('User PWK Kegiatan 2')) {
                if (Auth::guard('web1')->user()->hasRole(8)) {
                    return redirect()->route('opdkegiatan2');
                }
            }  
        }
     //    if (Auth::attempt(['username' => $request['username'], 'password' => $request['password']])) {
    	// }
    	return redirect()->back();
    }

    public function getlogout()
    {
    	// Auth::logout();
        Auth::guard('web')->logout();
        Auth::guard('web1')->logout();
    	return redirect('/');
    }

}
