<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\adminpemda\Perspektif;
use App\Models\adminpemda\Proses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class ProsesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proses = DB::table('tbl_proses')
        ->select('tbl_proses.id as id', 'tbl_perspektif.nama_perspektif as nama_perspektif', 'tbl_proses.nama_proses as nama_proses')
        ->join('tbl_perspektif', 'tbl_proses.id_perspektif', '=', 'tbl_perspektif.id')
        ->orderBy('id_perspektif','asc')
        ->orderBy('id','asc')
        ->get();

    return view('adminpemda.proses.index', compact('proses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perspektif = Perspektif::pluck('nama_perspektif', 'id');
        return view('adminpemda.proses.create', compact('perspektif'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check())
        {
            $rules = array(
                'id_perspektif' => 'required',
                'nama_proses' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('proses.create')
                ->withErrors($validator);
            } else {

                $proses = new Proses;
                $proses->id_perspektif = Input::get('id_perspektif');
                $proses->nama_proses = Input::get('nama_proses');
                $proses->save();
                Alert::success('Data proses telah ditambahkan.', 'Selamat');
                return redirect()->route('proses.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perspektif = Perspektif::pluck('nama_perspektif', 'id');
        $proses = Proses::find($id);
        return view('adminpemda.proses.edit', compact('perspektif', 'proses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $rules = array(
                'id_perspektif' => 'required',
                'nama_proses' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $proses = Proses::find($id);
                $proses->id_perspektif = Input::get('id_perspektif');
                $proses->nama_proses = Input::get('nama_proses');
                $proses->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('proses.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proses = Proses::find($id);
        $proses->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('proses.index');
    }
}
