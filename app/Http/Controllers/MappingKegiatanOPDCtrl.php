<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MappingKegiatanOPDCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tbl_mapping_unit')
            ->select('tbl_mapping_unit.id as id', 'sasaran_opd.nama_sasaran as nama_sasaran_unit', 'ref_unitkerja.unitkerja as nama_unit', 'program_opd.nama_program as nama_program_opd', 'kegiatan_opd.nama_kegiatan as nama_kegiatan_opd', 'tbl_baganrisiko.nama_risiko as nama_risiko', 'tbl_mapping_unit.bobot_risiko as bobot_risiko', 'tbl_mapping_unit.bobot_kegiatan as bobot_kegiatan', 'tbl_mapping_unit.bobot_program as bobot_program')
            ->join('sasaran_opd', 'sasaran_opd.id', 'tbl_mapping_unit.sasaran_unit_id')
            ->join('ref_unitkerja', 'ref_unitkerja.key_sort_unit', 'tbl_mapping_unit.key_sort_unit')
            ->join('program_opd', 'program_opd.id', 'tbl_mapping_unit.program_id')
            ->join('kegiatan_opd', 'kegiatan_opd.id', 'tbl_mapping_unit.kegiatan_id')
            ->join('tbl_baganrisiko', 'tbl_baganrisiko.id', 'tbl_mapping_unit.risiko_id')
            ->get();
        return view('adminopd.mappingkegiatanopd.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->data['sasaranpemda'] = Sasaran::pluck('nama_sasaran', 'id');
        // $this->data['opd'] = OPD::pluck('nama_opd', 'id');
        $this->data['opd'] = DB::table('ref_unitkerja')
            ->where('status','1')
            ->orderBy('key_sort_unit','ASC')
            ->pluck('unitkerja as nama_opd', 'key_sort_unit');
        return view('adminpemda.mapping.create', $this->data);
    }

    public function pilihSasaranOpd($id)
    {
        // $data = DB::table('sasaran_opd')
        //     ->where('opd_id', $id)
        //     ->get()
        //     ->pluck('nama_sasaran', 'id');
        $data = DB::table('sasaran_opd')
            ->where('key_sort_unit', $id)
            ->get()
            ->pluck('nama_sasaran', 'id');
        return json_encode($data);
    }


    public function cariSasaran(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = DB::table("sasaran_pemda")
                ->select("id","nama_sasaran")
                ->where('nama_sasaran','LIKE',"%$search%")
                ->get();
        }

        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'sasaranpemda_id' => 'required',
                'opd_id' => 'required',
                'sasaranopd_id' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('mapping.create')
                    ->withErrors($validator);
            } else {

                $mapping = new Mapping;
                $mapping->sasaranpemda_id   = Input::get('sasaranpemda_id');
                $mapping->key_sort_unit     = Input::get('opd_id');
                $mapping->opd_id            = '0';
                $mapping->sasaranopd_id     = Input::get('sasaranopd_id');
                $mapping->bobot             = Input::get('bobot');
                $mapping->save();
                Alert::success('Data mapping telah ditambahkan.', 'Selamat');
                return redirect()->route('mapping.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data['mapping'] = Mapping::find($id);
        $this->data['sasaranpemda'] = Sasaran::pluck('nama_sasaran', 'id');
        // $this->data['opd'] = OPD::pluck('nama_opd', 'id');
        $this->data['opd'] = DB::table('ref_unitkerja')->pluck('unitkerja as nama_opd', 'key_sort_unit');
        return view('adminpemda.mapping.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'sasaranpemda_id' => 'required',
                'opd_id' => 'required',
                'sasaranopd_id' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator);
            } else {

                $mapping = Mapping::find($id);
                $mapping->sasaranpemda_id = Input::get('sasaranpemda_id');
                $mapping->opd_id = Input::get('opd_id');
                $mapping->sasaranopd_id = Input::get('sasaranopd_id');
                $mapping->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('mapping.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mapping = Mapping::find($id);
        $mapping->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('mapping.index');
    }
}
