<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefJnsSebab;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\PenyebabRCA;


class Lini2EvaluasiCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $penyebab = DB::table('t_penyebab_rca')
                    ->select('t_penyebab_rca.id_penyebab as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_penyebab_1 as nama_penyebab_1', 't_penyebab_rca.nama_penyebab_2 as nama_penyebab_2', 't_penyebab_rca.nama_penyebab_3 as nama_penyebab_3', 't_penyebab_rca.nama_penyebab_4 as nama_penyebab_4', 't_penyebab_rca.nama_penyebab_5 as nama_penyebab_5', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 'ref_jns_sebab.nama_jns_sebab as nama_jns_sebab')
                    ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                    ->get();

        return view('lini2evaluasi.index', compact('penyebab'));

    }

    public function pilihRisikoUnit2($id) 
    {  

            $risiko = DB::table('t_identifikasi_risiko')
                ->select('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->get();
            return json_encode($risiko);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['s_kd_jabdetail'] = RefDataUmum::pluck('s_nmjabdetail_pemilik','s_kd_jabdetail_pemilik as s_kd_jabdetail');
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->pluck('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi');
        $this->data['id_jns_sebab'] = RefJnsSebab::pluck('nama_jns_sebab', 'id_jns_sebab');

        return view('lini2evaluasi.createevaluasi', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(

                'id_identifikasi' => 'required',
                'nama_penyebab_1' => 'required',
                'nama_akar_penyebab' => 'required',
                'id_jns_sebab' => 'required',
                
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new PenyebabRCA;
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->nama_penyebab_1 = Input::get('nama_penyebab_1');
                $id->nama_penyebab_2 = Input::get('nama_penyebab_2');
                $id->nama_penyebab_3 = Input::get('nama_penyebab_3');
                $id->nama_penyebab_4 = Input::get('nama_penyebab_4');
                $id->nama_penyebab_5 = Input::get('nama_penyebab_5');
                $id->nama_akar_penyebab = Input::get('nama_akar_penyebab');
                $id->id_jns_sebab = Input::get('id_jns_sebab');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2evaluasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getPenyebab()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera)')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $mnid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 1)
                        ->count();
            $myid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 2)
                        ->count();
            $mdid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 3)
                        ->count();
            $mrid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 4)
                        ->count();
            $mcid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 5)
                        ->count();
            $exid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 6)
                        ->count();
            $mnmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 1)
                        ->get()
                        ->count();
            $mymt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 2)
                        ->get()
                        ->count();
            $mdmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 3)
                        ->get()
                        ->count();
            $mrmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 4)
                        ->get()
                        ->count();
            $mcmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 5)
                        ->get()
                        ->count();
            $exmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 6)
                        ->get()
                        ->count();
        return view('lini2evaluasi.penyebab', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','insidenk','insidenr','insidenp','rtpjadwal','rtprealisasi','mnid','myid','mdid','mrid','mcid','exid','mnmt','mymt','mdmt','mrmt','mcmt','exmt'));
        }
    }

    public function getPenyebabList()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $evaluasi = DB::table('t_penyebab_rca')
                        ->select('t_penyebab_rca.id_penyebab as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->get();
            
        return view('lini2evaluasi.penyebablist', compact('evaluasi'));
        }
    }

    public function getFilterPenyebabList($id)
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $evaluasi = DB::table('t_penyebab_rca')
                        ->select('t_penyebab_rca.id_penyebab as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_penyebab_rca.id_jns_sebab as id_jns_sebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('t_penyebab_rca.id_jns_sebab', $id)
                        ->get();
        return view('lini2evaluasi.penyebablist', compact('evaluasi'));
        }
    }



}
