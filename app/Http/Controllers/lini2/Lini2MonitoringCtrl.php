<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\Keterjadian;


class Lini2MonitoringCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $monitoring = DB::table('t_keterjadian')
                    ->select('t_keterjadian.id_keterjadian as id', 'ref_periode.nama_periode as nama_periode', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_keterjadian.nama_kejadian as nama_kejadian', 't_keterjadian.waktu_kejadian as waktu_kejadian', 't_keterjadian.tempat_kejadian as tempat_kejadian', 't_keterjadian.skor_dampak as skor_dampak', 't_keterjadian.pemicu_kejadian as pemicu_kejadian')
                    ->join('ref_periode', 't_keterjadian.id_periode', '=', 'ref_periode.id_periode')
                    ->leftjoin('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                    ->get();

        return view('lini2monitoring.index', compact('monitoring'));

    }

    public function pilihSebab2($id) 
    {  

            $sebab = DB::table('t_penyebab_rca')
                ->select('t_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.id_identifikasi', $id)
                ->get();
            return json_encode($sebab);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['s_kd_jabdetail'] = RefDataUmum::pluck('s_nmjabdetail_pemilik','s_kd_jabdetail_pemilik as s_kd_jabdetail');
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->pluck('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi');
        $this->data['id_penyebab'] = DB::table('t_penyebab_rca')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->pluck('t_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab');
        $this->data['skor_dampak'] = ['1', '2', '3', '4', '5'];

        return view('lini2monitoring.createmonitoring', $this->data);

    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        if (Auth::check())
        {
            $rules = array(

                's_kd_jabdetail' => 'required',
                'nama_kejadian' => 'required',
                'waktu_kejadian' => 'required',
                'tempat_kejadian' => 'required',
                'skor_dampak' => 'required',
                'pemicu_kejadian' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new Keterjadian;
                $id->s_kd_jabdetail = Input::get('s_kd_jabdetail');
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->id_penyebab = Input::get('id_penyebab');
                $id->nama_kejadian = Input::get('nama_kejadian');
                $id->waktu_kejadian = Input::get('waktu_kejadian');
                $id->tempat_kejadian = Input::get('tempat_kejadian');
                $id->skor_dampak = Input::get('skor_dampak');
                $id->pemicu_kejadian = Input::get('pemicu_kejadian');
                $id->user_create = Auth::user()->user_nip;

                $dt = Carbon::parse($id->waktu_kejadian);
                $dn = $dt->quarter;
                $id->id_periode = $dn + 2;

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2monitoring.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getInsiden()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera)')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $r1 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 1)
                        ->count();
            $r2 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 2)
                        ->count();
            $r3 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 3)
                        ->count();
            $r4 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 4)
                        ->count();
            $r5 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 5)
                        ->count();
            $r6 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 6)
                        ->count();
            $p1 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 1)
                        ->count();
            $p2 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 2)
                        ->count();
            $p3 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 3)
                        ->count();
            $p4 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 4)
                        ->count();
            $p5 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 5)
                        ->count();
            $p6 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 6)
                        ->count();

        return view('lini2monitoring.insiden', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp','r1','r2','r3','r4','r5','r6','p1','p2','p3','p4','p5','p6'));
        }
    }

    public function getInsidenKejadian()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko')
                        ->leftjoin('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->get();

        return view('lini2monitoring.insidenkejadian', compact('monitoring'));
        }
    }

    public function getInsidenRisiko()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->get();

        return view('lini2monitoring.insidenrisiko', compact('monitoring'));
        }
    }


    public function getFilterInsidenRisiko($id)
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', $id)
                        ->get();

        return view('lini2monitoring.insidenrisiko', compact('monitoring'));
        }
    }

    public function getInsidenSebab()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca_kode.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->get();

        return view('lini2monitoring.insidensebab', compact('monitoring'));
        }
    }

    public function getFilterInsidenSebab($id)
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca_kode.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->where('t_penyebab_rca.id_jns_sebab', $id)
                        ->get();

        return view('lini2monitoring.insidensebab', compact('monitoring'));
        }
    }



}
