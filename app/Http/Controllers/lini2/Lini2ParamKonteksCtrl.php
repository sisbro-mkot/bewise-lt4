<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefKonteks;
use App\Models\dashboard\RefJnsKonteks;


class Lini2ParamKonteksCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $konteks = DB::table('ref_konteks')
                ->select('ref_konteks.id_konteks as id', 'ref_konteks.nama_konteks as nama_konteks', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->get();
        return view('lini2paramkonteks.index', compact('konteks'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $this->data['id_konteks'] = DB::table('ref_konteks')
                                    ->select('id_konteks as id')
                                    ->orderBy('id','desc')
                                    ->first();
        $this->data['id_jns_konteks'] = RefJnsKonteks::pluck('nama_jns_konteks as nama_jns_konteks','id_jns_konteks');


        return view('lini2paramkonteks.createparamkonteks', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(
                'id_konteks' => 'required',
                'nama_konteks' => 'required',
                'id_jns_konteks' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new RefKonteks;
                $id->id_konteks = Input::get('id_konteks');
                $id->nama_konteks = Input::get('nama_konteks');
                $id->id_jns_konteks = Input::get('id_jns_konteks');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2paramkonteks.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->data['id_jns_konteks'] = RefJnsKonteks::pluck('nama_jns_konteks as nama_jns_konteks','id_jns_konteks');
        $this->data['konteks'] = RefKonteks::find($id);

        return view('lini2paramkonteks.editparamkonteks', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


      if (Auth::check())
        {
            $rules = array(
                'id_konteks' => 'required',
                'nama_konteks' => 'required',
                'id_jns_konteks' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = RefKonteks::find($id);
                $id->id_konteks = Input::get('id_konteks');
                $id->nama_konteks = Input::get('nama_konteks');
                $id->id_jns_konteks = Input::get('id_jns_konteks');
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diedit.', 'Selamat');
                return redirect()->route('lini2paramkonteks.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
