<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefMatriks;


class Lini2AnalisisCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
        $analisis = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 't_analisis_risiko.existing_control as existing_control', 't_analisis_risiko.uraian_existing_control as uraian_existing_control', 't_analisis_risiko.existing_control_memadai as existing_control_memadai', 'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->get();
        
        return view('lini2analisis.index', compact('analisis'));

    }

    public function pilihRisikoUnit($id) 
    {  

            $risiko = DB::table('t_identifikasi_risiko')
                ->select('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->whereNull('t_analisis_risiko.id_identifikasi')
                ->get();
            return json_encode($risiko);
        
    }


    public function pilihSkor($id, $id2) 
    {  
        $skor = DB::table('ref_matriks')
                ->select('skor_risiko', 'id_matriks')
                ->where('skor_kemungkinan', $id)
                ->where('skor_dampak', $id2)
                ->get();
        return json_encode($skor);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->data['s_kd_jabdetail'] = RefDataUmum::pluck('s_nmjabdetail_pemilik','s_kd_jabdetail_pemilik as s_kd_jabdetail');
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->whereNull('t_analisis_risiko.id_identifikasi')
                ->pluck('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi');
        $this->data['kemungkinan_inherent'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_inherent'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_inherent'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['existing_control'] = ['T', 'Y'];
        $this->data['existing_control_memadai'] = ['T', 'Y'];
        $this->data['kemungkinan_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_residual'] = RefMatriks::pluck('skor_risiko', 'id_matriks');


        return view('lini2analisis.createanalisis', $this->data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if (Auth::check())
        {

            $id_matriks_inherent = RefMatriks::where('id_matriks', $request->input('id_matriks_inherent'))->firstOrFail();
            $skor_inherent = $id_matriks_inherent->skor_risiko;

            $id_matriks_residual = RefMatriks::where('id_matriks', $request->input('id_matriks_residual'))->firstOrFail();
            $skor_residual = $id_matriks_residual->skor_risiko;

            $rules = array(

                'id_identifikasi' => 'required',
                'kemungkinan_inherent' => 'required',
                'dampak_inherent' => 'required',
                'id_matriks_inherent' => 'required',
                'existing_control' => 'required',
                'uraian_existing_control' => 'required',
                'existing_control_memadai' => 'required',
                'kemungkinan_residual' => 'required',
                'dampak_residual' => 'required',
                'id_matriks_residual' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } elseif($skor_residual > $skor_inherent) {
                return redirect()->back()->withErrors('Level Risiko Residual tidak boleh lebih tinggi daripada Level Risiko Inherent');
            } else {

                $id = new AnalisisRisiko;
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->id_matriks_inherent = Input::get('id_matriks_inherent');
                $id->existing_control = Input::get('existing_control');
                $id->uraian_existing_control = Input::get('uraian_existing_control');
                $id->existing_control_memadai = Input::get('existing_control_memadai');
                $id->id_matriks_residual = Input::get('id_matriks_residual');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2analisis.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
 
    public function getSkorAnalisis()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $analisis = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera)')
                        ->get();
        return view('lini2analisis.skoranalisis', compact('analisis'));
        }
    }


}
