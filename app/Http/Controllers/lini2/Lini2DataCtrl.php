<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefMatriks;
use App\Models\dashboard\RefKonteks;
use App\Models\dashboard\PenetapanKonteks;

class Lini2DataCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = DB::table('ref_data_umum')
                ->select('ref_data_umum.id_data_umum as id','wm_jabdetail.s_nmjabdetail as s_nmjabdetail','ref_data_umum.skor_selera as skor_selera')
                ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->get();

        return view('lini2data.index', compact('data'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->data['namaunit'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $this->data['dataumum'] = DB::table('ref_data_umum')
                ->select('ref_data_umum.id_data_umum as id','pemilik.s_nmjabdetail as s_kd_jabdetail_pemilik','koordinator.s_nmjabdetail as s_kd_jabdetail_koordinator','ref_data_umum.skor_selera as skor_selera')
                ->join('wm_jabdetail as pemilik', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'pemilik.s_kd_jabdetail')
                ->join('wm_jabdetail as koordinator', 'ref_data_umum.s_kd_jabdetail_koordinator', '=', 'koordinator.s_kd_jabdetail')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();        
        $this->data['skor_selera'] = RefMatriks::orderBy('skor_risiko','desc')->pluck('skor_risiko');
        $this->data['data'] = RefDataUmum::find($id);

        return view('lini1data.edit', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
      if (Auth::check())
        {
            $rules = array(

                'skor_selera' => 'required',

            );
            $messages = [
                            'skor_selera.required' => 'Silahkan pilih skor selera risiko.',
                        ];

            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $idn = RefDataUmum::find($id);
                $idn->skor_selera = Input::get('skor_selera');

                $idn->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1data.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
