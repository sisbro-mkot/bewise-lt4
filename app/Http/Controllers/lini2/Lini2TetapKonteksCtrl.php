<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefJnsKonteks;
use App\Models\dashboard\RefKonteks;
use App\Models\dashboard\PenetapanKonteks;

class Lini2TetapKonteksCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $konteks = DB::table('t_penetapan_konteks')
                ->select('t_penetapan_konteks.id_penetapan_konteks as id', 't_penetapan_konteks.tahun as tahun', 'ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik', 't_penetapan_konteks.id_konteks as id_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks','t_penetapan_konteks.catatan_hapus as catatan_hapus')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->get();

        return view('lini2tetapkonteks.index', compact('konteks'));

    }
 
    public function pilihKonteks($id) 
    {  

            $konteks = DB::table('ref_konteks')
                ->select('nama_konteks', 'id_konteks')
                ->where('id_jns_konteks', $id)
                ->get();
            return json_encode($konteks);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $this->data['tahun'] = Carbon::now()->year;
        $this->data['id_data_umum'] = RefDataUmum::pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');
        $this->data['id_jns_konteks'] = RefJnsKonteks::pluck('nama_jns_konteks as nama_jns_konteks','id_jns_konteks');
        $this->data['id_konteks'] = RefKonteks::pluck('nama_konteks as nama_konteks','id_konteks');

        return view('lini2tetapkonteks.createtetapkonteks', $this->data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                'id_data_umum' => 'required',
                'id_konteks' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new PenetapanKonteks;
                $id->tahun = Input::get('tahun');
                $id->id_data_umum = Input::get('id_data_umum');
                $id->id_konteks = Input::get('id_konteks');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2tetapkonteks.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
 
    public function delete($id)
    {
        if (Auth::check())
        {
            $now = Carbon::now('Asia/Jakarta');
            $idn = PenetapanKonteks::find($id);
            $idn->catatan_hapus = Auth::user()->user_nip;
            $idn->deleted_at = $now;
            $idn->save();
            DB::table('t_identifikasi_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_identifikasi_risiko.catatan_hapus' => Auth::user()->user_nip],['t_identifikasi_risiko.deleted_at' => $now]);
            DB::table('t_analisis_risiko')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_analisis_risiko.catatan_hapus' => Auth::user()->user_nip],['t_analisis_risiko.deleted_at' => $now]);
            DB::table('t_penyebab_rca')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_penyebab_rca.catatan_hapus' => Auth::user()->user_nip],['t_penyebab_rca.deleted_at' => $now]);
            DB::table('t_rtp')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_rtp.catatan_hapus' => Auth::user()->user_nip],['t_rtp.deleted_at' => $now]);
            DB::table('t_pemantauan')
                ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_pemantauan.catatan_hapus' => Auth::user()->user_nip],['t_pemantauan.deleted_at' => $now]);

            Alert::info('Data telah dihapus.');
            return redirect()->route('lini2tetapkonteks.index');
        } else {
            return redirect()->back();
        }
    }

    public function batal($id)
    {
        if (Auth::check())
        {
            $now = Carbon::now('Asia/Jakarta');
            $idn = PenetapanKonteks::find($id);
            $idn->catatan_hapus = NULL;
            $idn->deleted_at = $now;
            $idn->save();
            DB::table('t_identifikasi_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_identifikasi_risiko.catatan_hapus' => NULL],['t_identifikasi_risiko.deleted_at' => $now]);
            DB::table('t_analisis_risiko')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_analisis_risiko.catatan_hapus' => NULL],['t_analisis_risiko.deleted_at' => $now]);
            DB::table('t_penyebab_rca')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_penyebab_rca.catatan_hapus' => NULL],['t_penyebab_rca.deleted_at' => $now]);
            DB::table('t_rtp')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_rtp.catatan_hapus' => NULL],['t_rtp.deleted_at' => $now]);
            DB::table('t_pemantauan')
                ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->where('t_identifikasi_risiko.id_penetapan_konteks', $id)
                ->update(['t_pemantauan.catatan_hapus' => NULL],['t_pemantauan.deleted_at' => $now]);

            Alert::info('Data telah dipulihkan.');
            return redirect()->route('lini2tetapkonteks.index');
        } else {
            return redirect()->back();
        }
    }


}
