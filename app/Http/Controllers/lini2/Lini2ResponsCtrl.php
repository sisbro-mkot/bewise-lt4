<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefSubUnsurSPIP;
use App\Models\dashboard\JabDetail;
use App\Models\dashboard\RefOutput;
use App\Models\dashboard\RefPeriode;
use App\Models\dashboard\RTP;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefMatriks;

class Lini2ResponsCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $rtp = DB::table('t_rtp')
            ->select('t_rtp.id_rtp as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.respon_risiko as respon_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'ref_output.nama_output as nama_output', 'ref_periode.nama_periode as nama_periode')
            ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
            ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
            ->join('ref_sub_unsur_spip', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
            ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
            ->join('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
            ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
            ->get();


        return view('lini2respons.index', compact('rtp'));

    }

    public function pilihSebab($id) 
    {  

            $sebab = DB::table('t_penyebab_rca')
                ->select('t_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->get();
            return json_encode($sebab);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['s_kd_jabdetail'] = RefDataUmum::pluck('s_nmjabdetail_pemilik','s_kd_jabdetail_pemilik as s_kd_jabdetail');
        $this->data['id_penyebab'] = DB::table('t_penyebab_rca')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->pluck('t_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab');
        $this->data['respon_risiko'] = ['D', 'K', 'B'];
        $this->data['id_sub_unsur'] = RefSubUnsurSPIP::pluck('nama_sub_unsur','id_sub_unsur');
        $this->data['s_kd_jabdetail_pic'] = JabDetail::pluck('s_nmjabdetail','s_kd_jabdetail');
        $this->data['id_output'] = RefOutput::pluck('nama_output','id_output');
        $this->data['id_periode'] = RefPeriode::where('jenis_periode', '=', 'tw')->pluck('nama_periode','id_periode');

        return view('lini2respons.createrespons', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (Auth::check())
        {
            $rules = array(

                'id_penyebab' => 'required',
                'respon_risiko' => 'required',
                'id_sub_unsur' => 'required',
                's_kd_jabdetail_pic' => 'required',
                'id_output' => 'required',
                'id_periode' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $currentYear = Carbon::now()->format('Y');
                $Q1end = Carbon::createMidnightDate($currentYear,3,31);
                $Q2end = Carbon::createMidnightDate($currentYear,6,30);
                $Q3end = Carbon::createMidnightDate($currentYear,9,30);
                $Q4end = Carbon::createMidnightDate($currentYear,12,31);

                $id = new RTP;
                $id->id_penyebab = Input::get('id_penyebab');
                $id->respon_risiko = Input::get('respon_risiko');
                $id->kegiatan_pengendalian = Input::get('kegiatan_pengendalian');
                $id->id_sub_unsur = Input::get('id_sub_unsur');
                $id->s_kd_jabdetail_pic = Input::get('s_kd_jabdetail_pic');
                $id->id_output = Input::get('id_output');
                $id->id_periode = Input::get('id_periode');
                $id->user_create = Auth::user()->user_nip;

                if ($id->id_periode == 3){
                    $id->deadline_rtp = $Q1end;
                } elseif ($id->id_periode == 4){
                    $id->deadline_rtp = $Q2end;
                } elseif ($id->id_periode == 5){
                    $id->deadline_rtp = $Q3end;
                } else {
                    $id->deadline_rtp = $Q4end;
                }
                
                $analisis = DB::table('t_analisis_risiko')
                            ->select('t_analisis_risiko.id_analisis as id_analisis')
                            ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                            ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                            ->where('t_penyebab_rca.id_penyebab', '=', $id->id_penyebab)
                            ->first(); 

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2treated.edit', $analisis->id_analisis);
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getRTP()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $triwulankini = Carbon::now()->quarter;
            $tricount = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->whereRaw(('t_rtp.id_periode - 2 = ?'), $triwulankini)
                        ->count();
            $terlambat = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_pemantauan.nama_hambatan')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->whereRaw('DATEDIFF(t_rtp.deadline_rtp, t_pemantauan.realisasi_waktu) < 0 OR (ISNULL(realisasi_waktu) AND DATEDIFF(t_rtp.deadline_rtp, CURRENT_TIMESTAMP) < 0)')
                        ->count();
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera)')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();

        return view('lini2respons.rtp', compact('triwulankini','tricount','terlambat','risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp'));
        }
    }

    public function getRTPList()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $respons = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->get();

        return view('lini2respons.rtplist', compact('respons'));
        }
    }

    public function getRTPListCurrent()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $triwulankini = Carbon::now()->quarter;
            $respons = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->whereRaw(('t_rtp.id_periode - 2 = ?'), $triwulankini)
                        ->get();

        return view('lini2respons.rtplistcurrent', compact('respons','triwulankini'));
        }
    }

    public function getMonitor()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $respons = DB::table('t_pemantauan')
                        ->select('t_pemantauan.id_pemantauan as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode', 't_pemantauan.realisasi_waktu as realisasi_waktu')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->get();

        return view('lini2respons.monitor', compact('respons'));
        }
    }

    public function getResponsLambat()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {

            $respons = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_pemantauan.nama_hambatan')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->whereRaw('DATEDIFF(t_rtp.deadline_rtp, t_pemantauan.realisasi_waktu) < 0 OR (ISNULL(realisasi_waktu) AND DATEDIFF(t_rtp.deadline_rtp, CURRENT_TIMESTAMP) < 0)')
                        ->get();

        return view('lini2respons.responslambat', compact('respons'));
        }
    }




}
