<?php

namespace App\Http\Controllers\lini2;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\IdentifikasiRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefBaganRisiko;


class Lini2IdentifikasiCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.tahun as tahun', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail','ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->whereRaw('ISNULL(t_penetapan_konteks.catatan_hapus)')
                ->get();

        return view('lini2identifikasi.index', compact('identifikasi'));


    }

    public function pilihKonteksUnit($id) 
    {  

            $konteks = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', 'ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->where('s_kd_jabdetail_pemilik', $id)
                ->get();
            return json_encode($konteks);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['tahun'] = Carbon::now()->year;
        $this->data['s_kd_jabdetail'] = RefDataUmum::pluck('s_nmjabdetail_pemilik','s_kd_jabdetail_pemilik as s_kd_jabdetail');
        $this->data['id_penetapan_konteks'] = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', 'ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->get();
        $this->data['id_bagan_risiko'] = RefBaganRisiko::pluck('nama_bagan_risiko as nama_bagan_risiko','id_bagan_risiko');

        return view('lini2identifikasi.createidentifikasi', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                's_kd_jabdetail' => 'required',
                'id_penetapan_konteks' => 'required',
                'id_bagan_risiko' => 'required',
                'nama_dampak' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new IdentifikasiRisiko;
                $id->tahun = Input::get('tahun');
                $id->s_kd_jabdetail = Input::get('s_kd_jabdetail');
                $id->id_penetapan_konteks = Input::get('id_penetapan_konteks');
                $id->id_bagan_risiko = Input::get('id_bagan_risiko');
                $id->nama_dampak = Input::get('nama_dampak');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2identifikasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getDashPeta()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera)')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $bpkpmax = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "BPKP"')
                        ->count();
            $bpkp = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "BPKP"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $sumax = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "SU"')
                        ->count();
            $su = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "BPKP"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $d1max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D1"')
                        ->count();
            $d1 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D1"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $d2max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D2"')
                        ->count();
            $d2 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D2"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $d4max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D4"')
                        ->count();
            $d4 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D4"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $d5max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D5"')
                        ->count();
            $d5 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D5"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $d6max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D6"')
                        ->count();
            $d6 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "D6"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $inmax = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "IN"')
                        ->count();
            $in = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "IN"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $dlmax = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "DL"')
                        ->count();
            $dl = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "DL"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $lbmax = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "LB"')
                        ->count();
            $lb = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "LB"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $ipmax = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "IP"')
                        ->count();
            $ip = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "IP"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $jfmax = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "JF"')
                        ->count();
            $jf = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "JF"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw01 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW01"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw02 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW02"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw03 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW03"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw04 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW04"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw05 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW05"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw06 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW06"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw07 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW07"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw08 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW08"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw09 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW09"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw10 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW10"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw11 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW11"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw12 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW12"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw13 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW13"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw14 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW14"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw15 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW15"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw16 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW16"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw17 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW17"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw18 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW18"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw19 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW19"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw20 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW20"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw21 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW21"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw22 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW22"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw23 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW23"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw24 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW24"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw25 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW25"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw26 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW26"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw27 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW27"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw28 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW28"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw29 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW29"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw30 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW30"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw31 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW31"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw32 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW32"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw33 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW33"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();
            $pw34 = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW34"')
                        ->whereRaw('ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera')
                        ->count();

            $pw01max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW01"')
                        ->count();
            $pw02max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW02"')
                        ->count();
            $pw03max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW03"')
                        ->count();
            $pw04max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW04"')
                        ->count();
            $pw05max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW05"')
                        ->count();
            $pw06max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW06"')
                        ->count();
            $pw07max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW07"')
                        ->count();
            $pw08max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW08"')
                        ->count();
            $pw09max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW09"')
                        ->count();
            $pw10max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW10"')
                        ->count();
            $pw11max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW11"')
                        ->count();
            $pw12max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW12"')
                        ->count();
            $pw13max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW13"')
                        ->count();
            $pw14max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW14"')
                        ->count();
            $pw15max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW15"')
                        ->count();
            $pw16max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW16"')
                        ->count();
            $pw17max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW17"')
                        ->count();
            $pw18max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW18"')
                        ->count();
            $pw19max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW19"')
                        ->count();
            $pw20max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW20"')
                        ->count();
            $pw21max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW21"')
                        ->count();
            $pw22max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW22"')
                        ->count();
            $pw23max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW23"')
                        ->count();
            $pw24max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW24"')
                        ->count();
            $pw25max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW25"')
                        ->count();
            $pw26max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW26"')
                        ->count();
            $pw27max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW27"')
                        ->count();
            $pw28max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW28"')
                        ->count();
            $pw29max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW29"')
                        ->count();
            $pw30max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW30"')
                        ->count();
            $pw31max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW31"')
                        ->count();
            $pw32max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW32"')
                        ->count();
            $pw33max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW33"')
                        ->count();
            $pw34max = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('wm_instansiunitorg.s_format_surat = "PW34"')
                        ->count();


            return view('lini2identifikasi.dashpeta',  
                compact('risikoteridentifikasi',
                'risikotermitigasi',
                'sebabteridentifikasi',
                'sebabtermitigasi',
                'rtpjadwal',
                'rtprealisasi',
                'insidenk',
                'insidenr',
                'insidenp',
                'bpkpmax',
                'bpkp',
                'sumax',
                'su',
                'd1max',
                'd1',
                'd2max',
                'd2',
                'd4max',
                'd4',
                'd5max',
                'd5',
                'd6max',
                'd6',
                'inmax',
                'in',
                'dlmax',
                'dl',
                'lbmax',
                'lb',
                'ipmax',
                'ip',
                'jfmax',
                'jf',
                'pw01',
                'pw01max',
                'pw02',
                'pw02max',
                'pw03',
                'pw03max',
                'pw04',
                'pw04max',
                'pw05',
                'pw05max',
                'pw06',
                'pw06max',
                'pw07',
                'pw07max',
                'pw08',
                'pw08max',
                'pw09',
                'pw09max',
                'pw10',
                'pw10max',
                'pw11',
                'pw11max',
                'pw12',
                'pw12max',
                'pw13',
                'pw13max',
                'pw14',
                'pw14max',
                'pw15',
                'pw15max',
                'pw16',
                'pw16max',
                'pw17',
                'pw17max',
                'pw18',
                'pw18max',
                'pw19',
                'pw19max',
                'pw20',
                'pw20max',
                'pw21',
                'pw21max',
                'pw22',
                'pw22max',
                'pw23',
                'pw23max',
                'pw24',
                'pw24max',
                'pw25',
                'pw25max',
                'pw26',
                'pw26max',
                'pw27',
                'pw27max',
                'pw28',
                'pw28max',
                'pw29',
                'pw29max',
                'pw30',
                'pw30max',
                'pw31',
                'pw31max',
                'pw32',
                'pw32max',
                'pw33',
                'pw33max',
                'pw34',
                'pw34max'));
        }
    }

    public function getHeatmap()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera)')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $l1 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 1)) OR (ref_matriks_treated.skor_risiko = 1)')
                ->count();
            $l2 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 2)) OR (ref_matriks_treated.skor_risiko = 2)')
                ->count();
            $l3 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 3)) OR (ref_matriks_treated.skor_risiko = 3)')
                ->count();
            $l4 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 4)) OR (ref_matriks_treated.skor_risiko = 4)')
                ->count();
            $l5 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 5)) OR (ref_matriks_treated.skor_risiko = 5)')
                ->count();
            $l6 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 6)) OR (ref_matriks_treated.skor_risiko = 6)')
                ->count();
            $l7 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 7)) OR (ref_matriks_treated.skor_risiko = 7)')
                ->count();
            $l8 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 8)) OR (ref_matriks_treated.skor_risiko = 8)')
                ->count();
            $l9 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 9)) OR (ref_matriks_treated.skor_risiko = 9)')
                ->count();
            $l10 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 10)) OR (ref_matriks_treated.skor_risiko = 10)')
                ->count();
            $l11 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 11)) OR (ref_matriks_treated.skor_risiko = 11)')
                ->count();
            $l12 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 12)) OR (ref_matriks_treated.skor_risiko = 12)')
                ->count();
            $l13 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 13)) OR (ref_matriks_treated.skor_risiko = 13)')
                ->count();
            $l14 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 14)) OR (ref_matriks_treated.skor_risiko = 14)')
                ->count();
            $l15 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 15)) OR (ref_matriks_treated.skor_risiko = 15)')
                ->count();
            $l16 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 16)) OR (ref_matriks_treated.skor_risiko = 16)')
                ->count();
            $l17 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 17)) OR (ref_matriks_treated.skor_risiko = 17)')
                ->count();
            $l18 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 18)) OR (ref_matriks_treated.skor_risiko = 18)')
                ->count();
            $l19 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 19)) OR (ref_matriks_treated.skor_risiko = 19)')
                ->count();
            $l20 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 20)) OR (ref_matriks_treated.skor_risiko = 20)')
                ->count();
            $l21 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 21)) OR (ref_matriks_treated.skor_risiko = 21)')
                ->count();
            $l22 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 22)) OR (ref_matriks_treated.skor_risiko = 22)')
                ->count();
            $l23 = DB::table('t_identifikasi_risiko')
              ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 23)) OR (ref_matriks_treated.skor_risiko = 23)')
                ->count();
            $l24 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 24)) OR (ref_matriks_treated.skor_risiko = 24)')
                ->count();
            $l25 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 25)) OR (ref_matriks_treated.skor_risiko = 25)')
                ->count();

        return view('lini2identifikasi.heatmap',  compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','insidenk','insidenr','insidenp','rtprealisasi','l1','l2','l3','l4','l5','l6','l7','l8','l9','l10','l11','l12','l13','l14','l15','l16','l17','l18','l19','l20','l21','l22','l23','l24','l25'));
        }
    }

    public function getDashRegister()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') { 
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                            })
                        ->leftjoin(DB::raw('(SELECT 
                            C.id_identifikasi AS id_identifikasi,
                            C.id_penyebab AS id_penyebab,
                            D.count_rtp AS count_rtp 
                            FROM t_penyebab_rca AS C
                            LEFT JOIN
                                (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                FROM t_rtp AS A
                                LEFT JOIN t_penyebab_rca AS B
                                ON A.id_penyebab = B.id_penyebab
                                GROUP BY B.id_penyebab) AS D
                                ON C.id_penyebab = D.id_penyebab
                                GROUP BY C.id_identifikasi) AS RTP'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                            })
                        ->get();
        return view('lini2identifikasi.register', compact('identifikasi'));
        }
    }

    public function getFilterRegister($id)
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            
            $identifikasi = DB::table('t_identifikasi_risiko')
                            ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                            ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                            ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                            ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                            ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                            ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                                })
                            ->leftjoin(DB::raw('(SELECT 
                                C.id_identifikasi AS id_identifikasi,
                                C.id_penyebab AS id_penyebab,
                                D.count_rtp AS count_rtp 
                                FROM t_penyebab_rca AS C
                                LEFT JOIN
                                    (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                    FROM t_rtp AS A
                                    LEFT JOIN t_penyebab_rca AS B
                                    ON A.id_penyebab = B.id_penyebab
                                    GROUP BY B.id_penyebab) AS D
                                    ON C.id_penyebab = D.id_penyebab
                                    GROUP BY C.id_identifikasi) AS RTP'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                                })
                            ->where('ref_matriks_treated.skor_risiko', $id)
                            ->orWhere([
                                ['ref_matriks_residual.skor_risiko', '=', $id],
                                ['ref_matriks_treated.skor_risiko', '=', NULL],
                            ])
                            ->get();
        return view('lini2identifikasi.register', compact('identifikasi'));
        }
    }

    public function getRegisterUnit($id)
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') { 
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id', 
                            't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                            'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                            'residual.skor_risiko as skor_risiko_residual',
                            't_penyebab_rca_kode.kode_penyebab as kode_penyebab',
                            't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab',
                            't_rtp.kegiatan_pengendalian as kegiatan_pengendalian',
                            'ref_periode.nama_periode as nama_periode',
                            't_pemantauan.realisasi_waktu as realisasi_waktu',
                            'treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as residual', 't_analisis_risiko.id_matriks_residual', '=', 'residual.id_matriks')
                        ->leftjoin('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->leftjoin('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->leftjoin('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->leftjoin('ref_matriks as treated', 't_analisis_risiko.id_matriks_treated', '=', 'treated.id_matriks')
                        ->where('wm_instansiunitorg.s_format_surat', $id)
                        ->get();

            $unit = DB::table('wm_instansiunitorg')
                    ->where('wm_instansiunitorg.s_format_surat', $id)
                    ->first();

        return view('lini2identifikasi.registerunit', compact('identifikasi','unit'));
        }
    }


}
