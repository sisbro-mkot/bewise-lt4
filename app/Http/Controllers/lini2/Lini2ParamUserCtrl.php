<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\TblUserRole;



class Lini2ParamUserCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $peran = DB::table('tbl_user_role')
                ->select('tbl_user_role.id as id', 'vw_renpeglast.nipbaru as nipbaru', 'vw_renpeglast.s_nama_lengkap as s_nama_lengkap', 'tbl_role.nama_role as nama_role')
                ->join('vw_renpeglast', 'tbl_user_role.user_nip', '=', 'vw_renpeglast.niplama')
                ->join('tbl_role', 'tbl_user_role.role_id', '=', 'tbl_role.id')
                ->get();
        return view('lini2paramuser.index', compact('peran'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $this->data['nip'] = DB::table('vw_renpeglast')
                ->select('vw_renpeglast.s_nama_lengkap as s_nama_lengkap', 'vw_renpeglast.nipbaru as nipbaru', 'vw_renpeglast.niplama as niplama')
                ->leftjoin('tbl_user_role', 'vw_renpeglast.niplama', '=', 'tbl_user_role.user_nip')
                ->whereNull('tbl_user_role.user_nip')
                ->get();

        return view('lini2paramuser.createparamuser', $this->data);

    }

    public function ubah()
    {
        
        $this->data['nip'] = DB::table('tbl_user_role')
                ->select('vw_renpeglast.s_nama_lengkap as s_nama_lengkap', 'vw_renpeglast.nipbaru as nipbaru', 'vw_renpeglast.niplama as niplama')
                ->join('vw_renpeglast', 'tbl_user_role.user_nip', '=', 'vw_renpeglast.niplama')
                ->get();

        return view('lini2paramuser.ubahparamuser', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(

                'roles' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $roles = $request->input('roles');

                foreach($roles as $role){
                $id = new TblUserRole;
                $id->user_nip = Input::get('nip');
                $id->role_id = $role;
                $id->save();}

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2paramuser.index');
            }
        } else {
            return redirect()->back();
        }

    }

    public function simpan(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(

                'roles' => 'required',

            );
            $messages = [
                            'roles.required' => 'Silahkan pilih minimal satu peran pengguna.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $roles = $request->input('roles');

                $niplama = Input::get('nip');
                $data = TblUserRole::where('user_nip', $niplama);
                $data->delete();

                foreach($roles as $role){
                $id = new TblUserRole;
                $id->user_nip = Input::get('nip');
                $id->role_id = $role;
                $id->save();}

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2paramuser.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $this->data['peran'] = TblRole::find($id);

        return view('lini2paramrole.editparamrole', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


      if (Auth::check())
        {
            $rules = array(

                'nama_role' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = TblRole::find($id);
                $id->nama_role = Input::get('nama_role');

                $id->save();
                Alert::success('Data telah diedit.', 'Selamat');
                return redirect()->route('lini2paramrole.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
