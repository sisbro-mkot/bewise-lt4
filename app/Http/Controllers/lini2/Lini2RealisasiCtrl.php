<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\Pemantauan;


class Lini2RealisasiCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $realisasi = DB::table('t_pemantauan')
                ->select('t_pemantauan.id_pemantauan as id', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'periode_rencana.nama_periode as nama_periode_rencana', 't_pemantauan.realisasi_waktu as realisasi_waktu', 't_pemantauan.nama_hambatan as nama_hambatan')
                ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->get();

        return view('lini2realisasi.index', compact('realisasi'));

    }
 
    public function pilihRTP($id) 
    {  

            $rtp = DB::table('t_rtp')
                ->select('t_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_rtp.id_rtp as id_rtp', 't_rtp.id_periode as id_periode', 'periode_rencana.nama_periode as nama_periode_rencana')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->whereNull('t_pemantauan.id_rtp')
                ->get();
            return json_encode($rtp);
        
    }

    public function pilihPeriodeRTP($id) 
    {  

            $periode = DB::table('t_rtp')
                ->select('t_rtp.id_periode as id_periode', 'periode_rencana.nama_periode as nama_periode_rencana')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->where('t_rtp.id_rtp', $id)
                ->get();
            return json_encode($periode);
        
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $this->data['s_kd_jabdetail'] = RefDataUmum::pluck('s_nmjabdetail_pemilik','s_kd_jabdetail_pemilik as s_kd_jabdetail');
            $this->data['id_rtp'] = DB::table('t_rtp')
                                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                                ->whereNull('t_pemantauan.id_rtp')
                                ->pluck('t_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_rtp.id_rtp as id_rtp');
            $this->data['periode_rencana'] = DB::table('t_rtp')
                                ->select('ref_periode.nama_periode as nama_periode', 't_rtp.id_periode as periode_rencana')
                                ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                                ->first();

            return view('lini2realisasi.createrealisasi', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (Auth::check())
        {
            $rules = array(

                'id_rtp' => 'required',
                'realisasi_waktu' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new Pemantauan;
                $id->id_rtp = Input::get('id_rtp');
                $id->realisasi_waktu = Input::get('realisasi_waktu');
                $id->nama_hambatan = Input::get('nama_hambatan');
                $id->user_create = Auth::user()->user_nip;

                $dt = Carbon::parse($id->realisasi_waktu);
                $dn = $dt->quarter;
                $id->id_periode = $dn + 2;

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2realisasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
