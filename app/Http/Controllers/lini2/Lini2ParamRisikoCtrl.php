<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefBaganRisiko;
use App\Models\dashboard\RefKategoriRisiko;
use App\Models\dashboard\RefTujuanSPIP;


class Lini2ParamRisikoCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $risiko = DB::table('ref_bagan_risiko')
                ->select('ref_bagan_risiko.id_bagan_risiko as id','ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko','ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('ref_kategori_risiko', 'ref_bagan_risiko.id_kategori_risiko', '=', 'ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->get();
        return view('lini2paramrisiko.index', compact('risiko'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $this->data['id_kategori_risiko'] = RefKategoriRisiko::pluck('nama_kategori_risiko as nama_kategori_risiko','id_kategori_risiko');
        $this->data['id_tujuan_spip'] = RefTujuanSPIP::pluck('nama_tujuan_spip as nama_tujuan_spip','id_tujuan_spip');


        return view('lini2paramrisiko.createparamrisiko', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(

                'id_kategori_risiko' => 'required',
                'nama_bagan_risiko' => 'required',
                'id_tujuan_spip' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new RefBaganRisiko;
                $id->id_kategori_risiko = Input::get('id_kategori_risiko');
                $id->nama_bagan_risiko = Input::get('nama_bagan_risiko');
                $id->id_tujuan_spip = Input::get('id_tujuan_spip');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2paramrisiko.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->data['id_kategori_risiko'] = RefKategoriRisiko::pluck('nama_kategori_risiko as nama_kategori_risiko','id_kategori_risiko');
        $this->data['id_tujuan_spip'] = RefTujuanSPIP::pluck('nama_tujuan_spip as nama_tujuan_spip','id_tujuan_spip');
        $this->data['bagan'] = RefBaganRisiko::find($id);

        return view('lini2paramrisiko.editparamrisiko', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


      if (Auth::check())
        {
            $rules = array(

                'id_kategori_risiko' => 'required',
                'nama_bagan_risiko' => 'required',
                'id_tujuan_spip' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = RefBaganRisiko::find($id);
                $id->id_kategori_risiko = Input::get('id_kategori_risiko');
                $id->nama_bagan_risiko = Input::get('nama_bagan_risiko');
                $id->id_tujuan_spip = Input::get('id_tujuan_spip');
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diedit.', 'Selamat');
                return redirect()->route('lini2paramrisiko.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
