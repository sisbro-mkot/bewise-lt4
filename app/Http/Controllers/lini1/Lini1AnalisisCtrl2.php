<?php
 
namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefMatriks;
use App\Models\dashboard\RefSubUnsurSPIP;


class Lini1AnalisisCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $analisis = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 't_analisis_risiko.existing_control as existing_control', 't_analisis_risiko.uraian_existing_control as uraian_existing_control', 'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur', 't_analisis_risiko.existing_control_memadai as existing_control_memadai', 'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('ref_sub_unsur_spip', 't_analisis_risiko.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $risiko = DB::table('t_identifikasi_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->count();
        $data = DB::table('t_identifikasi_risiko')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->whereNull('t_analisis_risiko.id_identifikasi')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->count();

        return view('lini1analisis.index', compact('analisis','unit','risiko','data'));

    }

    public function pilihRisikoUnit($id) 
    {  

            $risiko = DB::table('t_identifikasi_risiko')
                ->select('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->whereNull('t_analisis_risiko.id_identifikasi')
                ->get();
            return json_encode($risiko);
        
    }


    public function pilihSkor($id, $id2) 
    {  
        $skor = DB::table('ref_matriks')
                ->select('skor_risiko', 'id_matriks')
                ->where('skor_kemungkinan', $id)
                ->where('skor_dampak', $id2)
                ->get();
        return json_encode($skor);
    }

    public function cetak()
    {
        $analisis = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent', 
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 
                    't_analisis_risiko.existing_control as existing_control', 
                    't_analisis_risiko.uraian_existing_control as uraian_existing_control', 
                    'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur', 
                    't_analisis_risiko.existing_control_memadai as existing_control_memadai',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('ref_sub_unsur_spip', 't_analisis_risiko.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->orderBy('kode_identifikasi_risiko','asc')
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Carbon::now()->year;

        $pdf = PDF::loadView('lini1analisis.cetak',  compact('analisis', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('analisis_risiko.pdf', array('Attachment' => false));
        exit(0);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->whereNull('t_analisis_risiko.id_identifikasi')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['kemungkinan_inherent'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_inherent'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_inherent'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['existing_control'] = ["T" => "Tidak", "Y" => "Ya",];
        $this->data['id_sub_unsur'] = RefSubUnsurSPIP::pluck('nama_sub_unsur','id_sub_unsur');
        $this->data['existing_control_memadai'] = ["T" => "Tidak", "Y" => "Ya",];
        $this->data['kemungkinan_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_residual'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();


        return view('lini1analisis.createanunit', $this->data); 
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if (Auth::check())
        {

            $id_matriks_inherent = RefMatriks::where('id_matriks', $request->input('id_matriks_inherent'))->firstOrFail();
            $skor_inherent = $id_matriks_inherent->skor_risiko;

            $id_matriks_residual = RefMatriks::where('id_matriks', $request->input('id_matriks_residual'))->firstOrFail();
            $skor_residual = $id_matriks_residual->skor_risiko;

            $rules = array(

                'id_identifikasi' => 'required',
                'id_matriks_inherent' => 'required',
                'existing_control' => 'required',
                'existing_control_memadai' => 'required',
                'id_matriks_residual' => 'required',

            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'id_matriks_inherent.required' => 'Silahkan pilih skala kemungkinan dan dampak inherent risk.',
                            'existing_control.required' => 'Silahkan pilih status keberadaan pengendalian yang sudah ada.',
                            'existing_control_memadai.required' => 'Silahkan pilih efektivitas pengendalian yang sudah ada.',
                            'id_matriks_residual.required' => 'Silahkan pilih skala kemungkinan dan dampak residual risk.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } elseif($skor_residual > $skor_inherent) {
                return redirect()->back()->withErrors('Level Risiko Residual tidak boleh lebih tinggi daripada Level Risiko Inherent');
            } else {

                $id = new AnalisisRisiko;
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->id_matriks_inherent = Input::get('id_matriks_inherent');
                $id->existing_control = Input::get('existing_control');
                
                if ($id->existing_control == 'Y'){
                    $id->uraian_existing_control = Input::get('uraian_existing_control');
                    $id->id_sub_unsur = Input::get('id_sub_unsur');
                    $id->existing_control_memadai = Input::get('existing_control_memadai');
                    $id->id_matriks_residual = Input::get('id_matriks_residual');
                } else {
                    $id->uraian_existing_control = '-';
                    $id->id_sub_unsur = NULL;
                    $id->existing_control_memadai = 'T';
                    $id->id_matriks_residual = Input::get('id_matriks_inherent');
                }
                
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1analisis.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['kemungkinan_inherent'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_inherent'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_inherent'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['existing_control'] = ["T" => "Tidak", "Y" => "Ya",];
        $this->data['id_sub_unsur'] = RefSubUnsurSPIP::pluck('nama_sub_unsur','id_sub_unsur');
        $this->data['existing_control_memadai'] = ["T" => "Tidak", "Y" => "Ya",];
        $this->data['kemungkinan_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_residual'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $this->data['analisis'] = AnalisisRisiko::find($id);
        $this->data['skor'] = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent', 
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->where('t_analisis_risiko.id_analisis', $id)
                ->first();
        $this->data['risiko'] = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->where('t_analisis_risiko.id_analisis', $id)
                ->first();

        return view('lini1analisis.editanunit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {

            $id_matriks_inherent = RefMatriks::where('id_matriks', $request->input('id_matriks_inherent'))->firstOrFail();
            $skor_inherent = $id_matriks_inherent->skor_risiko;

            $id_matriks_residual = RefMatriks::where('id_matriks', $request->input('id_matriks_residual'))->firstOrFail();
            $skor_residual = $id_matriks_residual->skor_risiko;

            $rules = array(

                'id_identifikasi' => 'required',
                'id_matriks_inherent' => 'required',
                'existing_control' => 'required',
                'existing_control_memadai' => 'required',
                'id_matriks_residual' => 'required',

            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'id_matriks_inherent.required' => 'Silahkan pilih skala kemungkinan dan dampak inherent risk.',
                            'existing_control.required' => 'Silahkan pilih status keberadaan pengendalian yang sudah ada.',
                            'existing_control_memadai.required' => 'Silahkan pilih efektivitas pengendalian yang sudah ada.',
                            'id_matriks_residual.required' => 'Silahkan pilih skala kemungkinan dan dampak residual risk.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } elseif($skor_residual > $skor_inherent) {
                return redirect()->back()->withErrors('Level Risiko Residual tidak boleh lebih tinggi daripada Level Risiko Inherent');
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = AnalisisRisiko::find($id);
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->id_matriks_inherent = Input::get('id_matriks_inherent');
                $id->existing_control = Input::get('existing_control');
                if ($id->existing_control == 'Y'){
                    $id->uraian_existing_control = Input::get('uraian_existing_control');
                    $id->id_sub_unsur = Input::get('id_sub_unsur');
                    $id->existing_control_memadai = Input::get('existing_control_memadai');
                    $id->id_matriks_residual = Input::get('id_matriks_residual');
                } else {
                    $id->uraian_existing_control = '-';
                    $id->id_sub_unsur = NULL;
                    $id->existing_control_memadai = 'T';
                    $id->id_matriks_residual = Input::get('id_matriks_inherent');
                }
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1analisis.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSkorAnUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $analisis = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        return view('lini1analisis.skoranunit', compact('analisis','unit'));
        }
    }


}
