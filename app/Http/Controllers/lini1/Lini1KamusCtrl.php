<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;


class Lini1KamusCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kamus = DB::table('ref_bagan_risiko')
                ->select('ref_bagan_risiko.id_bagan_risiko as id', 'ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko', 'ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('ref_kategori_risiko', 'ref_bagan_risiko.id_kategori_risiko', '=', 'ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->orderBy('id_kategori_risiko')
                ->orderBy('nama_bagan_risiko')
                ->get();

        return view('lini1kamus.index', compact('kamus'));
    }

    public function cetak()
    {
        $kamus = DB::table('ref_bagan_risiko')
                ->select('ref_bagan_risiko.id_bagan_risiko as id', 'ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko', 'ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('ref_kategori_risiko', 'ref_bagan_risiko.id_kategori_risiko', '=', 'ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->orderBy('id_kategori_risiko')
                ->orderBy('nama_bagan_risiko')
                ->get();

        $pdf = PDF::loadView('lini1kamus.cetak',  compact('kamus'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('kamus_risiko.pdf', array('Attachment' => false));
        exit(0);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
