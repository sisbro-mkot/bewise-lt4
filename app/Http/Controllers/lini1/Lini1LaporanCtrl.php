<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefMatriks;


class Lini1LaporanCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1laporan.index', compact('unit'));

    }

    public function cetak()
    {
        $analisis = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent', 
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 
                    't_analisis_risiko.existing_control as existing_control', 
                    't_analisis_risiko.uraian_existing_control as uraian_existing_control', 
                    't_analisis_risiko.existing_control_memadai as existing_control_memadai',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->orderBy('kode_identifikasi_risiko','asc')
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Carbon::now()->year;

        $pdf = PDF::loadView('lini1analisis.cetak',  compact('analisis', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('analisis_risiko.pdf', array('Attachment' => false));
        exit(0);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
