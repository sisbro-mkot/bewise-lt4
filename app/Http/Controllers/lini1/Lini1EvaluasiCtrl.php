<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\RefJnsSebab;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\PenyebabRCA;


class Lini1EvaluasiCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $penyebab = DB::table('t_penyebab_rca')
                    ->select('t_penyebab_rca.id_penyebab as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_penyebab_1 as nama_penyebab_1', 't_penyebab_rca.nama_penyebab_2 as nama_penyebab_2', 't_penyebab_rca.nama_penyebab_3 as nama_penyebab_3', 't_penyebab_rca.nama_penyebab_4 as nama_penyebab_4', 't_penyebab_rca.nama_penyebab_5 as nama_penyebab_5', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 'ref_jns_sebab.nama_jns_sebab as nama_jns_sebab')
                    ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                    ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $risiko = DB::table('t_identifikasi_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->count();

        return view('lini1evaluasi.index', compact('penyebab','unit','risiko'));

    }

    public function pilihRisikoUnit2($id) 
    {  

            $risiko = DB::table('t_identifikasi_risiko')
                ->select('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->get();
            return json_encode($risiko);
        
    }


    public function cetakRisiko()
    {
        $evaluasi = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->whereRaw('(ref_matriks_residual.skor_risiko > ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?', Auth::user()->user_nip)
                ->orderBy('skor_risiko_residual', 'desc')
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $selera = DB::table('ref_data_umum')
                ->select('ref_data_umum.skor_selera as skor_selera')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Carbon::now()->year;

        $pdf = PDF::loadView('lini1evaluasi.cetakrisiko',  compact('evaluasi', 'unit', 'tahun', 'selera'));
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream('evaluasi_risiko_prioritas.pdf', array('Attachment' => false));
        exit(0);
    }

    public function cetakRCA()
    {
        $evaluasi = DB::table('t_penyebab_rca')
                    ->select('t_penyebab_rca.id_penyebab as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_penyebab_1 as nama_penyebab_1', 't_penyebab_rca.nama_penyebab_2 as nama_penyebab_2', 't_penyebab_rca.nama_penyebab_3 as nama_penyebab_3', 't_penyebab_rca.nama_penyebab_4 as nama_penyebab_4', 't_penyebab_rca.nama_penyebab_5 as nama_penyebab_5', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab')
                    ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                    ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                    ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Carbon::now()->year;

        $pdf = PDF::loadView('lini1evaluasi.cetakrca',  compact('evaluasi', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('evaluasi_risiko_rca.pdf', array('Attachment' => false));
        exit(0);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi', 't_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['id_jns_sebab'] = RefJnsSebab::pluck('nama_jns_sebab', 'id_jns_sebab');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1evaluasi.createevunit', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(

                'id_identifikasi' => 'required',
                'nama_penyebab_1' => 'required',
                'nama_akar_penyebab' => 'required',
                'id_jns_sebab' => 'required',
                
            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'nama_penyebab_1.required' => 'Silahkan isi nama prnyebab.',
                            'nama_akar_penyebab.required' => 'Silahkan isi nama akar penyebab.',
                            'id_jns_sebab.required' => 'Silahkan pilih jenis penyebab.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new PenyebabRCA;
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->nama_penyebab_1 = Input::get('nama_penyebab_1');
                $id->nama_penyebab_2 = Input::get('nama_penyebab_2');
                $id->nama_penyebab_3 = Input::get('nama_penyebab_3');
                $id->nama_penyebab_4 = Input::get('nama_penyebab_4');
                $id->nama_penyebab_5 = Input::get('nama_penyebab_5');
                $id->nama_akar_penyebab = Input::get('nama_akar_penyebab');
                $id->id_jns_sebab = Input::get('id_jns_sebab');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1evaluasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi', 't_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['id_jns_sebab'] = RefJnsSebab::pluck('nama_jns_sebab', 'id_jns_sebab');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $this->data['evaluasi'] = PenyebabRCA::find($id);

        return view('lini1evaluasi.editevunit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $rules = array(

                'id_identifikasi' => 'required',
                'nama_penyebab_1' => 'required',
                'nama_akar_penyebab' => 'required',
                'id_jns_sebab' => 'required',
                
            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'nama_penyebab_1.required' => 'Silahkan isi nama prnyebab.',
                            'nama_akar_penyebab.required' => 'Silahkan isi nama akar penyebab.',
                            'id_jns_sebab.required' => 'Silahkan pilih jenis penyebab.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = PenyebabRCA::find($id);
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->nama_penyebab_1 = Input::get('nama_penyebab_1');
                $id->nama_penyebab_2 = Input::get('nama_penyebab_2');
                $id->nama_penyebab_3 = Input::get('nama_penyebab_3');
                $id->nama_penyebab_4 = Input::get('nama_penyebab_4');
                $id->nama_penyebab_5 = Input::get('nama_penyebab_5');
                $id->nama_akar_penyebab = Input::get('nama_akar_penyebab');
                $id->id_jns_sebab = Input::get('id_jns_sebab');
                $id->updated_at = $now;
                $id->user_delete = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1evaluasi.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getPenyebabUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.s_kd_jabdetail', '=', 'ref_data_umum.s_kd_jabdetail_pemilik')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $mnid = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 1)
                        ->count();
            $myid = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 2)
                        ->count();
            $mdid = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 3)
                        ->count();
            $mrid = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 4)
                        ->count();
            $mcid = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 5)
                        ->count();
            $exid = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 6)
                        ->count();
            $mnmt = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 1)
                        ->get()
                        ->count();
            $mymt = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 2)
                        ->get()
                        ->count();
            $mdmt = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 3)
                        ->get()
                        ->count();
            $mrmt = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 4)
                        ->get()
                        ->count();
            $mcmt = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 5)
                        ->get()
                        ->count();
            $exmt = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->where('t_penyebab_rca.id_jns_sebab', 6)
                        ->get()
                        ->count();
        return view('lini1evaluasi.penyebabunit', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','insidenk','insidenr','insidenp','rtpjadwal','rtprealisasi','mnid','myid','mdid','mrid','mcid','exid','mnmt','mymt','mdmt','mrmt','mcmt','exmt'));
        }
    }

    public function getPenyebabListUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $evaluasi = DB::table('t_penyebab_rca')
                        ->select('t_penyebab_rca.id_penyebab as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1evaluasi.penyebablistunit', compact('evaluasi','unit'));
        }
    }

    public function getFilterPenyebabListUnit($id)
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $evaluasi = DB::table('t_penyebab_rca')
                        ->select('t_penyebab_rca.id_penyebab as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_penyebab_rca.id_jns_sebab as id_jns_sebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('t_penyebab_rca.id_jns_sebab', $id)
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        return view('lini1evaluasi.penyebablistunit', compact('evaluasi','unit'));
        }
    }



}
