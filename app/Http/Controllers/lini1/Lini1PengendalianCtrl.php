<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\RefSubUnsurSPIP;
use App\Models\dashboard\Pengendalian;
use App\Models\dashboard\AnalisisRisiko;


class Lini1PengendalianCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengendalian = DB::table('t_pengendalian')
                ->select('t_pengendalian.id_pengendalian as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_pengendalian.nama_pengendalian','ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur','t_pengendalian.tahun as tahun')
                ->join('t_identifikasi_risiko', 't_pengendalian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_sub_unsur_spip', 't_pengendalian.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();

        return view('lini1pengendalian.index', compact('pengendalian'));

    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah($id)
    {
            $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_analisis_risiko.id_analisis', $id)
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
            $this->data['id_sub_unsur'] = RefSubUnsurSPIP::pluck('nama_sub_unsur','id_sub_unsur');
            $this->data['existing_control'] = ["T" => "Tidak", "Y" => "Ya",];
            $this->data['analisis'] = AnalisisRisiko::find($id);

            return view('lini1pengendalian.createcontrolunit', $this->data); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Auth::check())
        {

            $rules = array(

                'id_identifikasi' => 'required',
                'nama_pengendalian' => 'required',

            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'nama_pengendalian.required' => 'Silahkan isi/ketik nama pengendalian.',

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new Pengendalian;
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->nama_pengendalian = Input::get('nama_pengendalian');
                $id->id_sub_unsur = Input::get('id_sub_unsur');
                $id->tahun = Carbon::now()->subYear()->format('Y');
           
                $id->user_create = Auth::user()->user_nip;

                $id->save();

                $analisis = DB::table('t_analisis_risiko')
                            ->select('t_analisis_risiko.id_analisis as id_analisis')
                            ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                            ->where('t_analisis_risiko.id_identifikasi', '=', $id->id_identifikasi)
                            ->first(); 

                Alert::success('Data telah ditambahkan.', 'Selamat');

                if (Input::get('existing_control') == "Y") {
                    return redirect()->route('lini1pengendalian.tambah', $analisis->id_analisis);
                } else {
                    return redirect()->route('lini1residual.edit', $analisis->id_analisis);
                }
                
            }
        } else {
            return redirect()->route('lini1analisis');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['pengendalian'] = Pengendalian::find($id);
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['id_sub_unsur'] = RefSubUnsurSPIP::pluck('nama_sub_unsur','id_sub_unsur');
        $this->data['risiko'] = DB::table('t_pengendalian')
                ->select('t_pengendalian.id_pengendalian as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_pengendalian.nama_pengendalian','ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur','t_pengendalian.tahun as tahun')
                ->join('t_identifikasi_risiko', 't_pengendalian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_sub_unsur_spip', 't_pengendalian.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->where('t_pengendalian.id_pengendalian', $id)
                ->first();

        
        return view('lini1pengendalian.editcontrolunit', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
      if (Auth::check())
        {
            $rules = array(

                'id_identifikasi' => 'required',
                'nama_pengendalian' => 'required',

            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'nama_pengendalian.required' => 'Silahkan isi/ketik nama pengendalian.',
                        ];

            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $idn = Pengendalian::find($id);
                $idn->nama_pengendalian = Input::get('nama_pengendalian');
                $idn->id_sub_unsur = Input::get('id_sub_unsur');
                $idn->user_update = Auth::user()->user_nip;

                $idn->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1analisis.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
