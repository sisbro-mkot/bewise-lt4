<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefMatriks;

class Lini1TreatedCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $analisis = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 't_analisis_risiko.existing_control as existing_control', 't_analisis_risiko.uraian_existing_control as uraian_existing_control', 't_analisis_risiko.existing_control_memadai as existing_control_memadai', 'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();

        return view('lini1treated.index', compact('analisis'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['s_nmjabdetail'] = DB::table('t_analisis_risiko')
                                        ->select('wm_jabdetail.s_nmjabdetail as nama')
                                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                                        ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                                        ->where('t_analisis_risiko.id_analisis', $id)
                                        ->first();
        $this->data['nama_bagan_risiko'] = DB::table('t_analisis_risiko')
                                        ->select('ref_bagan_risiko.nama_bagan_risiko as risiko')
                                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                                        ->where('t_analisis_risiko.id_analisis', $id)
                                        ->first();
        $this->data['residual'] = DB::table('t_analisis_risiko')
                                ->select('ref_matriks.skor_dampak as skor_dampak', 'ref_matriks.skor_kemungkinan as skor_kemungkinan', 'ref_matriks.skor_risiko as skor_risiko')
                                ->leftjoin('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                                ->where('t_analisis_risiko.id_analisis', $id)
                                ->first();
        $this->data['treated'] = DB::table('t_rtp')
                                ->select('t_rtp.id_rtp as id', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.respon_risiko as respon_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian')
                                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                                ->where('t_analisis_risiko.id_analisis', $id)
                                ->get();
        $this->data['kemungkinan_treated'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_treated'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_treated'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['analisis'] = AnalisisRisiko::find($id);

        return view('lini1treated.edit', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
      if (Auth::check())
        {
            $rules = array(

                'kemungkinan_treated' => 'required',
                'dampak_treated' => 'required',
                'id_matriks_treated' => 'required',

            );
            $messages = [
                            'kemungkinan_treated.required' => 'Silahkan isi Level Kemungkinan Treated.',
                            'dampak_treated.required' => 'Silahkan isi Level Dampak Treated.',
                            'id_matriks_treated.required' => 'Silahkan isi Level Risiko Treated.',
                        ];

            $id_matriks_residual = AnalisisRisiko::leftjoin('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                                ->where('t_analisis_risiko.id_analisis', $id)
                                ->firstOrFail();
            $skor_residual = $id_matriks_residual->skor_risiko;

            $id_matriks_treated = RefMatriks::where('id_matriks', $request->input('id_matriks_treated'))->firstOrFail();
            $skor_treated = $id_matriks_treated->skor_risiko;

            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } elseif($skor_treated > $skor_residual) {
                return redirect()->back()->withErrors('Level Risiko Treated tidak boleh lebih tinggi daripada Level Risiko Residual');
            } else {

                $idn = AnalisisRisiko::find($id);
                $idn->id_matriks_treated = Input::get('id_matriks_treated');
                $idn->user_update = Auth::user()->user_nip;

                $idn->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1respons.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
