<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\Pemantauan;


class Lini1RealisasiCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $realisasi = DB::table('t_pemantauan')
                ->select('t_pemantauan.id_pemantauan as id', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'periode_rencana.nama_periode as nama_periode_rencana', 't_pemantauan.realisasi_waktu as realisasi_waktu', 't_pemantauan.nama_hambatan as nama_hambatan')
                ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $rca = DB::table('t_rtp')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->count();
        $data = DB::table('t_rtp')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                ->whereNull('t_pemantauan.id_rtp')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->count();

        return view('lini1realisasi.index', compact('realisasi','unit','rca','data'));

    }
 
    public function pilihRTP($id) 
    {  

            $rtp = DB::table('t_rtp')
                ->select('t_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_rtp.id_rtp as id_rtp', 't_rtp.id_periode as id_periode', 'periode_rencana.nama_periode as nama_periode_rencana')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->whereNull('t_pemantauan.id_rtp')
                ->get();
            return json_encode($rtp);
        
    }

    public function pilihPeriodeRTP($id) 
    {  

            $periode = DB::table('t_rtp')
                ->select('t_rtp.id_periode as id_periode', 'periode_rencana.nama_periode as nama_periode_rencana')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->where('t_rtp.id_rtp', $id)
                ->get();
            return json_encode($periode);
        
    }


    public function cetak()
    {
        $respons = DB::table('t_pemantauan')
                ->select('t_pemantauan.id_pemantauan as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'periode_rencana.nama_periode as nama_periode_rencana', 't_pemantauan.realisasi_waktu as realisasi_waktu', 'ref_output.nama_output as nama_output', 't_pemantauan.nama_hambatan as nama_hambatan')
                ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Carbon::now()->year;

        $triwulan = Carbon::now()->quarter;

        $pdf = PDF::loadView('lini1realisasi.cetak',  compact('respons', 'unit', 'tahun', 'triwulan'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('respons_risiko_realisasi.pdf', array('Attachment' => false));
        exit(0);
    }



 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            
            $this->data['id_rtp'] = DB::table('t_rtp')
                                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                                ->whereNull('t_pemantauan.id_rtp')
                                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                                ->pluck('t_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_rtp.id_rtp as id_rtp');
            $this->data['periode_rencana'] = DB::table('t_rtp')
                                ->select('ref_periode.nama_periode as nama_periode', 't_rtp.id_periode as periode_rencana')
                                ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                                ->first();
            $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

            return view('lini1realisasi.createrealunit', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (Auth::check())
        {
            $rules = array(

                'id_rtp' => 'required',
                'realisasi_waktu' => 'required',

            );
            $messages = [
                            'id_rtp.required' => 'Silahkan pilih nama kegiatan pengendalian.',
                            'realisasi_waktu.required' => 'Silahkan isi waktu realisasi.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new Pemantauan;
                $id->id_rtp = Input::get('id_rtp');
                $id->realisasi_waktu = Input::get('realisasi_waktu');
                $id->nama_hambatan = Input::get('nama_hambatan');
                $id->user_create = Auth::user()->user_nip;

                $dt = Carbon::parse($id->realisasi_waktu);
                $dn = $dt->quarter;
                $id->id_periode = $dn + 2;

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1realisasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $this->data['id_rtp'] = DB::table('t_rtp')
                                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                                ->pluck('t_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_rtp.id_rtp as id_rtp');
            $this->data['periode_rencana'] = DB::table('t_rtp')
                                ->select('ref_periode.nama_periode as nama_periode', 't_rtp.id_periode as periode_rencana')
                                ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                                ->first();
            $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
            $this->data['realisasi'] = Pemantauan::find($id);
           $this->data['respons'] = DB::table('t_pemantauan')
                ->select('t_pemantauan.id_pemantauan as id', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian')
                ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->where('t_pemantauan.id_pemantauan', $id)
                ->first();

            return view('lini1realisasi.editrealunit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if (Auth::check())
        {
            $rules = array(

                'id_rtp' => 'required',
                'realisasi_waktu' => 'required',

            );
            $messages = [
                            'id_rtp.required' => 'Silahkan pilih nama kegiatan pengendalian.',
                            'realisasi_waktu.required' => 'Silahkan isi waktu realisasi.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = Pemantauan::find($id);
                $id->id_rtp = Input::get('id_rtp');
                $id->realisasi_waktu = Input::get('realisasi_waktu');
                $id->nama_hambatan = Input::get('nama_hambatan');
                $id->updated_at = $now;
                $id->user_delete = Auth::user()->user_nip;

                $dt = Carbon::parse($id->realisasi_waktu);
                $dn = $dt->quarter;
                $id->id_periode = $dn + 2;

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1realisasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
