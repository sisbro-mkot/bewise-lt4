<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefJnsKonteks;
use App\Models\dashboard\RefKonteks;
use App\Models\dashboard\PenetapanKonteks;

class Lini1TetapKonteksCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
        $konteks = DB::table('t_penetapan_konteks')
                ->select('t_penetapan_konteks.id_penetapan_konteks as id', 't_penetapan_konteks.tahun as tahun', 'ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik', 't_penetapan_konteks.id_konteks as id_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.ket_konteks as ket_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->whereRaw('vw_renpeglast.niplama = ? AND ISNULL(t_penetapan_konteks.catatan_hapus)', Auth::user()->user_nip)
                ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1tetapkonteks.index', compact('konteks','unit'));

    }
 
    public function pilihKonteks($id) 
    {  

            $konteks = DB::table('ref_konteks')
                ->select('nama_konteks', 'id_konteks')
                ->where('id_jns_konteks', $id)
                ->get();
            return json_encode($konteks);
        
    }
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $this->data['tahun'] = Carbon::now()->year;
        $this->data['id_data_umum'] = DB::table('ref_data_umum')
                            ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                            ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                            ->pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');
        $this->data['id_jns_konteks'] = RefJnsKonteks::pluck('nama_jns_konteks as nama_jns_konteks','id_jns_konteks');
        $this->data['id_konteks'] = RefKonteks::pluck('nama_konteks as nama_konteks','id_konteks');

        return view('lini1tetapkonteks.createtkunit', $this->data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                'id_data_umum' => 'required',
                'id_konteks' => 'required',

            );
            $messages = [
                            'tahun.required' => 'Silahkan isi tahun.',
                            'id_data_umum.required' => 'Silahkan pilih data umum.',
                            'id_konteks.required' => 'Silahkan pilih jenis dan nama konteks.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new PenetapanKonteks;
                $id->tahun = Input::get('tahun');
                $id->id_data_umum = Input::get('id_data_umum');
                $id->id_konteks = Input::get('id_konteks');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1tetapkonteks.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
