<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefSubUnsurSPIP;
use App\Models\dashboard\JabDetail;
use App\Models\dashboard\RefOutput;
use App\Models\dashboard\RefPeriode;
use App\Models\dashboard\RTP;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefMatriks;

class Lini1ResponsCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $rtp = DB::table('t_rtp')
            ->select('t_rtp.id_rtp as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.respon_risiko as respon_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'ref_output.nama_output as nama_output', 'ref_periode.nama_periode as nama_periode')
            ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
            ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
            ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
            ->join('ref_sub_unsur_spip', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
            ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
            ->join('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
            ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
            ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
            ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
            ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
            ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
            ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $sebab = DB::table('t_penyebab_rca')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->count(); 


        return view('lini1respons.index', compact('rtp','unit','sebab'));

    }

    public function pilihSebab($id) 
    {  

            $sebab = DB::table('t_penyebab_rca')
                ->select('t_penyebab_rca_kode.kode_penyebab as kode_penyebab', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.s_kd_jabdetail', $id)
                ->get();
            return json_encode($sebab);
        
    }


    public function cetak()
    {
        $respons = DB::table('t_rtp')
                    ->select('t_rtp.id_rtp as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.respon_risiko as respon_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'ref_output.nama_output as nama_output', 'ref_periode.nama_periode as nama_periode')
                    ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                    ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                    ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('ref_sub_unsur_spip', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                    ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                    ->join('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
                    ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Carbon::now()->year;

        $pdf = PDF::loadView('lini1respons.cetak',  compact('respons', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('respons_risiko_rtp.pdf', array('Attachment' => false));
        exit(0);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->data['id_penyebab'] = DB::table('t_penyebab_rca')
                ->select('t_penyebab_rca_kode.kode_penyebab as kode_penyebab', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab')
                ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['respon_risiko'] = ["K" => "Mengurangi Kemungkinan",
                                        "D" => "Mengurangi Dampak", 
                                        "B" => "Mengurangi Kemungkinan dan Dampak"];
        $this->data['id_sub_unsur'] = RefSubUnsurSPIP::pluck('nama_sub_unsur','id_sub_unsur'); 
        $this->data['s_kd_jabdetail_pic'] =  DB::table('wm_jabdetail')
                ->join('vw_renpeglast', 'wm_jabdetail.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->pluck('wm_jabdetail.s_nmjabdetail as s_nmjabdetail','wm_jabdetail.s_kd_jabdetail as s_kd_jabdetail');
        $this->data['id_output'] = RefOutput::pluck('nama_output','id_output');
        $this->data['id_periode'] = RefPeriode::where('jenis_periode', '=', 'tw')->pluck('nama_periode','id_periode');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1respons.createrespunit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (Auth::check())
        {
            $rules = array(

                'id_penyebab' => 'required',
                'respon_risiko' => 'required',
                'id_sub_unsur' => 'required',
                's_kd_jabdetail_pic' => 'required',
                'id_output' => 'required',
                'id_periode' => 'required',

            );
            $messages = [
                            'id_penyebab.required' => 'Silahkan pilih nama penyebab.',
                            'respon_risiko.required' => 'Silahkan pilih jenis respons risiko.',
                            'id_sub_unsur.required' => 'Silahkan pilih nama Sub Unsur SPIP.',
                            's_kd_jabdetail_pic.required' => 'Silahkan pilih penanggung jawab kegiatan.',
                            'id_output.required' => 'Silahkan pilih indikator output.',
                            'id_periode.required' => 'Silahkan pilih periode rencana.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $currentYear = Carbon::now()->format('Y');
                $Q1end = Carbon::createMidnightDate($currentYear,3,31);
                $Q2end = Carbon::createMidnightDate($currentYear,6,30);
                $Q3end = Carbon::createMidnightDate($currentYear,9,30);
                $Q4end = Carbon::createMidnightDate($currentYear,12,31);

                $id = new RTP;
                $id->id_penyebab = Input::get('id_penyebab');
                $id->respon_risiko = Input::get('respon_risiko');
                $id->kegiatan_pengendalian = Input::get('kegiatan_pengendalian');
                $id->id_sub_unsur = Input::get('id_sub_unsur');
                $id->s_kd_jabdetail_pic = Input::get('s_kd_jabdetail_pic');
                $id->id_output = Input::get('id_output');
                $id->id_periode = Input::get('id_periode');
                $id->user_create = Auth::user()->user_nip;

                if ($id->id_periode == 3){
                    $id->deadline_rtp = $Q1end;
                } elseif ($id->id_periode == 4){
                    $id->deadline_rtp = $Q2end;
                } elseif ($id->id_periode == 5){
                    $id->deadline_rtp = $Q3end;
                } else {
                    $id->deadline_rtp = $Q4end;
                }
                
                $analisis = DB::table('t_analisis_risiko')
                            ->select('t_analisis_risiko.id_analisis as id_analisis')
                            ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                            ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                            ->where('t_penyebab_rca.id_penyebab', '=', $id->id_penyebab)
                            ->first(); 

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1treated.edit', $analisis->id_analisis);
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['id_penyebab'] = DB::table('t_penyebab_rca')
                ->select('t_penyebab_rca_kode.kode_penyebab as kode_penyebab', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab')
                ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['respon_risiko'] = ["K" => "Mengurangi Kemungkinan",
                                        "D" => "Mengurangi Dampak", 
                                        "B" => "Mengurangi Kemungkinan dan Dampak"];
        $this->data['id_sub_unsur'] = RefSubUnsurSPIP::pluck('nama_sub_unsur','id_sub_unsur');
        $this->data['s_kd_jabdetail_pic'] =  DB::table('wm_jabdetail')
                ->join('vw_renpeglast', 'wm_jabdetail.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->pluck('wm_jabdetail.s_nmjabdetail as s_nmjabdetail','wm_jabdetail.s_kd_jabdetail as s_kd_jabdetail');
        $this->data['id_output'] = RefOutput::pluck('nama_output','id_output');
        $this->data['id_periode'] = RefPeriode::where('jenis_periode', '=', 'tw')->pluck('nama_periode','id_periode');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $this->data['respons'] = RTP::find($id);

        return view('lini1respons.editrespunit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $rules = array(

                'id_penyebab' => 'required',
                'respon_risiko' => 'required',
                'id_sub_unsur' => 'required',
                's_kd_jabdetail_pic' => 'required',
                'id_output' => 'required',
                'id_periode' => 'required',

            );
            $messages = [
                            'id_penyebab.required' => 'Silahkan pilih nama penyebab.',
                            'respon_risiko.required' => 'Silahkan pilih jenis respons risiko.',
                            'id_sub_unsur.required' => 'Silahkan pilih nama Sub Unsur SPIP.',
                            's_kd_jabdetail_pic.required' => 'Silahkan pilih penanggung jawab kegiatan.',
                            'id_output.required' => 'Silahkan pilih indikator output.',
                            'id_periode.required' => 'Silahkan pilih periode rencana.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $currentYear = Carbon::now()->format('Y');
                $now = Carbon::now('Asia/Jakarta');
                $Q1end = Carbon::createMidnightDate($currentYear,3,31);
                $Q2end = Carbon::createMidnightDate($currentYear,6,30);
                $Q3end = Carbon::createMidnightDate($currentYear,9,30);
                $Q4end = Carbon::createMidnightDate($currentYear,12,31);

                $id = RTP::find($id);
                $id->id_penyebab = Input::get('id_penyebab');
                $id->respon_risiko = Input::get('respon_risiko');
                $id->kegiatan_pengendalian = Input::get('kegiatan_pengendalian');
                $id->id_sub_unsur = Input::get('id_sub_unsur');
                $id->s_kd_jabdetail_pic = Input::get('s_kd_jabdetail_pic');
                $id->id_output = Input::get('id_output');
                $id->id_periode = Input::get('id_periode');
                $id->updated_at = $now;
                $id->user_delete = Auth::user()->user_nip;

                if ($id->id_periode == 3){
                    $id->deadline_rtp = $Q1end;
                } elseif ($id->id_periode == 4){
                    $id->deadline_rtp = $Q2end;
                } elseif ($id->id_periode == 5){
                    $id->deadline_rtp = $Q3end;
                } else {
                    $id->deadline_rtp = $Q4end;
                }
                
                $analisis = DB::table('t_analisis_risiko')
                            ->select('t_analisis_risiko.id_analisis as id_analisis')
                            ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                            ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                            ->where('t_penyebab_rca.id_penyebab', '=', $id->id_penyebab)
                            ->first(); 

                $id->save();

                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1treated.edit', $analisis->id_analisis);
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getRTPUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $triwulankini = Carbon::now()->quarter;
            $tricount = DB::table('t_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->whereRaw(('t_rtp.id_periode - 2 = ?'), $triwulankini)
                        ->count();
            $terlambat = DB::table('t_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->whereRaw('vw_renpeglast.niplama = ? AND (DATEDIFF(t_rtp.deadline_rtp, t_pemantauan.realisasi_waktu) < 0 OR (ISNULL(realisasi_waktu) AND DATEDIFF(t_rtp.deadline_rtp, CURRENT_TIMESTAMP) < 0))', Auth::user()->user_nip)
                        ->count();
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.s_kd_jabdetail', '=', 'ref_data_umum.s_kd_jabdetail_pemilik')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();

        return view('lini1respons.rtpunit', compact('triwulankini','tricount','terlambat','risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp'));
        }
    }

    public function getRTPListUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $respons = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1respons.rtplistunit', compact('respons','unit'));
        }
    }

    public function getRTPListCurUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $triwulankini = Carbon::now()->quarter;
            $respons = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->whereRaw(('t_rtp.id_periode - 2 = ?'), $triwulankini)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1respons.rtplistcurunit', compact('respons','triwulankini','unit'));
        }
    }

    public function getMonitorUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $respons = DB::table('t_pemantauan')
                        ->select('t_pemantauan.id_pemantauan as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode', 't_pemantauan.realisasi_waktu as realisasi_waktu')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1respons.monitorunit', compact('respons','unit'));
        }
    }

    public function getResLambatUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {

            $respons = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_pemantauan.nama_hambatan')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->whereRaw('vw_renpeglast.niplama = ? AND (DATEDIFF(t_rtp.deadline_rtp, t_pemantauan.realisasi_waktu) < 0 OR (ISNULL(realisasi_waktu) AND DATEDIFF(t_rtp.deadline_rtp, CURRENT_TIMESTAMP) < 0))', Auth::user()->user_nip)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1respons.reslambatunit', compact('respons','unit'));
        }
    }




}
