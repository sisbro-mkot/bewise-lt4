<?php
 
namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\dashboard\IdentifikasiRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefBaganRisiko;
 
class Lini1IdentifikasiCtrl extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.tahun as tahun', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_identifikasi_risiko.nama_dampak as nama_dampak','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $konteks = DB::table('t_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->whereRaw('vw_renpeglast.niplama = ? AND ISNULL(t_penetapan_konteks.catatan_hapus)', Auth::user()->user_nip)
                ->count();


        return view('lini1identifikasi.index', compact('identifikasi','unit','konteks'));
    }

    public function cetak()
    {
        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.tahun as tahun', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_identifikasi_risiko.nama_dampak as nama_dampak','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Carbon::now()->year;

        $pdf = PDF::loadView('lini1identifikasi.cetak',  compact('identifikasi', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('identifikasi_risiko.pdf', array('Attachment' => false));
        exit(0);
    }

    public function excel()
    {
        $kolom = ['No.','Jenis Konteks', 'Nama Konteks', 'Kode Risiko','Pernyataan Risiko','Uraian Dampak'];
        $baris = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 
                    'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 
                    'ref_konteks.nama_konteks as nama_konteks',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $writer = new Xlsx($spreadsheet);
        $filename = 'identifikasi_risiko.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $this->data['tahun'] = Carbon::now()->year;
        $this->data['s_kd_jabdetail'] = DB::table('ref_data_umum')
                ->select('ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','ref_data_umum.s_kd_jabdetail_pemilik as s_kd_jabdetail')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['id_penetapan_konteks'] = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=','ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->whereRaw('vw_renpeglast.niplama = ? AND ISNULL(t_penetapan_konteks.catatan_hapus)', Auth::user()->user_nip)
                ->get();
        $this->data['id_bagan_risiko'] = RefBaganRisiko::pluck('nama_bagan_risiko as nama_bagan_risiko','id_bagan_risiko');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1identifikasi.createidunit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                's_kd_jabdetail' => 'required',
                'id_penetapan_konteks' => 'required',
                'id_bagan_risiko' => 'required',
                'nama_dampak' => 'required',
            );
            $messages = [
                            'tahun.required' => 'Silahkan isi tahun.',
                            's_kd_jabdetail.required' => 'Silahkan pilih pemilik risiko.',
                            'id_penetapan_konteks.required' => 'Silahkan pilih nama konteks.',
                            'id_bagan_risiko.required' => 'Silahkan pilih nama risiko.',
                            'nama_dampak.required' => 'Silahkan isi nama dampak.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new IdentifikasiRisiko;
                $id->tahun = Input::get('tahun');
                $id->s_kd_jabdetail = Input::get('s_kd_jabdetail');
                $id->id_penetapan_konteks = Input::get('id_penetapan_konteks');
                $id->id_bagan_risiko = Input::get('id_bagan_risiko');
                $id->nama_dampak = Input::get('nama_dampak');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1identifikasi.index');
            }
        } else {
            return redirect()->back();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['tahun'] = Carbon::now()->year;
        $this->data['s_kd_jabdetail'] = DB::table('ref_data_umum')
                ->select('ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','ref_data_umum.s_kd_jabdetail_pemilik as s_kd_jabdetail')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['id_penetapan_konteks'] = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=','ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->whereRaw('vw_renpeglast.niplama = ? AND ISNULL(t_penetapan_konteks.catatan_hapus)', Auth::user()->user_nip)
                ->get();
        $this->data['id_bagan_risiko'] = RefBaganRisiko::pluck('nama_bagan_risiko as nama_bagan_risiko','id_bagan_risiko');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $this->data['identifikasi'] = IdentifikasiRisiko::find($id);

        return view('lini1identifikasi.editidunit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                's_kd_jabdetail' => 'required',
                'id_penetapan_konteks' => 'required',
                'id_bagan_risiko' => 'required',
                'nama_dampak' => 'required',
            );
            $messages = [
                            'tahun.required' => 'Silahkan isi tahun.',
                            's_kd_jabdetail.required' => 'Silahkan pilih pemilik risiko.',
                            'id_penetapan_konteks.required' => 'Silahkan pilih nama konteks.',
                            'id_bagan_risiko.required' => 'Silahkan pilih nama risiko.',
                            'nama_dampak.required' => 'Silahkan isi nama dampak.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = IdentifikasiRisiko::find($id);
                $id->tahun = Input::get('tahun');
                $id->s_kd_jabdetail = Input::get('s_kd_jabdetail');
                $id->id_penetapan_konteks = Input::get('id_penetapan_konteks');
                $id->id_bagan_risiko = Input::get('id_bagan_risiko');
                $id->nama_dampak = Input::get('nama_dampak');
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1identifikasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getHeatmapUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                      ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                      ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                      ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                      ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                      ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.s_kd_jabdetail', '=', 'ref_data_umum.s_kd_jabdetail_pemilik')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $l1 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 1) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 1) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l2 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 2) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 2) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count(); 
            $l3 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 3) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 3) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count(); 
            $l4 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 4) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 4) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l5 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 5) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 5) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l6 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 6) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 6) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l7 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 7) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 7) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l8 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 8) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 8) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l9 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 9) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 9) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l10 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 10) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 10) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l11 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 11) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 11) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l12 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 12) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 12) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l13 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 13) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 13) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l14 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 14) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 14) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l15 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 15) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 15) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l16 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 16) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 16) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l17 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 17) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 17) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l18 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 18) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 18) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l19 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 19) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 19) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l20 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 20) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 20) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l21 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 21) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 21) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l22 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 22) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 22) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l23 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 23) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 23) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l24 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 24) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 24) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $l25 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 25) AND vw_renpeglast.niplama = ?) OR ((ref_matriks_treated.skor_risiko = 25) AND vw_renpeglast.niplama = ?)', [Auth::user()->user_nip, Auth::user()->user_nip])
                    ->count();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
            $selera = DB::table('ref_data_umum')
                    ->select('ref_data_umum.skor_selera as skor_selera')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->first();

        return view('lini1identifikasi.heatmapunit', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp','l1','l2','l3','l4','l5','l6','l7','l8','l9','l10','l11','l12','l13','l14','l15','l16','l17','l18','l19','l20','l21','l22','l23','l24','l25','unit','selera'));
        }
    }


    public function getDashRegisterUnit()
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                            })
                        ->leftjoin(DB::raw('(SELECT 
                            C.id_identifikasi AS id_identifikasi,
                            C.id_penyebab AS id_penyebab,
                            D.count_rtp AS count_rtp 
                            FROM t_penyebab_rca AS C
                            LEFT JOIN
                                (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                FROM t_rtp AS A
                                LEFT JOIN t_penyebab_rca AS B
                                ON A.id_penyebab = B.id_penyebab
                                GROUP BY B.id_penyebab) AS D
                                ON C.id_penyebab = D.id_penyebab
                                GROUP BY C.id_identifikasi) AS RTP'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                            })
                        ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                        ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
            
        return view('lini1identifikasi.registerunit', compact('identifikasi','unit'));
        }
    }

    public function getFilterRegisterUnit($id)
    {
        if(Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10') {
            
            $identifikasi = DB::table('t_identifikasi_risiko')
                            ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                            ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                            ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                            ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                            ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                            ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                            ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                            ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                            ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                                })
                            ->leftjoin(DB::raw('(SELECT 
                                C.id_identifikasi AS id_identifikasi,
                                C.id_penyebab AS id_penyebab,
                                D.count_rtp AS count_rtp 
                                FROM t_penyebab_rca AS C
                                LEFT JOIN
                                    (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                    FROM t_rtp AS A
                                    LEFT JOIN t_penyebab_rca AS B
                                    ON A.id_penyebab = B.id_penyebab
                                    GROUP BY B.id_penyebab) AS D
                                    ON C.id_penyebab = D.id_penyebab
                                    GROUP BY C.id_identifikasi) AS RTP'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                                })
                            ->where([['vw_renpeglast.niplama', '=', Auth::user()->user_nip],
                                    ['ref_matriks_treated.skor_risiko', '=', $id],
                                    ])
                            ->orWhere([['vw_renpeglast.niplama', '=', Auth::user()->user_nip],
                                    ['ref_matriks_residual.skor_risiko', '=', $id],
                                    ['ref_matriks_treated.skor_risiko', '=', NULL],
                                    ])
                            ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1identifikasi.registerunit', compact('identifikasi','unit'));
        }
    }






}
