<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Tujuan;
use App\Models\adminpemda\Misi;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class TujuanCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tujuan = DB::table('tujuan_pemda')
            ->select('tujuan_pemda.id as id', 'misi_pemda.nama_misi as nama_misi', 'tujuan_pemda.nama_tujuan as nama_tujuan')
            ->join('misi_pemda', 'tujuan_pemda.misi_id', '=', 'misi_pemda.id')
            ->get();
        return view('adminpemda.tujuan.index', compact('tujuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $misi = Misi::orderBy('nama_misi', 'desc')->pluck('nama_misi', 'id');
        return view('adminpemda.tujuan.create', compact('misi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'misi_id' => 'required',
                'nama_tujuan' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('tujuan.create')
                ->withErrors($validator);
            } else {

                $tujuan = new Tujuan;
                $tujuan->misi_id = Input::get('misi_id');
                $tujuan->nama_tujuan = Input::get('nama_tujuan');
                $tujuan->save();
                Alert::success('Data tujuan telah ditambahkan.', 'Selamat');
                return redirect()->route('tujuan.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $misi = Misi::pluck('nama_misi', 'id');
        $tujuan = Tujuan::find($id);
        return view('adminpemda.tujuan.edit', compact('misi', 'tujuan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'misi_id' => 'required',
                'nama_tujuan' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $tujuan = Tujuan::find($id);
                $tujuan->misi_id = Input::get('misi_id');
                $tujuan->nama_tujuan = Input::get('nama_tujuan');
                $tujuan->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('tujuan.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tujuan = Tujuan::find($id);
        $tujuan->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('tujuan.index');
    }
}
