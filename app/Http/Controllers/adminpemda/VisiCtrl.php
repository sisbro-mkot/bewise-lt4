<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Visi;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class VisiCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $visi = DB::table('visi_pemda')
            ->select('visi_pemda.id as id', 'visi_pemda.nama_visi as nama_visi')
            ->get();
        return view('adminpemda.visi.index', compact('visi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->data['visi'] = DB::table('visi_pemda');
        return view('adminpemda.visi.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'nama_visi' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('visi.create')
                ->withErrors($validator);
            } else {

                $visi = new Visi;
                $visi->nama_visi = Input::get('nama_visi');
                $visi->save();
                Alert::success('Data visi telah ditambahkan.', 'Selamat');
                return redirect()->route('visi.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data['visi'] = Visi::find($id);
        return view('adminpemda.visi.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'nama_visi' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $visi = Visi::find($id);
                $visi->nama_visi = Input::get('nama_visi');
                $visi->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('visi.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $visi = Visi::find($id);
        $visi->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('visi.index');
    }
}
