<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Baganrisiko;
use App\Models\adminpemda\Kategori;
use App\Models\adminpemda\Perspektif;
use App\Models\adminpemda\Proses;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;
use Illuminate\Support\Facades\DB as IlluminateDB;
use App\Models\utils\Unitkerja;
use App\Models\adminpemda\Pemda;
use PDF;

class BaganrisikoCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $baganrisiko = DB::table('tbl_baganrisiko')
            ->select('tbl_baganrisiko.id as id', 'tbl_perspektif.nama_perspektif as nama_perspektif', 'tbl_kategori.nama_kategori as nama_kategori', 'tbl_proses.nama_proses as nama_proses', 'tbl_baganrisiko.nama_risiko as nama_risiko', 'tbl_baganrisiko.kode_risiko as kode_risiko')
            ->join('tbl_perspektif', 'tbl_baganrisiko.perspektif_id', '=', 'tbl_perspektif.id')
            ->join('tbl_kategori', 'tbl_baganrisiko.kategori_id', '=', 'tbl_kategori.id')
            ->join('tbl_proses', 'tbl_baganrisiko.proses_id', '=', 'tbl_proses.id')
            ->orderBy('perspektif_id','asc')
            ->orderBy('proses_id','asc')
            ->get();

        return view('adminpemda.baganrisiko.index', compact('baganrisiko'));

        // $kategori = DB::table('tbl_kategori')
        //     ->select('tbl_kategori.id as id', 'tbl_kategori.nama_kategori as nama_kategori', 'tbl_kategori.selera_risiko as selera_risiko')
        //     ->get();
        // return view('adminpemda.kategori.index', compact('kategori'));
    }
    public function pilihKelompokRisiko($id)
    {
        $data = DB::table('tbl_proses')
            ->where('id_perspektif', $id)
            ->orderBy('id','ASC')
            ->get()
            ->pluck('nama_proses', 'id');
        return json_encode($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perspektif = Perspektif::pluck('nama_perspektif', 'id');
        $kategori = Kategori::pluck('nama_kategori', 'id');
        $proses = Proses::pluck('nama_proses', 'id');
        return view('adminpemda.baganrisiko.create', compact('perspektif', 'kategori', 'proses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'perspektif_id' => 'required',
                'kategori_id' => 'required',
                'proses_id' => 'required',
                'nama_risiko' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('baganrisiko.create')
                ->withErrors($validator);
            } else {

                $risk = new Baganrisiko;
                $risk->perspektif_id = Input::get('perspektif_id');
                $risk->kategori_id = Input::get('kategori_id');
                $risk->proses_id = Input::get('proses_id');
                $risk->nama_risiko = Input::get('nama_risiko');
                $risk->save();
                Alert::success('Data risiko telah ditambahkan.', 'Selamat');
                return redirect()->route('baganrisiko.index');
            }
        } else {
            return redirect()->back();
        }
    }
    
    public function cetakBaganRisiko()
    {
            $baganrisiko = DB::table('tbl_baganrisiko')
            ->select('tbl_baganrisiko.id as id', 'tbl_perspektif.nama_perspektif as nama_perspektif', 'tbl_kategori.nama_kategori as nama_kategori', 'tbl_proses.nama_proses as nama_proses', 'tbl_baganrisiko.nama_risiko as nama_risiko', 'tbl_baganrisiko.kode_risiko as kode_risiko')
            ->join('tbl_perspektif', 'tbl_baganrisiko.perspektif_id', '=', 'tbl_perspektif.id')
            ->join('tbl_kategori', 'tbl_baganrisiko.kategori_id', '=', 'tbl_kategori.id')
            ->join('tbl_proses', 'tbl_baganrisiko.proses_id', '=', 'tbl_proses.id')
            ->orderBy('perspektif_id','asc')
            ->orderBy('proses_id','asc')
            ->get();
        // $nama = OPD::select('nama_opd')->where('id', Auth::user()->opd_id)->first();
        $nama = Unitkerja::select('unitkerja as nama_opd')->where('key_sort_unit', Auth::user()->key_sort_unit)->first();
        $nama_pemda = Pemda::where('id', 1)->first();
        $pdf = PDF::loadView('adminpemda.baganrisiko.cetak', compact('baganrisiko', 'nama', 'nama_pemda'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('bagan_risiko.pdf', array('Attachment' => false));
        // return view('adminpemda.baganrisiko.cetak', compact('baganrisiko', 'nama', 'nama_pemda'));
        exit(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perspektif = Perspektif::pluck('nama_perspektif', 'id');
        $kategori = Kategori::pluck('nama_kategori', 'id');
        
        $baganrisiko = BaganRisiko::find($id);
        $proses = Proses::where('id_perspektif',$baganrisiko->perspektif_id)->orderBy('id','ASC')->get()->pluck('nama_proses', 'id');
        return view('adminpemda.baganrisiko.edit', compact('perspektif', 'kategori', 'proses', 'baganrisiko'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'perspektif_id' => 'required',
                'kategori_id' => 'required',
                'proses_id' => 'required',
                'nama_risiko' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $risk = Baganrisiko::find($id);
                $risk->perspektif_id = Input::get('perspektif_id');
                $risk->kategori_id = Input::get('kategori_id');
                $risk->proses_id = Input::get('proses_id');
                $risk->nama_risiko = Input::get('nama_risiko');
                $risk->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('baganrisiko.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $baganrisiko = BaganRisiko::find($id);
        $baganrisiko->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('baganrisiko.index');
    }
}
