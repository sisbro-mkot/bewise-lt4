<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;

class DashboardCtrl extends Controller
{

    public function getDashPeta()
    {
        if(Auth::user()->role_id == '1') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')->count();
            $risikotermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_rtp.id_penyebab as id_rtp', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();

            return view('dashboard.dashpeta',  compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp'));
        }
    }

    public function getHeatmap()
    {
        if(Auth::user()->role_id == '1') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')->count();
            $risikotermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_rtp.id_penyebab as id_rtp', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $l1 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','1')
                ->count();
            $l2 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','2')
                ->count(); 
            $l3 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','3')
                ->count(); 
            $l4 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','4')
                ->count();
            $l5 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','5')
                ->count();
            $l6 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','6')
                ->count();
            $l7 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','7')
                ->count();
            $l8 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','8')
                ->count();
            $l9 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','9')
                ->count();
            $l10 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','10')
                ->count();
            $l11 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','11')
                ->count();
            $l12 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','12')
                ->count();
            $l13 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','13')
                ->count();
            $l14 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','14')
                ->count();
            $l15 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','15')
                ->count();
            $l16 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','16')
                ->count();
            $l17 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','17')
                ->count();
            $l18 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','18')
                ->count();
            $l19 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','19')
                ->count();
            $l20 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','20')
                ->count();
            $l21 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','21')
                ->count();
            $l22 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','22')
                ->count();
            $l23 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','23')
                ->count();
            $l24 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','24')
                ->count();
            $l25 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->where('skor_risiko','25')
                ->count();

        return view('dashboard.heatmap',  compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','insidenk','insidenr','insidenp','rtprealisasi','l1','l2','l3','l4','l5','l6','l7','l8','l9','l10','l11','l12','l13','l14','l15','l16','l17','l18','l19','l20','l21','l22','l23','l24','l25'));
        }
    }

    public function getPenyebab()
    {
        if(Auth::user()->role_id == '1') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')->count();
            $risikotermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_rtp.id_penyebab as id_rtp', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $mnid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 1)
                        ->count();
            $myid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 2)
                        ->count();
            $mdid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 3)
                        ->count();
            $mrid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 4)
                        ->count();
            $mcid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 5)
                        ->count();
            $exid = DB::table('t_penyebab_rca')
                        ->select('id_penyebab', 'id_jns_sebab')
                        ->where('id_jns_sebab', 6)
                        ->count();
            $mnmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 1)
                        ->get()
                        ->count();
            $mymt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 2)
                        ->get()
                        ->count();
            $mdmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 3)
                        ->get()
                        ->count();
            $mrmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 4)
                        ->get()
                        ->count();
            $mcmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 5)
                        ->get()
                        ->count();
            $exmt = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->where('id_jns_sebab', 6)
                        ->get()
                        ->count();
        return view('dashboard.penyebab', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','insidenk','insidenr','insidenp','rtpjadwal','rtprealisasi','mnid','myid','mdid','mrid','mcid','exid','mnmt','mymt','mdmt','mrmt','mcmt','exmt'));
        }
    }

    public function getPenyebabList()
    {
        if(Auth::user()->role_id == '1') {
            $evaluasi = DB::table('t_penyebab_rca')
                        ->select('t_penyebab_rca.id_penyebab as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian')
                        ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->get();
        return view('dashboard.penyebablist', compact('evaluasi'));
        }
    }

    public function getFilterPenyebabList($id)
    {
        if(Auth::user()->role_id == '1') {
            $evaluasi = DB::table('t_penyebab_rca')
                        ->select('t_penyebab_rca.id_penyebab as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_penyebab_rca.id_jns_sebab as id_jns_sebab')
                        ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('t_penyebab_rca.id_jns_sebab', $id)
                        ->get();
        return view('dashboard.penyebablist', compact('evaluasi'));
        }
    }



    public function getRTP()
    {
        if(Auth::user()->role_id == '1') {
            $triwulankini = Carbon::now()->quarter;
            $tricount = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->whereRaw(('t_rtp.id_periode - 2 = ?'), $triwulankini)
                        ->count();
            $terlambat = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_pemantauan.nama_hambatan')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->whereRaw('DATEDIFF(t_rtp.deadline_rtp, t_pemantauan.realisasi_waktu) < 0 OR (ISNULL(realisasi_waktu) AND DATEDIFF(t_rtp.deadline_rtp, CURRENT_TIMESTAMP) < 0)')
                        ->count();
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')->count();
            $risikotermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_rtp.id_penyebab as id_rtp', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();

        return view('dashboard.rtp', compact('triwulankini','tricount','terlambat','risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp'));
        }
    }

    public function getRTPList()
    {
        if(Auth::user()->role_id == '1') {
            $evaluasi = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->get();

        return view('dashboard.rtplist', compact('evaluasi'));
        }
    }

    public function getRTPListCurrent()
    {
        if(Auth::user()->role_id == '1') {
            $triwulankini = Carbon::now()->quarter;
            $evaluasi = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->whereRaw(('t_rtp.id_periode - 2 = ?'), $triwulankini)
                        ->get();

        return view('dashboard.rtplistcurrent', compact('evaluasi','triwulankini'));
        }
    }

    public function getMonitor()
    {
        if(Auth::user()->role_id == '1') {
            $evaluasi = DB::table('t_pemantauan')
                        ->select('t_pemantauan.id_pemantauan as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_periode.nama_periode as nama_periode', 't_pemantauan.realisasi_waktu as realisasi_waktu')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                        ->get();

        return view('dashboard.monitor', compact('evaluasi'));
        }
    }

    public function getResponsLambat()
    {
        if(Auth::user()->role_id == '1') {

            $evaluasi = DB::table('t_rtp')
                        ->select('t_rtp.id_rtp as id', 't_penyebab_rca.kode_penyebab as kode_penyebab', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_pemantauan.nama_hambatan')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->whereRaw('DATEDIFF(t_rtp.deadline_rtp, t_pemantauan.realisasi_waktu) < 0 OR (ISNULL(realisasi_waktu) AND DATEDIFF(t_rtp.deadline_rtp, CURRENT_TIMESTAMP) < 0)')
                        ->get();

        return view('dashboard.responslambat', compact('evaluasi'));
        }
    }

    public function getInsiden()
    {
        if(Auth::user()->role_id == '1') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')->count();
            $risikotermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_rtp.id_penyebab as id_rtp', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.kode_penyebab as kode_penyebab', 't_rtp.id_penyebab as id_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $r1 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 1)
                        ->count();
            $r2 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 2)
                        ->count();
            $r3 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 3)
                        ->count();
            $r4 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 4)
                        ->count();
            $r5 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', 5)
                        ->count();
            $p1 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 1)
                        ->count();
            $p2 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 2)
                        ->count();
            $p3 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 3)
                        ->count();
            $p4 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 4)
                        ->count();
            $p5 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 5)
                        ->count();
            $p6 = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', 6)
                        ->count();

        return view('dashboard.insiden', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp','r1','r2','r3','r4','r5','p1','p2','p3','p4','p5','p6'));
        }
    }

    public function getInsidenKejadian()
    {
        if(Auth::user()->role_id == '1') {

            $evaluasi = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak')
                        ->leftjoin('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->get();

        return view('dashboard.insidenkejadian', compact('evaluasi'));
        }
    }

    public function getInsidenRisiko()
    {
        if(Auth::user()->role_id == '1') {

            $evaluasi = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->get();

        return view('dashboard.insidenrisiko', compact('evaluasi'));
        }
    }


    public function getFilterInsidenRisiko($id)
    {
        if(Auth::user()->role_id == '1') {

            $evaluasi = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','ref_bagan_risiko.id_kategori_risiko as id_kategori_risiko')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('id_kategori_risiko', $id)
                        ->get();

        return view('dashboard.insidenrisiko', compact('evaluasi'));
        }
    }

    public function getInsidenSebab()
    {
        if(Auth::user()->role_id == '1') {

            $evaluasi = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->get();

        return view('dashboard.insidensebab', compact('evaluasi'));
        }
    }

    public function getFilterInsidenSebab($id)
    {
        if(Auth::user()->role_id == '1') {

            $evaluasi = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->where('id_jns_sebab', $id)
                        ->get();

        return view('dashboard.insidensebab', compact('evaluasi'));
        }
    }

    public function getDashRegister()
    {
        if(Auth::user()->role_id == '1') {
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                            })
                        ->leftjoin(DB::raw('(SELECT 
                            C.id_identifikasi AS id_identifikasi,
                            C.id_penyebab AS id_penyebab,
                            D.count_rtp AS count_rtp 
                            FROM t_penyebab_rca AS C
                            LEFT JOIN
                                (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                FROM t_rtp AS A
                                LEFT JOIN t_penyebab_rca AS B
                                ON A.id_penyebab = B.id_penyebab
                                GROUP BY B.id_penyebab) AS D
                                ON C.id_penyebab = D.id_penyebab
                                GROUP BY C.id_identifikasi) AS RTP'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                            })
                        ->get();
        return view('dashboard.index', compact('identifikasi'));
        }
    }

    public function getFilterRegister($id)
    {
        if(Auth::user()->role_id == '1') {
            $identifikasi = DB::table('t_identifikasi_risiko')
                            ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                            ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                            ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                            ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                            ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                            ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                                })
                            ->leftjoin(DB::raw('(SELECT 
                                C.id_identifikasi AS id_identifikasi,
                                C.id_penyebab AS id_penyebab,
                                D.count_rtp AS count_rtp 
                                FROM t_penyebab_rca AS C
                                LEFT JOIN
                                    (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                    FROM t_rtp AS A
                                    LEFT JOIN t_penyebab_rca AS B
                                    ON A.id_penyebab = B.id_penyebab
                                    GROUP BY B.id_penyebab) AS D
                                    ON C.id_penyebab = D.id_penyebab
                                    GROUP BY C.id_identifikasi) AS RTP'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                                })
                            ->where('skor_risiko', $id)
                            ->get();
        return view('dashboard.index', compact('identifikasi'));
        }
    }


    public function getDashRegisterUnit($unit)
    {
        if(Auth::user()->role_id == '1') {
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                        ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                            })
                        ->leftjoin(DB::raw('(SELECT 
                            C.id_identifikasi AS id_identifikasi,
                            C.id_penyebab AS id_penyebab,
                            D.count_rtp AS count_rtp 
                            FROM t_penyebab_rca AS C
                            LEFT JOIN
                                (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                FROM t_rtp AS A
                                LEFT JOIN t_penyebab_rca AS B
                                ON A.id_penyebab = B.id_penyebab
                                GROUP BY B.id_penyebab) AS D
                                ON C.id_penyebab = D.id_penyebab
                                GROUP BY C.id_identifikasi) AS RTP'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                            })
                        ->where('s_format_surat', $unit)
                        ->get();
            $risiko = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                        ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->where('s_format_surat', $unit)
                        ->count();
            $first = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat', 'wm_instansiunitorg.s_nama_instansiunitorg')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                        ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->where('s_format_surat', $unit)
                        ->first();
        return view('dashboard.indexunit', compact('identifikasi','risiko','first'));
        }
    }



    public function getHeatmapUnit($unit)
    {
        if(Auth::user()->role_id == '1') {
            $risiko = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                        ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->where('s_format_surat', $unit)
                        ->count();
            $l1 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','1')
                ->count();
            $l2 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','2')
                ->count(); 
            $l3 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','3')
                ->count(); 
            $l4 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','4')
                ->count();
            $l5 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','5')
                ->count();
            $l6 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','6')
                ->count();
            $l7 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','7')
                ->count();
            $l8 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','8')
                ->count();
            $l9 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)      
                ->where('skor_risiko','9')
                ->count();
            $l10 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','10')
                ->count();
            $l11 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','11')
                ->count();
            $l12 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','12')
                ->count();
            $l13 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','13')
                ->count();
            $l14 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','14')
                ->count();
            $l15 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','15')
                ->count();
            $l16 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','16')
                ->count();
            $l17 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','17')
                ->count();
            $l18 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','18')
                ->count();
            $l19 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','19')
                ->count();
            $l20 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','20')
                ->count();
            $l21 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','21')
                ->count();
            $l22 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','22')
                ->count();
            $l23 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','23')
                ->count();
            $l24 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','24')
                ->count();
            $l25 = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('s_format_surat', $unit)
                ->where('skor_risiko','25')
                ->count();
            $first = DB::table('t_identifikasi_risiko')
                            ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat', 'wm_instansiunitorg.s_nama_instansiunitorg')
                            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                            ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                            ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                            ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                            ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                            ->where('s_format_surat', $unit)
                            ->first();

        return view('dashboard.heatmapunit', compact('risiko','l1','l2','l3','l4','l5','l6','l7','l8','l9','l10','l11','l12','l13','l14','l15','l16','l17','l18','l19','l20','l21','l22','l23','l24','l25','first'));
        }
    }

    public function getFilterRegisterUnit($unit, $id)
    {
        if(Auth::user()->role_id == '1') {
            $identifikasi = DB::table('t_identifikasi_risiko')
                            ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                            ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                            ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                            ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                            ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                            ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                                })
                            ->leftjoin(DB::raw('(SELECT 
                                C.id_identifikasi AS id_identifikasi,
                                C.id_penyebab AS id_penyebab,
                                D.count_rtp AS count_rtp 
                                FROM t_penyebab_rca AS C
                                LEFT JOIN
                                    (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                    FROM t_rtp AS A
                                    LEFT JOIN t_penyebab_rca AS B
                                    ON A.id_penyebab = B.id_penyebab
                                    GROUP BY B.id_penyebab) AS D
                                    ON C.id_penyebab = D.id_penyebab
                                    GROUP BY C.id_identifikasi) AS RTP'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                                })
                            ->where('s_format_surat', $unit)
                            ->where('skor_risiko', $id)
                            ->get();
            $risiko = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                        ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->where('s_format_surat', $unit)
                        ->count();
            $first = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko.id_risiko_cetak as id_risiko_cetak', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks.skor_risiko as skor_risiko', 'wm_instansiunitorg.s_format_surat as s_format_surat', 'wm_instansiunitorg.s_nama_instansiunitorg')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->join('ref_matriks', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks.id_matriks')
                        ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                        ->join('wm_instansiunitorg', 'wm_jabdetail.s_kd_instansiunitorg', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->where('s_format_surat', $unit)
                        ->first();
        return view('dashboard.indexunit', compact('identifikasi','risiko','first'));
        }
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
