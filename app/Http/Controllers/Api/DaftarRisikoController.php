<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Baganrisiko;
use App\Models\opdkegiatan1\TopTenKegiatan;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Collection;

class DaftarRisikoController extends Controller
{
    public function daftarRisiko($num='all'){
    	$user_role = Auth::guard('api')->user()->role_id;
        if($num=='all'){
            if($user_role <= 3){
                $data = DB::table('identifikasi_pemda')
                        ->select('identifikasi_pemda.id','tbl_baganrisiko.nama_risiko as nama_risiko','tbl_perspektif.nama_perspektif as nama_perspektif','tbl_kategori.nama_kategori as nama_kategori','tbl_proses.nama_proses as nama_proses','identifikasi_pemda.sumber_risiko','identifikasi_pemda.kontrol','identifikasi_pemda.penyebab','identifikasi_pemda.dampak','identifikasi_pemda.pengendalian','identifikasi_pemda.sisa_risiko','analisis_pemda.kemungkinan_id as skor_kemungkinan','analisis_pemda.dampak_id as skor_dampak','analisis_pemda.tingkat_risiko as skor_risiko')
                        ->join('tbl_baganrisiko','tbl_baganrisiko.id','identifikasi_pemda.risiko_id')
                        ->join('tbl_perspektif','tbl_perspektif.id','tbl_baganrisiko.perspektif_id')
                        ->join('tbl_kategori','tbl_kategori.id','tbl_baganrisiko.kategori_id')
                        ->join('tbl_proses','tbl_proses.id','tbl_baganrisiko.proses_id')
                        ->leftjoin('analisis_pemda','analisis_pemda.identifikasi_id','identifikasi_pemda.id')
                        ->where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)
                        ->get();
            }elseif($user_role <=6){
                $data = DB::table('identifikasi_opd')
                        ->select('identifikasi_opd.id','tbl_baganrisiko.nama_risiko as nama_risiko','tbl_perspektif.nama_perspektif as nama_perspektif','tbl_kategori.nama_kategori as nama_kategori','tbl_proses.nama_proses as nama_proses','identifikasi_opd.sumber_risiko','identifikasi_opd.kontrol','identifikasi_opd.penyebab','identifikasi_opd.dampak','identifikasi_opd.pengendalian','identifikasi_opd.sisa_risiko','analisis_opd.kemungkinan_id as skor_kemungkinan','analisis_opd.dampak_id as skor_dampak','analisis_opd.tingkat_risiko as skor_risiko')
                        ->join('tbl_baganrisiko','tbl_baganrisiko.id','identifikasi_opd.risiko_id')
                        ->join('tbl_perspektif','tbl_perspektif.id','tbl_baganrisiko.perspektif_id')
                        ->join('tbl_kategori','tbl_kategori.id','tbl_baganrisiko.kategori_id')
                        ->join('tbl_proses','tbl_proses.id','tbl_baganrisiko.proses_id')
                        ->leftjoin('analisis_opd','analisis_opd.identifikasi_id','identifikasi_opd.id')
                        ->where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)
                        ->get();
            }else{
                $data = DB::table('identifikasi_kegiatan')
                        ->select('identifikasi_kegiatan.id','tbl_baganrisiko.nama_risiko as nama_risiko','tbl_perspektif.nama_perspektif as nama_perspektif','tbl_kategori.nama_kategori as nama_kategori','tbl_proses.nama_proses as nama_proses','identifikasi_kegiatan.sumber_risiko','identifikasi_kegiatan.kontrol','identifikasi_kegiatan.penyebab','identifikasi_kegiatan.dampak','identifikasi_kegiatan.pengendalian','identifikasi_kegiatan.sisa_risiko','analisis_kegiatan.kemungkinan_id as skor_kemungkinan','analisis_kegiatan.dampak_id as skor_dampak','analisis_kegiatan.tingkat_risiko as skor_risiko')
                        ->join('tbl_baganrisiko','tbl_baganrisiko.id','identifikasi_kegiatan.risiko_id')
                        ->join('tbl_perspektif','tbl_perspektif.id','tbl_baganrisiko.perspektif_id')
                        ->join('tbl_kategori','tbl_kategori.id','tbl_baganrisiko.kategori_id')
                        ->join('tbl_proses','tbl_proses.id','tbl_baganrisiko.proses_id')
                        ->leftjoin('analisis_kegiatan','analisis_kegiatan.identifikasi_id','identifikasi_kegiatan.id')
                        ->where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)
                        ->get();
            }
        }else{
            if($user_role <= 3){
                $data = DB::table('identifikasi_pemda')
                        ->select('identifikasi_pemda.id','tbl_baganrisiko.nama_risiko as nama_risiko','tbl_perspektif.nama_perspektif as nama_perspektif','tbl_kategori.nama_kategori as nama_kategori','tbl_proses.nama_proses as nama_proses','identifikasi_pemda.sumber_risiko','identifikasi_pemda.kontrol','identifikasi_pemda.penyebab','identifikasi_pemda.dampak','identifikasi_pemda.pengendalian','identifikasi_pemda.sisa_risiko','analisis_pemda.kemungkinan_id as skor_kemungkinan','analisis_pemda.dampak_id as skor_dampak','analisis_pemda.tingkat_risiko as skor_risiko')
                        ->join('tbl_baganrisiko','tbl_baganrisiko.id','identifikasi_pemda.risiko_id')
                        ->join('tbl_perspektif','tbl_perspektif.id','tbl_baganrisiko.perspektif_id')
                        ->join('tbl_kategori','tbl_kategori.id','tbl_baganrisiko.kategori_id')
                        ->join('tbl_proses','tbl_proses.id','tbl_baganrisiko.proses_id')
                        ->leftjoin('analisis_pemda','analisis_pemda.identifikasi_id','identifikasi_pemda.id')
                        ->where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)
                        ->orderBy('skor_risiko','DESC')
                        ->take($num)
                        ->get();
            }elseif($user_role <=6){
                $data = DB::table('identifikasi_opd')
                        ->select('identifikasi_opd.id','tbl_baganrisiko.nama_risiko as nama_risiko','tbl_perspektif.nama_perspektif as nama_perspektif','tbl_kategori.nama_kategori as nama_kategori','tbl_proses.nama_proses as nama_proses','identifikasi_opd.sumber_risiko','identifikasi_opd.kontrol','identifikasi_opd.penyebab','identifikasi_opd.dampak','identifikasi_opd.pengendalian','identifikasi_opd.sisa_risiko','analisis_opd.kemungkinan_id as skor_kemungkinan','analisis_opd.dampak_id as skor_dampak','analisis_opd.tingkat_risiko as skor_risiko')
                        ->join('tbl_baganrisiko','tbl_baganrisiko.id','identifikasi_opd.risiko_id')
                        ->join('tbl_perspektif','tbl_perspektif.id','tbl_baganrisiko.perspektif_id')
                        ->join('tbl_kategori','tbl_kategori.id','tbl_baganrisiko.kategori_id')
                        ->join('tbl_proses','tbl_proses.id','tbl_baganrisiko.proses_id')
                        ->leftjoin('analisis_opd','analisis_opd.identifikasi_id','identifikasi_opd.id')
                        ->where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)
                        ->orderBy('skor_risiko','DESC')
                        ->take($num)
                        ->get();
            }else{
                $data = DB::table('identifikasi_kegiatan')
                        ->select('identifikasi_kegiatan.id','tbl_baganrisiko.nama_risiko as nama_risiko','tbl_perspektif.nama_perspektif as nama_perspektif','tbl_kategori.nama_kategori as nama_kategori','tbl_proses.nama_proses as nama_proses','identifikasi_kegiatan.sumber_risiko','identifikasi_kegiatan.kontrol','identifikasi_kegiatan.penyebab','identifikasi_kegiatan.dampak','identifikasi_kegiatan.pengendalian','identifikasi_kegiatan.sisa_risiko','analisis_kegiatan.kemungkinan_id as skor_kemungkinan','analisis_kegiatan.dampak_id as skor_dampak','analisis_kegiatan.tingkat_risiko as skor_risiko')
                        ->join('tbl_baganrisiko','tbl_baganrisiko.id','identifikasi_kegiatan.risiko_id')
                        ->join('tbl_perspektif','tbl_perspektif.id','tbl_baganrisiko.perspektif_id')
                        ->join('tbl_kategori','tbl_kategori.id','tbl_baganrisiko.kategori_id')
                        ->join('tbl_proses','tbl_proses.id','tbl_baganrisiko.proses_id')
                        ->leftjoin('analisis_kegiatan','analisis_kegiatan.identifikasi_id','identifikasi_kegiatan.id')
                        ->where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)
                        ->orderBy('skor_risiko','DESC')
                        ->take($num)
                        ->get();
            }
        }
    	
    	if ($data) {
    		$res['success'] = true;
    		$res['message'] = $data;
    	}else{
    		$res['success'] = false;
    		$res['message'] = NULL;
    	}

        return response($res);
    }
    public function daftarRisikoTopTen(){
        // $data = TopTenKegiatan::where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)->get();
      $data = TopTenKegiatan::with('unitkerja', 'risiko', 'kegiatan')->where('key_sort_unit',Auth::guard('api')->user()->key_sort_unit)->get();
      $res = [];
      // return $data;
      foreach($data as $key => $dt){
          $datum  = new \stdClass();
          $datum->namaunit                = $dt->unitkerja->unitkerja_skt;
          $datum->nama_kegiatan           = $dt->kegiatan->nama_kegiatan;
          $datum->nama_risiko             = $dt->risiko->nama_risiko;
          $datum->nilai_komposit          = $dt->tingkat_risiko;
          $res[] = $datum;
      }
      $result = new Collection($res);
      // return $result;
      // $data['namaunit']        = $data->unitkerja->unitkerja_skt;
      // $data['nama_kegiatan']   = $data->kegiatan->nama_kegiatan;
      // $data['nama_risiko']     = $data->risiko->nama_risiko;
      if ($result) {
          $resp['success'] = true;
          $resp['message'] = $result;
      }else{
          $resp['success'] = false;
          $resp['message'] = NULL;
      }
      // return $data;
      return response($resp);
  }
}
