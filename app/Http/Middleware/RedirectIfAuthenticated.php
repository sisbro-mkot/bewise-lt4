<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //   if('web1' === $guard){
        //     if (Auth::guard('web1')->user()->hasRole('Admin BPKP')) {
        //       return redirect()->route('adminpemda');
        //     }

        //     if (Auth::guard('web1')->user()->hasRole('User BPKP 1')) {
        //         return redirect()->route('userpemda1');
        //     }

        //     if (Auth::guard('web1')->user()->hasRole('User BPKP 2')) {
        //         return redirect()->route('userpemda2');
        //     }

        //     if (Auth::guard('web1')->user()->hasRole('Admin PWK')) {
        //         return redirect()->route('adminopd');
        //     }

        //     if (Auth::guard('web1')->user()->hasRole('User PWK 1')) {
        //         return redirect()->route('useropd1');
        //     }

        //     if (Auth::guard('web1')->user()->hasRole('User PWK 2')) {
        //         return redirect()->route('useropd2');
        //     }

        //     if (Auth::guard('web1')->user()->hasRole('User PWK Kegiatan 1')) {
        //         return redirect()->route('opdkegiatan1');
        //     }

        //     if (Auth::guard('web1')->user()->hasRole('User PWK Kegiatan 2')) {
        //         return redirect()->route('opdkegiatan2');
        //     }
        //   }

        //   if (Auth::user()->hasRole('Admin BPKP')) {
        //     return redirect()->route('adminpemda');
        //   }

        //   if (Auth::user()->hasRole('User BPKP 1')) {
        //       return redirect()->route('userpemda1');
        //   }

        //   if (Auth::user()->hasRole('User BPKP 2')) {
        //       return redirect()->route('userpemda2');
        //   }

        //   if (Auth::user()->hasRole('Admin PWK')) {
        //       return redirect()->route('adminopd');
        //   }

        //   if (Auth::user()->hasRole('User PWK 1')) {
        //       return redirect()->route('useropd1');
        //   }

        //   if (Auth::user()->hasRole('User PWK 2')) {
        //       return redirect()->route('useropd2');
        //   }

        //   if (Auth::user()->hasRole('User PWK Kegiatan 1')) {
        //       return redirect()->route('opdkegiatan1');
        //   }

        //   if (Auth::user()->hasRole('User PWK Kegiatan 2')) {
        //       return redirect()->route('opdkegiatan2');
        //   }
        //     // return redirect('/test');
        // }

        return $next($request);
    }
}
