<?php

namespace App;

use App\Models\adminopd\KegiatanOPD;
use App\Models\adminopd\ProgramOPD;
use App\Models\adminopd\SasaranOPD;
use App\Models\adminpemda\Baganrisiko;
use App\Models\utils\Unitkerja;
use Illuminate\Database\Eloquent\Model;

class MappingUnit extends Model
{
    protected $table = 'tbl_mapping_unit';
    protected $fillable = ['sasaran_unit_id', 'key_sort_unit', 'program_id', 'kegiatan_id', 'risiko_id', 'bobot_risiko', 'bobot_kegiatan', 'bobot_program', 'created_at', 'updated_at'];

    public function unit()
    {
        return $this->hasOne(Unitkerja::class, 'key_sort_unit', 'key_sort_unit');
    }

    public function sasaran_unit()
    {
        return $this->hasOne(SasaranOPD::class, 'id', 'sasaran_unit_id');
    }

    public function program()
    {
        return $this->hasOne(ProgramOPD::class, 'id', 'program_id');
    }

    public function kegiatan()
    {
        return $this->hasOne(KegiatanOPD::class, 'id', 'kegiatan_id');
    }

    public function risiko()
    {
        return $this->hasOne(Baganrisiko::class, 'id', 'risiko_id');
    }
}
