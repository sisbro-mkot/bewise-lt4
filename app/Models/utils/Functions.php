<?php

namespace App\Models\utils;
use DB;
use Auth;
use App\Models\utils\Functions;

class Functions{
    public static function LoginLDAP($username,$password){
        $ldaphost = "10.10.1.10";
        $ldapUsername  = "cn=".$username.",o=BPKP";
        $ldapPassword = $password;
        $ldapport = 389;

        $ldap_base_dn = $ldapUsername;//'cn=debrian ruhut saragih,o=BPKP';
        $search_filter= "(objectclass=person)";
        $ds = ldap_connect($ldaphost,$ldapport);

        if(!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)){
            return null;
        }else {
                if (@ldap_bind($ds, $ldapUsername, $ldapPassword)) {
                    $sr = ldap_search($ds,$ldap_base_dn, $search_filter);
                    $val = ldap_get_entries($ds, $sr);

                    $user = new \stdClass();
                    $user->niplama = $val[0]["employeeid"][0];
                    $user->email = strtolower($val[0]["mail"][0]);
                    return $user;
                } else {
                    return null;
                }
            }
    }

}
