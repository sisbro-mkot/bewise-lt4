<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class RefKetWarna extends Model
{
    //
    protected $table = 'ref_ket_warna';
    public $timestamps = false;
    protected $fillable = ['id_ket_warna','nama_ket_warna','level_warna_1','level_warna_2','ket_warna','catatan_hapus','user_create','user_delete'];
}
