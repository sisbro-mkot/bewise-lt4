<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class RefBaganRisikoKode extends Model
{
    //
    protected $table = 'ref_bagan_risiko_kode';
    public $timestamps = false;
    protected $fillable = ['id_bagan_risiko','no_urut_bagan_risiko','kode_bagan_risiko','ket_bagan_risiko','catatan_hapus','user_create','user_update'];
}
