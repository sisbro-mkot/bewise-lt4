<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class PemantauanLevel extends Model
{
    //
    protected $table = 't_pemantauan_level';
    public $timestamps = false;
    protected $fillable = ['id_pemantauan_level','id_rtp','jumlah_kejadian_risiko','id_matriks_aktual','nama_rekomendasi','ket_komentar','ket_pemantauan_level','catatan_hapus','user_create','user_delete'];
}
