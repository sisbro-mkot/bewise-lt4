<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class AnalisisRisiko extends Model
{
    //
    protected $table = 't_analisis_risiko';
    public $timestamps = false;
    protected $primaryKey = 'id_analisis';
    protected $fillable = ['id_analisis','id_identifikasi','id_matriks_inherent','existing_control','uraian_existing_control','existing_control_memadai','id_matriks_residual','id_matriks_treated','ket_analisis','catatan_hapus','user_create','user_update'];
}
