<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefOutput extends Model
{
    //
    protected $table = 'ref_output';
    public $timestamps = false;
    protected $fillable = ['id_output','nama_output','ket_output','catatan_hapus','user_create','user_delete'];
}
