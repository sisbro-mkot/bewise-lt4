<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefDataUmum extends Model
{
    //
    protected $table = 'ref_data_umum';
    public $timestamps = false;
    protected $primaryKey = 'id_data_umum';
    protected $fillable = ['id_data_umum','s_kd_instansiunitorg','nama_pemilik','s_kd_jabdetail_pemilik','s_nmjabdetail_pemilik','nama_koordinator','s_kd_jabdetail_koordinator','s_nmjabdetail_koordinator','skor_selera','catatan_hapus','user_create','user_update'];
}
