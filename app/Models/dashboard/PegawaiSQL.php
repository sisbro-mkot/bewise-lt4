<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class PegawaiSQL extends Model
{
    protected $connection = 'dbhrm';
    protected $table = 'ren_peg_last';
}
