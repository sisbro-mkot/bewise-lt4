<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class Keterjadian extends Model
{
    //
    protected $table = 't_keterjadian';
    public $timestamps = false;
    protected $primaryKey = 'id_keterjadian';
    protected $fillable = ['id_keterjadian','id_periode','id_penyebab','nama_kejadian','waktu_kejadian','tempat_kejadian','skor_dampak','pemicu_kejadian','ket_kejadian','catatan_hapus','user_create','user_delete'];
}
