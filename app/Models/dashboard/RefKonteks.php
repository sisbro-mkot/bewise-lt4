<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefKonteks extends Model
{
    //
    protected $table = 'ref_konteks';
    public $timestamps = false;
    protected $primaryKey = 'id_konteks';
    protected $fillable = ['id_konteks','nama_konteks','ket_konteks','id_jns_konteks','catatan_hapus','user_create','user_update'];
}
