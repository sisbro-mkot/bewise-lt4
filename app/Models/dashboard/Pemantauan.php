<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class Pemantauan extends Model
{
    //
    protected $table = 't_pemantauan';
    public $timestamps = false;
    protected $primaryKey = 'id_pemantauan';
    protected $fillable = ['id_pemantauan','id_periode','id_rtp','realisasi_waktu','nama_hambatan','ket_komentar','ket_pemantauan','catatan_hapus','user_create','user_delete'];
}
