<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class IdentifikasiRisiko extends Model
{
    //
    protected $table = 't_identifikasi_risiko';
    public $timestamps = false;
    protected $primaryKey = 'id_identifikasi';
    protected $fillable = ['id_identifikasi','tahun','s_kd_jabdetail','id_risiko_cetak','id_konteks','id_bagan_risiko','nama_dampak','ket_dampak','catatan_hapus','user_create','user_update'];
}
