<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class PenyebabRCA extends Model
{
    //
    protected $table = 't_penyebab_rca';
    public $timestamps = false;
    protected $primaryKey = 'id_penyebab';
    protected $fillable = ['id_penyebab','id_identifikasi','nama_penyebab_1','nama_penyebab_2','nama_penyebab_3','nama_penyebab_4','nama_penyebab_5','nama_akar_penyebab','id_jns_sebab','no_urut_penyebab','kegiatan_pengendalian','ket_penyebab','catatan_hapus','user_create','user_delete'];
}
