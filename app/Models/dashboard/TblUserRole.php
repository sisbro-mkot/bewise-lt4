<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class TblUserRole extends Model
{
    //
    protected $table = 'tbl_user_role';
    public $timestamps = false;
    protected $fillable = ['user_nip','role_id'];
}
