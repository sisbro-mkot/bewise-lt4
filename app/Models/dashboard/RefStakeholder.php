<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class RefStakeholder extends Model
{
    //
    protected $table = 'ref_stakeholder';
    public $timestamps = false;
    protected $fillable = ['id_stakeholder','nama_stakeholder','ket_stakeholder','catatan_hapus','user_create','user_delete'];
}
