<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class Pengendalian extends Model
{
    //
    protected $table = 't_pengendalian';
    public $timestamps = false;
    protected $primaryKey = 'id_pengendalian';
    protected $fillable = ['id_pengendalian','id_identifikasi','nama_pengendalian','respon_risiko','id_sub_unsur','tahun','ket_pengendalian','s_kd_jabdetail_pic','id_output','catatan_hapus','user_create','user_update','user_delete'];
}
