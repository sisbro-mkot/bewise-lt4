<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class PenetapanKonteks extends Model
{
    //
    protected $table = 't_penetapan_konteks';
    public $timestamps = false;
    protected $primaryKey = 'id_penetapan_konteks';
    protected $fillable = ['id_penetapan_konteks','tahun','id_periode','id_data_umum','id_konteks','id_stakeholder','catatan_hapus','user_create','user_update'];
}
