<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class RefSeleraRisiko extends Model
{
    //
    protected $table = 'ref_selera_risiko';
    public $timestamps = false;
    protected $fillable = ['id_selera','tahun','s_kd_jabdetail_selera','skor_selera','catatan_hapus','user_create','user_update'];
}
