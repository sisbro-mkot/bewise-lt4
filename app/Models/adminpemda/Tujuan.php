<?php

namespace App\Models\adminpemda;

use Illuminate\Database\Eloquent\Model;

class Tujuan extends Model
{
    //
    protected $table = 'tujuan_pemda';
    public $timestamps = false;
    protected $fillable = ['misi_id', 'nama_tujuan'];

    public function misi(){
        return $this->hasOne(\App\Models\adminpemda\Misi::class,'id','misi_id');
    }
}
