<?php

namespace App\Models\adminpemda;

use Illuminate\Database\Eloquent\Model;

class Baganrisiko extends Model
{
    //
    protected $table = 'tbl_baganrisiko';
    public $timestamps = false;
    protected $fillable = ['perspektif_id','kategori_id', 'proses_id', 'nama_risiko', 'kode_risiko'];

    public function perspektif(){
        return $this->hasOne(Perspektif::class,'id','perspektif_id');
    }   
}
