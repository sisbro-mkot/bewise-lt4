<?php

namespace App\Models\adminpemda;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserHrm extends Authenticatable
{
	protected $guard = 'web';
    protected $table = 'tbl_user_hrm';
    public $timestamps = false;
    protected $fillable = [
        'name', 'email','username','password', 'role_id', 'user_nip','remember_token','api_token','nomorhp','key_sort_unit'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function unit(){
        return $this->hasOne(\App\Models\utils\Unitkerja::class,'key_sort_unit','key_sort_unit');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\adminpemda\Role','role_id');
    }

    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        
        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->cekUserRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->cekUserRole($roles);
        }
        return false;
    }

    private function getUserRole()
    {
       return $this->role()->getResults();
    }
    
    private function cekUserRole($role)
    {
        return (strtolower($role)==strtolower($this->have_role->nama_role)) ? true : false;
    }
}
