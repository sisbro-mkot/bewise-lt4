<?php

namespace App\Models\adminpemda;

use Illuminate\Database\Eloquent\Model;

class Perspektif extends Model
{
    //
    protected $table = 'tbl_perspektif';
    public $timestamps = false;
    protected $fillable = ['nama_perspektif'];
}
