<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function() {
// 	return view('home.main');
// });
// 
// 

// v2.0 awal
// 
// Lini 1
//
Route::get('/lini1analisis/kemungkinan', 'lini1\Lini1AnalisisCtrl@kemungkinan');
Route::get('/lini1analisis/dampak', 'lini1\Lini1AnalisisCtrl@dampak');
Route::get('/lini1analisis/showECedit/{id}', 'lini1\Lini1AnalisisCtrl@showECedit');
Route::get('/lini1analisis/showEC/{id}', 'lini1\Lini1AnalisisCtrl@showEC');
Route::get('/lini1realisasi/pilihPeriodeRTP/{id}', 'lini1\Lini1RealisasiCtrl@pilihPeriodeRTP');

Route::group(['middleware' => ['web', 'auth', 'roles']],function() {
Route::group(['roles' => '4' or '5' or '6' or '7' or '10'],function() { 
Route::namespace('lini1')->group(function () {
Route::get('dashboardunit', ['uses' => 'Lini1DashboardCtrl@index', 'as' => 'dashboardunit']);
// Parameter
Route::get('lini1data', ['uses' => 'Lini1DataCtrl@index', 'as' => 'lini1data']);
Route::resource('lini1data', 'Lini1DataCtrl');
Route::get('lini1panduan', ['uses' => 'Lini1DokumenCtrl@panduan', 'as' => 'lini1panduan']);
Route::get('lini1kamus', ['uses' => 'Lini1KamusCtrl@index', 'as' => 'lini1kamus']);
Route::get('cetakkamus', 'Lini1KamusCtrl@cetak');
// Penetapan Konteks
Route::get('createtkunit', ['uses' => 'Lini1TetapKonteksCtrl@create', 'as' => 'createtkunit']);
Route::get('lini1tetapkonteks', ['uses' => 'Lini1TetapKonteksCtrl@index', 'as' => 'lini1tetapkonteks']);
Route::resource('lini1tetapkonteks', 'Lini1TetapKonteksCtrl');
// Identifikasi Risiko
Route::get('heatmapunit', ['uses' => 'Lini1IdentifikasiCtrl@getHeatmapUnit', 'as' => 'heatmapunit']);
Route::get('dashboardregisterunit', 'Lini1IdentifikasiCtrl@getDashRegisterUnit');
Route::get('dashboardregisterunit/{id}', 'Lini1IdentifikasiCtrl@getFilterRegisterUnit');
Route::get('excelidunit', 'Lini1IdentifikasiCtrl@excel');
Route::get('cetakidunit', 'Lini1IdentifikasiCtrl@cetak');
Route::get('createidunit', ['uses' => 'Lini1IdentifikasiCtrl@create', 'as' => 'createidunit']);
Route::get('lini1identifikasi', ['uses' => 'Lini1IdentifikasiCtrl@index', 'as' => 'lini1identifikasi']);
Route::resource('lini1identifikasi', 'Lini1IdentifikasiCtrl');
// Analisis Risiko
Route::get('skoranunit', 'Lini1AnalisisCtrl@getSkorAnUnit');
Route::get('cetakanunit', 'Lini1AnalisisCtrl@cetak');
Route::get('createanunit', ['uses' => 'Lini1AnalisisCtrl@create', 'as' => 'createanunit']);
Route::get('lini1analisis', ['uses' => 'Lini1AnalisisCtrl@index', 'as' => 'lini1analisis']);
Route::resource('lini1analisis', 'Lini1AnalisisCtrl');
Route::get('lini1pengendalian/{id}/tambah', ['as' => 'lini1pengendalian.tambah', 'uses' => 'Lini1PengendalianCtrl@tambah']);
Route::get('lini1pengendalian', ['uses' => 'Lini1PengendalianCtrl@index', 'as' => 'lini1pengendalian']);
Route::resource('lini1pengendalian', 'Lini1PengendalianCtrl');
Route::get('lini1residual', ['uses' => 'Lini1ResidualCtrl@index', 'as' => 'lini1residual']);
Route::resource('lini1residual', 'Lini1ResidualCtrl');
// Evaluasi Risiko
Route::get('penyebabunit', 'Lini1EvaluasiCtrl@getPenyebabUnit');
Route::get('penyebablistunit', 'Lini1EvaluasiCtrl@getPenyebabListUnit');
Route::get('penyebablistunit/{id}', 'Lini1EvaluasiCtrl@getFilterPenyebabListUnit');
Route::get('cetakevunit', 'Lini1EvaluasiCtrl@cetakRisiko');
Route::get('cetakrcaunit', 'Lini1EvaluasiCtrl@cetakRCA');
Route::get('createevunit', ['uses' => 'Lini1EvaluasiCtrl@create', 'as' => 'createevunit']);
Route::get('lini1evaluasi', ['uses' => 'Lini1EvaluasiCtrl@index', 'as' => 'lini1evaluasi']);
Route::resource('lini1evaluasi', 'Lini1EvaluasiCtrl');
// Respons Risiko (Rencana)
Route::get('rtpunit', 'Lini1ResponsCtrl@getRTPUnit');
Route::get('rtplistunit', 'Lini1ResponsCtrl@getRTPListUnit');
Route::get('rtplistcurunit', 'Lini1ResponsCtrl@getRTPListCurUnit');
Route::get('cetakresunit', 'Lini1ResponsCtrl@cetak');
Route::get('createrespunit', ['uses' => 'Lini1ResponsCtrl@create', 'as' => 'createrespunit']);
Route::get('lini1respons', ['uses' => 'Lini1ResponsCtrl@index', 'as' => 'lini1respons']);
Route::resource('lini1respons', 'Lini1ResponsCtrl');
Route::get('lini1treated', ['uses' => 'Lini1TreatedCtrl@index', 'as' => 'lini1treated']);
Route::resource('lini1treated', 'Lini1TreatedCtrl');
// Respons Risiko (Realisasi)
Route::get('monitorunit', 'Lini1ResponsCtrl@getMonitorUnit');
Route::get('reslambatunit', 'Lini1ResponsCtrl@getResLambatUnit');
Route::get('cetakrealunit', 'Lini1RealisasiCtrl@cetak');
Route::get('createrealunit', ['uses' => 'Lini1RealisasiCtrl@create', 'as' => 'createrealunit']);
Route::get('lini1realisasi', ['uses' => 'Lini1RealisasiCtrl@index', 'as' => 'lini1realisasi']);
Route::resource('lini1realisasi', 'Lini1RealisasiCtrl');
// Monitoring Risiko
Route::get('insidenunit', 'Lini1MonitoringCtrl@getInsidenUnit');
Route::get('inskejadianunit', 'Lini1MonitoringCtrl@getInsKejadianUnit');
Route::get('insrisikounit', 'Lini1MonitoringCtrl@getInsRisikoUnit');
Route::get('insrisikounit/{id}', 'Lini1MonitoringCtrl@getFilterInsRisikoUnit');
Route::get('inssebabunit', 'Lini1MonitoringCtrl@getInsSebabUnit');
Route::get('inssebabunit/{id}', 'Lini1MonitoringCtrl@getFilterInsSebabUnit');
Route::get('cetakmonunit', 'Lini1MonitoringCtrl@cetak');
Route::get('createmonitorunit', ['uses' => 'Lini1MonitoringCtrl@create', 'as' => 'createmonitorunit']);
Route::get('lini1monitoring', ['uses' => 'Lini1MonitoringCtrl@index', 'as' => 'lini1monitoring']);
Route::resource('lini1monitoring', 'Lini1MonitoringCtrl');
// Pelaporan Risiko
Route::get('lini1laporan', ['uses' => 'Lini1LaporanCtrl@index', 'as' => 'lini1laporan']);
Route::resource('lini1laporan', 'Lini1LaporanCtrl');
}); //namespace
}); //roles

}); //middleware
//
// Lini 2
//
Route::get('/lini2tetapkonteks/pilihKonteks/{id}', 'lini2\Lini2TetapKonteksCtrl@pilihKonteks');
Route::get('/lini2identifikasi/pilihKonteksUnit/{id}', 'lini2\Lini2IdentifikasiCtrl@pilihKonteksUnit');
Route::get('/lini2analisis/pilihRisikoUnit/{id}', 'lini2\Lini2AnalisisCtrl@pilihRisikoUnit');
Route::get('/lini2analisis/pilihSkor/{id}/{id2}', 'lini2\Lini2AnalisisCtrl@pilihSkor');
Route::get('/lini2evaluasi/pilihRisikoUnit2/{id}', 'lini2\Lini2EvaluasiCtrl@pilihRisikoUnit2');
Route::get('/lini2respons/pilihSebab/{id}', 'lini2\Lini2ResponsCtrl@pilihSebab');
Route::get('/lini2realisasi/pilihRTP/{id}', 'lini2\Lini2RealisasiCtrl@pilihRTP');
Route::get('/lini2realisasi/pilihPeriodeRTP/{id}', 'lini2\Lini2RealisasiCtrl@pilihPeriodeRTP');
Route::get('/lini2monitoring/pilihSebab2/{id}', 'lini2\Lini2MonitoringCtrl@pilihSebab2');

Route::group(['middleware' => ['web', 'auth', 'roles']],function() {
Route::group(['roles' => '1' or '2' or '3' or '8'],function() { 
Route::namespace('lini2')->group(function () {
// Parameter
Route::get('lini2data', ['uses' => 'Lini2DataCtrl@index', 'as' => 'lini2data']);
Route::resource('lini2data', 'Lini2DataCtrl');
Route::get('createparamkonteks', ['uses' => 'Lini2ParamKonteksCtrl@create', 'as' => 'createparamkonteks']);
Route::get('lini2paramkonteks', ['uses' => 'Lini2ParamKonteksCtrl@index', 'as' => 'lini2paramkonteks']);
Route::resource('lini2paramkonteks', 'Lini2ParamKonteksCtrl');
Route::get('createparamrisiko', ['uses' => 'Lini2ParamRisikoCtrl@create', 'as' => 'createparamrisiko']);
Route::get('lini2paramrisiko', ['uses' => 'Lini2ParamRisikoCtrl@index', 'as' => 'lini2paramrisiko']);
Route::resource('lini2paramrisiko', 'Lini2ParamRisikoCtrl');
Route::get('createparamrole', ['uses' => 'Lini2ParamRoleCtrl@create', 'as' => 'createparamrole']);
Route::get('lini2paramrole', ['uses' => 'Lini2ParamRoleCtrl@index', 'as' => 'lini2paramrole']);
Route::resource('lini2paramrole', 'Lini2ParamRoleCtrl');
Route::get('createparamuser', ['uses' => 'Lini2ParamUserCtrl@create', 'as' => 'createparamuser']);
Route::get('ubahparamuser', ['uses' => 'Lini2ParamUserCtrl@ubah', 'as' => 'ubahparamuser']);
Route::post('simpanparamuser', ['uses' => 'Lini2ParamUserCtrl@simpan', 'as' => 'simpanparamuser']);
Route::get('lini2paramuser', ['uses' => 'Lini2ParamUserCtrl@index', 'as' => 'lini2paramuser']);
Route::resource('lini2paramuser', 'Lini2ParamUserCtrl');
// Penetapan Konteks
Route::get('lini2tetapkonteks/{id}/delete', ['as' => 'lini2tetapkonteks.delete', 'uses' => 'Lini2TetapKonteksCtrl@delete']);
Route::get('lini2tetapkonteks/{id}/batal', ['as' => 'lini2tetapkonteks.batal', 'uses' => 'Lini2TetapKonteksCtrl@batal']);
Route::get('createtetapkonteks', ['uses' => 'Lini2TetapKonteksCtrl@create', 'as' => 'createtetapkonteks']);
Route::get('lini2tetapkonteks', ['uses' => 'Lini2TetapKonteksCtrl@index', 'as' => 'lini2tetapkonteks']);
Route::resource('lini2tetapkonteks', 'Lini2TetapKonteksCtrl');
// Identifikasi Risiko
Route::get('peta', ['uses' => 'Lini2IdentifikasiCtrl@getDashPeta', 'as' => 'peta']);
Route::get('heatmap', ['uses' => 'Lini2IdentifikasiCtrl@getHeatmap', 'as' => 'heatmap']);
Route::get('registerunit/{id}', 'Lini2IdentifikasiCtrl@getRegisterUnit');
Route::get('dashboardregister', 'Lini2IdentifikasiCtrl@getDashRegister');
Route::get('dashboardregister/{id}', 'Lini2IdentifikasiCtrl@getFilterRegister');
Route::get('createidentifikasi', ['uses' => 'Lini2IdentifikasiCtrl@create', 'as' => 'createidentifikasi']);
Route::get('lini2identifikasi', ['uses' => 'Lini2IdentifikasiCtrl@index', 'as' => 'lini2identifikasi']);
Route::resource('lini2identifikasi', 'Lini2IdentifikasiCtrl');
// Analisis Risiko
Route::get('skoranalisis', 'Lini2AnalisisCtrl@getSkorAnalisis');
Route::get('createanalisis', ['uses' => 'Lini2AnalisisCtrl@create', 'as' => 'createanalisis']);
Route::get('lini2analisis', ['uses' => 'Lini2AnalisisCtrl@index', 'as' => 'lini2analisis']);
Route::resource('lini2analisis', 'Lini2AnalisisCtrl');
// Evaluasi Risiko
Route::get('penyebab', 'Lini2EvaluasiCtrl@getPenyebab');
Route::get('penyebablist', 'Lini2EvaluasiCtrl@getPenyebabList');
Route::get('penyebablist/{id}', 'Lini2EvaluasiCtrl@getFilterPenyebabList');
Route::get('createevaluasi', ['uses' => 'Lini2EvaluasiCtrl@create', 'as' => 'createevaluasi']);
Route::get('lini2evaluasi', ['uses' => 'Lini2EvaluasiCtrl@index', 'as' => 'lini2evaluasi']);
Route::resource('lini2evaluasi', 'Lini2EvaluasiCtrl');
// Respons Risiko (Rencana)
Route::get('rtp', 'Lini2ResponsCtrl@getRTP');
Route::get('rtplist', 'Lini2ResponsCtrl@getRTPList');
Route::get('rtplistcurrent', 'Lini2ResponsCtrl@getRTPListCurrent');
Route::get('createrespons', ['uses' => 'Lini2ResponsCtrl@create', 'as' => 'createrespons']);
Route::get('lini2respons', ['uses' => 'Lini2ResponsCtrl@index', 'as' => 'lini2respons']);
Route::resource('lini2respons', 'Lini2ResponsCtrl');
Route::get('lini2treated', ['uses' => 'Lini2TreatedCtrl@index', 'as' => 'lini2treated']);
Route::resource('lini2treated', 'Lini2TreatedCtrl');
// Respons Risiko (Realisasi)
Route::get('monitor', 'Lini2ResponsCtrl@getMonitor');
Route::get('responslambat', 'Lini2ResponsCtrl@getResponsLambat');
Route::get('createrealisasi', ['uses' => 'Lini2RealisasiCtrl@create', 'as' => 'createrealisasi']);
Route::get('lini2realisasi', ['uses' => 'Lini2RealisasiCtrl@index', 'as' => 'lini2realisasi']);
Route::resource('lini2realisasi', 'Lini2RealisasiCtrl');
// Monitoring Risiko
Route::get('insiden', 'Lini2MonitoringCtrl@getInsiden');
Route::get('insidenkejadian', 'Lini2MonitoringCtrl@getInsidenKejadian');
Route::get('insidenrisiko', 'Lini2MonitoringCtrl@getInsidenRisiko');
Route::get('insidenrisiko/{id}', 'Lini2MonitoringCtrl@getFilterInsidenRisiko');
Route::get('insidensebab', 'Lini2MonitoringCtrl@getInsidenSebab');
Route::get('insidensebab/{id}', 'Lini2MonitoringCtrl@getFilterInsidenSebab');
Route::get('createmonitoring', ['uses' => 'Lini2MonitoringCtrl@create', 'as' => 'createmonitoring']);
Route::get('lini2monitoring', ['uses' => 'Lini2MonitoringCtrl@index', 'as' => 'lini2monitoring']);
Route::resource('lini2monitoring', 'Lini2MonitoringCtrl');
}); //namespace
}); //roles
}); //middleware
//
// v2.0 akhir

Route::get('/', function() {
	return redirect('/login');
});

Route::get('/login', ['uses' => 'admin\AuthCtrl@getLogin','as' => 'login']); 
Route::post('/login', ['uses' => 'admin\AuthCtrl@postLogin']);
Route::get('/peran', ['uses' => 'admin\AuthCtrl@getPeran','as' => 'peran']); 
Route::post('/peran', ['uses' => 'admin\AuthCtrl@postPeran']);
Route::post('/logout', ['uses' => 'admin\AuthCtrl@getLogout','as' => 'logout']);
Route::get('/logout', ['uses' => 'admin\AuthCtrl@getLogout','as' => 'logout']);


