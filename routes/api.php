<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix('v1')->group(function(){
	Route::post('login', 'Api\LoginController@postLogin');

	Route::group(['middleware' => 'auth:api'], function(){
		Route::get('daftar_risiko/{num?}', 'Api\DaftarRisikoController@daftarRisiko');
		Route::get('daftar_risiko_topten', 'Api\DaftarRisikoController@daftarRisikoTopTen');

		Route::get('logout', 'Api\LoginController@logout');
	});
});

// Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function () {
//     Route::post('/short', 'UrlMapperController@store');
// });