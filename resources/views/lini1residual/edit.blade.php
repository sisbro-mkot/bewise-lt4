@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Analisis Risiko Unit Kerja</li>
  </ol>
  <h6 class="slim-pagetitle">Analisis Risiko</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Penilaian Residual Risk</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1residual.update', $analisis->id_analisis)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="hidden" name="id_analisis" value="{{$analisis->id_analisis}}">

    <div class="box-body">

      <div class="form-inline">
        <label for="kemungkinan_inherent" class="col-sm-2 control-label">Kemungkinan Inheren :</label>
        <i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalKemungkinan" onclick="showKemungkinan(this)"></i>
        <div class="col-sm-1">
          <input class="form-control col-sm-12" value="{{$inherent->skor_kemungkinan}}" name="kemungkinan_inherent" id="kemungkinan_inherent" readonly></input>
        </div>
        <label for="dampak_inherent" class="col-sm-2 control-label">Dampak Inheren :</label>
        <i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalDampak" onclick="showDampak(this)"></i>
        <div class="col-sm-1">
          <input class="form-control col-sm-12" value="{{$inherent->skor_dampak}}" name="dampak_inherent" id="dampak_inherent" readonly></input>
        </div>
        <label for="id_matriks_inherent" class="col-sm-2 control-label">Level Risiko Inheren :</label>
        <div class="col-sm-1">
          <input class="form-control col-sm-12" value="{{$inherent->skor_risiko}}" name="id_matriks_inherent" id="id_matriks_inherent" readonly></input>
        </div>
      </div>

      <br/>
 
      <div class="form-inline">
        <label for="kemungkinan_residual" class="col-sm-2 control-label">Kemungkinan Residual :</label>
        <i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalKemungkinan" onclick="showKemungkinan(this)"></i>
        <div class="col-sm-1">
          <select class="form-control" name="kemungkinan_residual" id="kemungkinan_residual">
            @foreach($kemungkinan_residual as $per)
            <option value="{{$per}}" {{$skor->skor_kemungkinan_residual == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="dampak_residual" class="col-sm-2 control-label">Dampak Residual :</label>
        <i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalDampak" onclick="showDampak(this)"></i>
        <div class="col-sm-1">
          <select class="form-control" name="dampak_residual" id="dampak_residual">
            @foreach($dampak_residual as $per)
            <option value="{{$per}}" {{$skor->skor_dampak_residual == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="id_matriks_residual" class="col-sm-2 control-label">Level Risiko Residual :</label>
        <div class="col-sm-1">
          <input class="form-control col-sm-12" value="" name="id_matriks_residual_read" id="id_matriks_residual_read" readonly></input>
        </div>
      </div>
      <br/>
      <div class="form-group" id="data-risiko">
        <div class="col-sm-2">
          <select name="id_matriks_residual" class="form-control" id="id_matriks_residual">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Kemungkingan/Dampak Risiko Residual--</option>
          </select>
        </div>
      </div>


      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
  </div>
</div>

<!-- Modal Kemungkinan -->

<div class="modal" tabindex="-1" id="modalKemungkinan" aria-hidden="true">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h5 class="modal-title">Tabel Kriteria Kemungkinan</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="card card-table">
        <div class="pd-20">
        <div class="table-responsive-lg">
        <div class="table-wrapper">
        <table id="tabel-kemungkinan" class="table">
            <thead>
                <tr>
                    <th style="text-align: center;" rowspan="2">No.</th>
                    <th style="text-align: center;" rowspan="2">Level Kemungkinan</th>
                    <th style="text-align: center;" colspan="3">Kriteria Kemungkinan</th>
                </tr>
                <tr>
                    <th style="text-align: center;">Persentase dalam  1 Tahun</th>
                    <th style="text-align: center;">Jumlah Frekuensi dalam 1 Tahun</th>
                    <th style="text-align: center;">Kejadian Toleransi Rendah</th>
                </tr>
             </thead>
        </table>
        </div>
        </div>
        </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- Modal Dampak -->

<div class="modal" tabindex="-1" id="modalDampak" aria-hidden="true">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h5 class="modal-title">Tabel Kriteria Dampak</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="card card-table">
        <div class="pd-20">
        <div class="table-responsive-lg">
        <div class="table-wrapper">
        <table id="tabel-dampak" class="table">
            <thead>
                <tr>
                    <th style="text-align: center;" rowspan="2">No.</th>
                    <th style="text-align: center;" rowspan="2">Area Dampak</th>
                    <th style="text-align: center;" colspan="5">Level Dampak</th>
                </tr>
                <tr>
                    <th style="text-align: center;">Tidak Signifikan (1)</th>
                    <th style="text-align: center;">Minor (2)</th>
                    <th style="text-align: center;">Moderat (3)</th>
                    <th style="text-align: center;">Signifikan (4)</th>
                    <th style="text-align: center;">Sangat Signifikan (5)</th>
                </tr>
             </thead>
        </table>
        </div>
        </div>
        </div>
        </div>
      </div>

    </div>
  </div>
</div>

@stop


@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

    $(document).ready(function() {
    $("#data-risiko").hide();
    $("#kemungkinan_residual").select2();
    $("#dampak_residual").select2();

      var kemungkinanID = $('#kemungkinan_residual').val();
      var dampakID = $('#dampak_residual').val();
      console.log(kemungkinanID);
      if(kemungkinanID) {
          $.ajax({
              url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                  $('select[name="id_matriks_residual"]').empty();
                  $.each(data, function(key, value) {
                      $('select[name="id_matriks_residual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                      var input = value.skor_risiko;
                      var text2 = document.getElementById("id_matriks_residual_read");
                      text2.value = input;
                  });

              }
          });
      } else {
          $('select[name="id_matriks_residual"]').empty();
      }

    $("#id_matriks_residual").select2();

});

  </script>
@endpush

@push('js1')
  <script type="text/javascript">

    function showKemungkinan(ele) 
      {

          $.ajax({
              url: '../../lini1analisis/kemungkinan',
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#tabel-kemungkinan').DataTable({
                    searching: false, paging: false, info: false,
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'no' ,"fnRender": function( oObj ) { return oObj.aData[3].no }},
                        { mData: 'level' ,"fnRender": function( oObj ) { return oObj.aData[3].level }},
                        { mData: 'persentase' ,"fnRender": function( oObj ) { return oObj.aData[3].persentase }},
                        { mData: 'jumlah',"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
                        { mData: 'toleransi',"fnRender": function( oObj ) { return oObj.aData[3].toleransi }}
                              ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

    function showDampak(ele) 
      {

          $.ajax({
              url: '../../lini1analisis/dampak',
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#tabel-dampak').DataTable({
                    paging: false, info: false,
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'no' ,"fnRender": function( oObj ) { return oObj.aData[3].no }},
                        { mData: 'area' ,"fnRender": function( oObj ) { return oObj.aData[3].area }},
                        { mData: 'l1' ,"fnRender": function( oObj ) { return oObj.aData[3].l1 }},
                        { mData: 'l2',"fnRender": function( oObj ) { return oObj.aData[3].l2 }},
                        { mData: 'l3',"fnRender": function( oObj ) { return oObj.aData[3].l3 }},
                        { mData: 'l4',"fnRender": function( oObj ) { return oObj.aData[3].l4 }},
                        { mData: 'l5',"fnRender": function( oObj ) { return oObj.aData[3].l5 }}
                              ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

  $(document).ready(function() {

        $('#kemungkinan_residual').select2().on('change', function() {
            var kemungkinanID = $(this).val();
            var dampakID = $('#dampak_residual').val();
            console.log(kemungkinanID);
            if(kemungkinanID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_residual"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_residual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_residual_read");
                            text2.value = input;
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_residual"]').empty();
            }
        });

        $('#dampak_residual').select2().on('change', function() {
            var dampakID = $(this).val();
            var kemungkinanID = $('#kemungkinan_residual').val();
            console.log(dampakID);
            if(dampakID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_residual"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_residual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_residual_read");
                            text2.value = input;
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_residual"]').empty();
            }
        });

        $('#id_matriks_residual').select2()

  });
  </script>
@endpush