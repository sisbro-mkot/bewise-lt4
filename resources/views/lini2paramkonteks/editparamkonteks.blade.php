@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Jenis dan Nama Konteks</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
  </ol>
  <h6 class="slim-pagetitle">Jenis dan Nama Konteks</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Edit Data</h6>
  </div>
 

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini2paramkonteks.update', $konteks->id_konteks)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$konteks->id_konteks}}" id="id_konteks" name="id_konteks" hidden>
        </div>
      </div>

      <div class="form-group">
        <label for="id_jns_konteks" class="col-sm-2 control-label">Jenis Konteks</label>
        <div class="col-sm-12">
          <select name="id_jns_konteks" class="form-control" id="id_jns_konteks" autofocus>
            @foreach($id_jns_konteks as $key => $value)
              <option value="{{$key}}" {{$konteks->id_jns_konteks == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_konteks" class="col-sm-12 control-label">Nama Konteks</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_konteks" id="nama_konteks">{{$konteks->nama_konteks}}</textarea>
          </div>
      </div>



      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2paramkonteks.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#id_jns_konteks").select2();
});
</script>
@endpush
