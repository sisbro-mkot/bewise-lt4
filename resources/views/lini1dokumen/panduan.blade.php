@extends('layout.app')

@section('isi')

<style type="text/css">

body {

  background-color: white;
}

.faq-nav {
  flex-direction: column;
  margin: 0 0 32px;
  border-radius: 2px;
  border: 1px solid #ddd;
  box-shadow: 0 1px 5px rgba(85, 85, 85, 0.15);
}
.faq-nav .nav-link {
  position: relative;
  display: block;
  margin: 0;
  padding: 13px 16px;
  background-color: #fff;
  border: 0;
  border-bottom: 1px solid #ddd;
  border-radius: 0;
  color: #616161;
  transition: background-color .2s ease;
}
.faq-nav .nav-link:hover {
  background-color: #f6f6f6;
}
.faq-nav .nav-link.active {
  background-color: #f6f6f6;
  font-weight: 700;
  color: rgba(0, 0, 0, 0.87);
}
.faq-nav .nav-link:last-of-type {
  border-bottom-left-radius: 2px;
  border-bottom-right-radius: 2px;
  border-bottom: 0;
}
.faq-nav .nav-link i.mdi {
  margin-right: 5px;
  font-size: 20px;
  position: relative;
}

.tab-content {
  box-shadow: 0 1px 5px rgba(85, 85, 85, 0.15);
}
.tab-content .card {
  border-radius: 0;
}
.tab-content .card-header {
  padding: 15px 16px;
  border-radius: 0;
  background-color: #f6f6f6;
}
.tab-content .card-header h5 {
  margin: 0;
}
.tab-content .card-header h5 button {
  display: block;
  width: 100%;
  padding: 0;
  border: 0;
  font-weight: 700;
  color: rgba(0, 0, 0, 0.87);
  text-align: left;
  white-space: normal;
}
.tab-content .card-header h5 button:hover, .tab-content .card-header h5 button:focus, .tab-content .card-header h5 button:active, .tab-content .card-header h5 button:hover:active {
  text-decoration: none;
}
.tab-content .card-body p {
  color: #616161;
}
.tab-content .card-body p:last-of-type {
  margin: 0; 
}

.accordion > .card:not(:first-child) {
  border-top: 0; 
}

.collapse.show .card-body {
  border-bottom: 1px solid rgba(0, 0, 0, 0.125); color: black;
}

</style>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
        <div class="py-1 text-center">
          <br/>
            <h2 style="color: black;">Panduan Penggunaan Aplikasi</h2>
          <br/>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-4">
      <div class="nav nav-pills faq-nav" id="faq-tabs" role="tablist" aria-orientation="vertical">
        <a href="#tab1" class="nav-link active" data-toggle="pill" role="tab" aria-controls="tab1" aria-selected="true">
          <i class="mdi mdi-help-circle"></i> Penetapan Skor Selera Risiko
        </a>
        <a href="#tab2" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab2" aria-selected="true">
          <i class="mdi mdi-cellphone-android"></i> Tahap Penetapan Konteks
        </a>
        <a href="#tab3" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab3" aria-selected="true">
          <i class="mdi mdi-motion-sensor"></i> Tahap Identifikasi Risiko
        </a>
        <a href="#tab4" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab4" aria-selected="true">
          <i class="mdi mdi-briefcase-search-outline"></i> Tahap Analisis Risiko
        </a>
        <a href="#tab5" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab5" aria-selected="true">
          <i class="mdi mdi-book-open-page-variant"></i> Tahap Evaluasi Risiko
        </a>
        <a href="#tab6" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab6" aria-selected="true">
          <i class="mdi mdi-calendar-clock"></i> Tahap Respons Risiko
        </a>
        <a href="#tab7" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab7" aria-selected="true">
          <i class="mdi mdi-airplane-takeoff"></i> Tahap Pemantauan Risiko
        </a>
        
      </div>
    </div>

    <div class="col-lg-8">
      <div class="tab-content" id="faq-tab-content">
        <div class="tab-pane  show active " id="tab1" role="tabpanel" aria-labelledby="tab1">
          <div class="accordion" id="accordion-tab-1">
            <div class="card">
              <div class="card-header" id="accordion-tab-1-heading-1">
                <h5>
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-1" aria-expanded="false" aria-controls="accordion-tab-1-content-1"><i class="mdi mdi-swap-vertical float-right"></i> Penetapan Skor Selera Risiko</button>
                </h5>
              </div>
              <div class="collapse  show " id="accordion-tab-1-content-1" aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Parameter</b> -> <b>Data Umum Unit Kerja</b>.</li>
                    <li>Tekan tombol <b>“Ubah skor selera risiko”</b>.</li>
                    <li>Pilh skor selera risiko.</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="tab-pane " id="tab2" role="tabpanel" aria-labelledby="tab2">
          <div class="accordion" id="accordion-tab-2">
            <div class="card">
              <div class="card-header" id="accordion-tab-2-heading-1">
                <h5>
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-1" aria-expanded="false" aria-controls="accordion-tab-2-content-1"><i class="mdi mdi-swap-vertical float-right"></i> Pengisian Data Penetapan Konteks</button>
                </h5>
              </div>
              <div class="collapse  show " id="accordion-tab-2-content-1" aria-labelledby="accordion-tab-2-heading-1" data-parent="#accordion-tab-2">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Pengelolaan Risiko</b> -> <b>Penetapan Konteks</b>.</li>
                    <li>Tekan tombol <b>“Tambah Data”</b>.</li>
                    <li>Pilh <b>jenis konteks</b> (<i>Going Concern</i>/Sasaran Strategis/Proses Bisnis).</li>
                    <li>Pilih <b>konteks</b>. Konteks bisa dicari dengan mengetik satu/lebih kata kunci. Misalnya, ketik kata “pengawasan” untuk mencari seluruh nama konteks yang mengandung kata “pengawasan”.</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane " id="tab3" role="tabpanel" aria-labelledby="tab3">
          <div class="accordion" id="accordion-tab-3">
            <div class="card">
              <div class="card-header" id="accordion-tab-3-heading-1">
                <h5>
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-1" aria-expanded="false" aria-controls="accordion-tab-3-content-1"><i class="mdi mdi-swap-vertical float-right"></i> Pengisian Data Identifikasi Risiko</button>
                </h5>
              </div>
              <div class="collapse  show " id="accordion-tab-3-content-1" aria-labelledby="accordion-tab-3-heading-1" data-parent="#accordion-tab-3">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Pengelolaan Risiko</b> -> <b>Identifikasi Risiko</b>.</li>
                    <li>Tekan tombol <b>“Tambah Data”</b>.</li>
                    <li>Pilih <b>konteks</b>. Konteks bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Pilih <b>nama risiko</b>. Nama risiko bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Isi (ketik) <b>nama dampak</b>, yaitu uraian akibat/potensi kerugian yang akan diperoleh jika risiko tersebut terjadi.</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane " id="tab4" role="tabpanel" aria-labelledby="tab4">
          <div class="accordion" id="accordion-tab-4">
            <div class="card">
              <div class="card-header" id="accordion-tab-4-heading-1">
                <h5>
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-1" aria-expanded="false" aria-controls="accordion-tab-4-content-1"><i class="mdi mdi-swap-vertical float-right"></i> Pengisian Data Analisis Risiko</button>
                </h5>
              </div>
              <div class="collapse  show " id="accordion-tab-4-content-1" aria-labelledby="accordion-tab-4-heading-1" data-parent="#accordion-tab-4">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Pengelolaan Risiko</b> -> <b>Analisis Risiko</b>.</li>
                    <li>Tekan tombol <b>“Tambah Data”</b>.</li>
                    <li>Pilih <b>nama risiko</b> yang akan dianalisis. Nama risiko bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Pilih skor kemungkinan <i>inherent</i> (skala 1 – 5)</li>
                    <li>Pilih skor dampak <i>inherent</i> (skala 1 – 5).</li>
                    <li>Masukkan <b>status keberadaan (ada/tidaknya) pengendalian yang sudah ada</b> (<i>existing control</i>), dengan memilih Ya/Tidak.</li>
                    <li>Isi (ketik) <b>nama pengendalian</b> yang sudah ada.</li>
                    <li>Masukkan <b>efektivitas (memadai/tidaknya) pengendalian</b> yang sudah ada, dengan memilih Ya/Tidak.</li>
                    <li>Pilih skor kemungkinan <i>residual</i> (skala 1 – 5).</li>
                    <li>Pilih skor dampak <i>residual</i> (skala 1 – 5).</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane " id="tab5" role="tabpanel" aria-labelledby="tab5">
          <div class="accordion" id="accordion-tab-5">
            <div class="card">
              <div class="card-header" id="accordion-tab-5-heading-1">
                <h5>
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-1" aria-expanded="false" aria-controls="accordion-tab-5-content-1"><i class="mdi mdi-swap-vertical float-right"></i> Pengisian Data Evaluasi Risiko</button>
                </h5>
              </div>
              <div class="collapse  show " id="accordion-tab-5-content-1" aria-labelledby="accordion-tab-5-heading-1" data-parent="#accordion-tab-5">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Pengelolaan Risiko</b> -> <b>Evaluasi Risiko</b>.</li>
                    <li>Tekan tombol <b>“Tambah Data”</b>.</li>
                    <li>Pilih <b>nama risiko</b> yang akan dievaluasi. Nama risiko bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Isi (ketik) nama penyebab (<b><i>why 1</i></b>) yang merupakan penyebab langsung terjadinya risiko pada langkah nomor 3. Jika risiko tersebut merupakan risiko eksternal (tidak dapat dikurangi tingkat kemungkinan terjadinya, misalnya bencana alam), isi (ketik) “tidak ada”.</li>
                    <li>Jika <b>terdapat alasan</b> terjadinya penyebab (<i>why 1</i>), tekan tombol “<b>Bukan Akar Penyebab</b>” dan lanjutkan ke langkah nomor 6.  Jika <b>tidak terdapat alasan</b> terjadinya penyebab (<i>why 1</i>), tekan tombol “<b>Akar Penyebab</b>” dan lanjutkan ke langkah nomor 14.</li>
                    <li>Isi (ketik) nama penyebab (<b><i>why 2</i></b>) yang merupakan alasan terjadinya penyebab (<i>why 1</i>).</li>
                    <li>Jika <b>terdapat alasan</b> terjadinya penyebab (<i>why 2</i>), tekan tombol “<b>Bukan Akar Penyebab</b>” dan lanjutkan ke langkah nomor 8.  Jika <b>tidak terdapat alasan</b> terjadinya penyebab (<i>why 2</i>), tekan tombol “<b>Akar Penyebab</b>” dan lanjutkan ke langkah nomor 14.</li>
                    <li>Isi (ketik) nama penyebab (<b><i>why 3</i></b>) yang merupakan alasan terjadinya penyebab (<i>why 2</i>).</li>
                    <li>Jika <b>terdapat alasan</b> terjadinya penyebab (<i>why 3</i>), tekan tombol “<b>Bukan Akar Penyebab</b>” dan lanjutkan ke langkah nomor 8.  Jika <b>tidak terdapat alasan</b> terjadinya penyebab (<i>why 3</i>), tekan tombol “<b>Akar Penyebab</b>” dan lanjutkan ke langkah nomor 14.</li>
                    <li>Isi (ketik) nama penyebab (<b><i>why 4</i></b>) yang merupakan alasan terjadinya penyebab (<i>why 3</i>).</li>
                    <li>Jika <b>terdapat alasan</b> terjadinya penyebab (<i>why 4</i>), tekan tombol “<b>Bukan Akar Penyebab</b>” dan lanjutkan ke langkah nomor 8.  Jika <b>tidak terdapat alasan</b> terjadinya penyebab (<i>why 4</i>), tekan tombol “<b>Akar Penyebab</b>” dan lanjutkan ke langkah nomor 14.</li>
                    <li>Isi (ketik) nama penyebab (<b><i>why 5</i></b>) yang merupakan alasan terjadinya penyebab (<i>why 4</i>).</li>
                    <li>Tekan tombol “<b>Akar Penyebab</b>”.</li>
                    <li>Pilih <b>jenis penyebab</b>.</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane " id="tab6" role="tabpanel" aria-labelledby="tab6">
          <div class="accordion" id="accordion-tab-6">
            <div class="card">
              <div class="card-header" id="accordion-tab-6-heading-1">
                <h5>
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-6-content-1" aria-expanded="false" aria-controls="accordion-tab-6-content-1"><i class="mdi mdi-swap-vertical float-right"></i> Pengisian Data Respons Risiko (RTP)</button>
                </h5>
              </div>
              <div class="collapse  show " id="accordion-tab-6-content-1" aria-labelledby="accordion-tab-6-heading-1" data-parent="#accordion-tab-6">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Pengelolaan Risiko</b> -> <b>Respons Risiko (RTP)</b>.</li>
                    <li>Tekan tombol <b>“Tambah Data”</b>.</li>
                    <li>Pilih <b>nama penyebab risiko</b> yang akan dimitigasi. Nama penyebab bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Pilih <b>jenis respons risiko</b> (mengurangi kemungkinan/dampak/keduanya).</li>
                    <li>Isi (ketik) <b>nama kegiatan pengendalian</b> yang akan dilaksanakan sebagai respons risiko.</li>
                    <li>Pilih nama <b>Sub Unsur SPIP</b> yang sesuai dengan kegiatan pengendalian tersebut. Sub Unsur SPIP bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Pilih <b>penanggung jawab</b> kegiatan pengendalian. Nama jabatan penanggung jawab kegiatan pengendalian bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Pilih <b>indikator output</b> kegiatan pengendalian.</li>
                    <li>Pilih <b>target waktu</b> pelaksanaan kegiatan pengendalian.</li>
                    <li>Tekan tombol  <b>“Simpan”</b>.</li>
                    <li>Pilih skor kemungkinan <i>treated</i> (skala 1 – 5). Skor kemungkinan <i>treated</i> harus lebih kecil daripada skor kemungkinan <i>residual</i> jika respons risiko adalah mengurangi kemungkinan atau mengurangi kemungkinan dan dampak.</li>
                    <li>Pilih skor dampak <i>treated</i> (skala 1 – 5). Skor dampak <i>treated</i> harus lebih kecil daripada skor dampak <i>residual</i> jika respons risiko adalah mengurangi dampak atau mengurangi kemungkinan dan dampak.</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="accordion-tab-6-heading-2">
                <h5>
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-6-content-2" aria-expanded="false" aria-controls="accordion-tab-6-content-2"><i class="mdi mdi-swap-vertical float-right"></i> Pengisian Data Respons Risiko (Realisasi)</button>
                </h5>
              </div>
              <div class="collapse" id="accordion-tab-6-content-2" aria-labelledby="accordion-tab-6-heading-2" data-parent="#accordion-tab-6">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Pengelolaan Risiko</b> -> <b>Respons Risiko (Realisasi)</b>.</li>
                    <li>Tekan tombol <b>“Tambah Data”</b>.</li>
                    <li>Pilih <b>nama kegiatan pengendalian</b> yang telah dilaksanakan. Nama kegiatan pengendalian bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Masukkan data <b>tanggal realisasi</b> kegiatan pengendalian dengan menekan tombol ▼, lalu memilih tanggal dalam formulir kalender yang tersedia. Data tanggal realisasi kegiatan pengendalian juga dapat diisi dengan cara mengetik tanggal realisasi dalam format <b>bulan/tanggal/tahun</b> (12/31/2020 untuk tanggal 31 Desember 2020).</li>
                    <li>Isi (ketik) <b>nama hambatan</b> jika realisasi kegiatan pengendalian <b>lebih lambat dari jadwal</b> (periode rencana dalam RTP). Jika realisasi kegiatan pengendalian tepat waktu atau lebih cepat dari jadwal, lewati langkah ini.</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane " id="tab7" role="tabpanel" aria-labelledby="tab7">
          <div class="accordion" id="accordion-tab-7">
            <div class="card">
              <div class="card-header" id="accordion-tab-7-heading-1">
                <h5>
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-7-content-1" aria-expanded="false" aria-controls="accordion-tab-7-content-1"><i class="mdi mdi-swap-vertical float-right"></i> Pengisian Data Pemantauan Risiko</button>
                </h5>
              </div>
              <div class="collapse  show " id="accordion-tab-7-content-1" aria-labelledby="accordion-tab-7-heading-1" data-parent="#accordion-tab-7">
                <div class="card-body">
                  <ol>
                    <li>Pilih menu <b>Pengelolaan Risiko</b> -> <b>Pemantauan Risiko</b>.</li>
                    <li>Tekan tombol <b>“Tambah Data”</b>.</li>
                    <li>Isi (ketik) <b>nama kejadian</b> atau risiko yang telah terjadi.</li>
                    <li>Jika kejadian tersebut <b>telah ada dalam register risiko</b>, tekan tombol <b>Ya</b>. Jika kejadian tersebut <b>belum ada dalam register risiko</b>, tekan tombol <b>Tidak</b> dan lanjutkan ke langkah nomor 8.</li>
                    <li>Pilih <b>nama risiko</b> sesuai kejadian pada langkah 3. Nama risiko bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Jika risiko tersebut <b>telah dianalisis penyebabnya</b>, tekan tombol <b>Ya</b>. Jika risiko tersebut <b>belum ada dianalisis penyebabnya</b>, tekan tombol <b>Tidak</b> dan lanjutkan ke langkah nomor 8.</li>
                    <li>Pilih <b>nama penyebab risiko</b>. Nama penyebab bisa dicari dengan mengetik satu/lebih kata kunci.</li>
                    <li>Masukkan data <b>waktu kejadian</b> dengan menekan tombol ▼, lalu memilih tanggal dalam formulir kalender yang tersedia. Data waktu kejadian juga dapat diisi dengan cara mengetik tanggal realisasi dalam format <b>bulan/tanggal/tahun</b> (12/31/2020 untuk tanggal 31 Desember 2020).</li>
                    <li>Isi (ketik) nama <b>tempat kejadian</b>.</li>
                    <li>Pilih <b>skor dampak</b> (skala 1 – 5). </li>
                    <li>Isi (ketik) nama <b>pemicu kejadian</b>.</li>
                    <li>Tekan tombol <b>“Simpan”</b>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>



  </div>
</div>


@endsection
