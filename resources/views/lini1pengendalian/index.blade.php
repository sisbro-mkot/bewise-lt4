@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pengendalian yang Sudah Ada</li>
  </ol>
  <h6 class="slim-pagetitle">Pengendalian yang Sudah Ada</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Kode Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Nama Pengendalian</th>
          <th style="text-align: center;">Klasifikasi Sub Unsur SPIP</th>
          <th style="text-align: center;">Tahun</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($pengendalian as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td style="text-align: center;">{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_pengendalian}}</td>
          <td>{{$item->nama_sub_unsur}}</td>
          <td style="text-align: center;">{{$item->tahun}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
     
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
