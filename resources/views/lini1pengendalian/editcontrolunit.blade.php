@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Analisis Risiko Unit Kerja</li>
  </ol>
  <h6 class="slim-pagetitle">Pengendalian yang Sudah Ada</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Edit Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1pengendalian.update', $pengendalian->id_pengendalian)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$pengendalian->id_pengendalian}}" id="id_pengendalian" name="id_pengendalian" hidden>
        </div>
      </div>

      <div class="form-group" id="data-umum">
        <label for="id_identifikasi" class="col-sm-2 control-label">Nama Risiko</label>
        <div class="col-sm-12">
          <select name="id_identifikasi" class="form-control" id="id_identifikasi" autofocus>
            @foreach($id_identifikasi as $key)
              <option value="{{$key->id_identifikasi}}" {{$pengendalian->id_identifikasi == $key->id_identifikasi ? 'selected' : ''}}>{{$key->kode_identifikasi_risiko}} - {{$key->nama_bagan_risiko}}></option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_identifikasi_read" class="col-sm-12 control-label">Nama Risiko</label>
        <div class="col-sm-12">
          <input class="form-control" value="{{$risiko->kode_identifikasi_risiko}} - {{$risiko->nama_bagan_risiko}}" name="id_identifikasi_read" id="id_identifikasi_read" readonly></input>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_pengendalian" class="col-sm-12 control-label">Nama pengendalian yang sudah ada (existing control)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_pengendalian">{{$pengendalian->nama_pengendalian}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="id_sub_unsur" class="col-sm-2 control-label">Sub Unsur SPIP</label>
        <div class="col-sm-12">
          <select name="id_sub_unsur" class="form-control" id="id_sub_unsur" autofocus>
            @foreach($id_sub_unsur as $key => $value)
              <option value="{{$key}}" {{$pengendalian->id_sub_unsur == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <br/>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1analisis.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-umum").hide();
    $("#id_identifikasi").select2();
    $("#id_sub_unsur").select2();

});
</script>
@endpush


