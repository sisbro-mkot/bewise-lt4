<!DOCTYPE html>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Monitoring Risiko</title>
    <style type="text/css">
    table {
    border-collapse: collapse;
    }
    table, th, td {
    border: 1px solid;
    }
    th, td {
    padding: 3;
    }
    .kop {width:100%; text-align: left;margin-bottom: 5px; border:none;}
    .kop tr td{border:none;}

    .kopsurat{text-align: center;}
    .kopsurat1{line-height:15px;text-align: center;font-family:sans-serif !important; font-size: 18px; margin-bottom: 6px;font-weight: bold}
    .kopsurat2{line-height:11px;text-align: center;font-family:sans-serif; font-size: 16px }
  </style>
</head><body>

<div align="center">
  <table class="kop">
    <tr>
        <td class="kopsurat" style="width:90%;">
          <p class="kopsurat1">PEMANTAUAN TERHADAP KETERJADIAN RISIKO</p>
        </td>
    </tr>
    <tr>
        <td>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Nama Unit Pemilik Risiko: {{$unit->s_nama_instansiunitorg}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Tahun: {{$tahun}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Triwulan: {{$triwulan}}</p>
        </td>
    </tr>
  </table>

</div>

   <div>
      <table width="100%" style="font-family: sans-serif; font-size: 12px;">
        <thead>
        <tr align="center">
          <th width="3%">No.</th>
          <th>Nama Kejadian</th>
          <th>Pernyataan Risiko</th>
          <th>Waktu Kejadian</th>
          <th>Tempat Kejadian</th>
          <th>Skor Dampak</th>
          <th>Pemicu Keterjadian</th>
        </tr>
        <tr style="background-color: #BDBDBD; font-style: italic; font-size: 8px" align="center">
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
        </tr>
        </thead>
        <?php $no=1; ?>
        @foreach($monitoring as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_kejadian}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td style="text-align: center;">{{Carbon\Carbon::parse($item->waktu_kejadian)->format('d/m/Y')}}</td>
          <td>{{$item->tempat_kejadian}}</td>
          <td style="text-align: center;">{{$item->skor_dampak}}</td>
          <td>{{$item->pemicu_kejadian}}</td>
        </tr>
        @endforeach
      </table>
    </div>

</body></html>