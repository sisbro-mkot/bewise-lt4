@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pemantauan Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Pemantauan Keterjadian Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <div class="card-header">
    <a href="{{url('createmonitorunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
<!--     <a href="{{url('lini1monitoring')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a> -->
  </div>
  @endif
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}
    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Periode Kejadian</th>
          <th style="text-align: center;">Nama Kejadian</th>
          <th style="text-align: center;">Nama Penyebab</th>
          <th style="text-align: center;">Waktu Kejadian</th>
          <th style="text-align: center;">Tempat Kejadian</th>
          <th style="text-align: center;">Skor Dampak</th>
          <th style="text-align: center;">Pemicu Kejadian</th>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <th></th>
          @endif
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($monitoring as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td style="text-align: center;">{{$item->nama_periode}}</td>
          <td>{{$item->nama_kejadian}}</td>
          <td>{{$item->nama_akar_penyebab}}</td>
          <td style="text-align: center;">{{Carbon\Carbon::parse($item->waktu_kejadian)->format('d M Y')}}</td>
          <td>{{$item->tempat_kejadian}}</td>
          <td style="text-align: center;">{{$item->skor_dampak}}</td>
          <td>{{$item->pemicu_kejadian}}</td>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <td>
            <a href="{{route('lini1monitoring.edit', $item->id)}}" class="btn btn-success btn-xs"><i class="icon ion-edit"></i> Edit</a>
          </td>
          @endif
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "columnDefs": [ {
        "targets": 8,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@else
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@endif