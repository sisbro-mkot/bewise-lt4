@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pemantauan Risiko</li>
  </ol>

  <h6 class="slim-pagetitle">Pemantauan Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->

 
<div class="card card-table">
  <div class="card-header">
    <a href="{{url('createmonitorunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    <a href="{{url('lini1monitoring')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data Pemantauan Risiko</a>
  </div>
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="5%">No.</th>
          <th style="text-align: center;">Nama Kejadian</th>
          <th style="text-align: center;">Kode Risiko</th>
          <th width="10%" style="text-align: center;">Waktu Kejadian</th>
          <th style="text-align: center;">Tempat Kejadian</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
      @foreach($monitoring as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_kejadian}}</td>
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{Carbon\Carbon::parse($item->waktu_kejadian)->format('d M Y')}}</td>
          <td>{{$item->tempat_kejadian}}</td>
        </tr>
      @endforeach
        </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "order": [[ 0, "asc" ]],

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
