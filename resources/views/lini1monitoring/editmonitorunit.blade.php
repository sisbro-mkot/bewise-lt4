@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Pemantauan Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
  </ol>
  <h6 class="slim-pagetitle">Pemantauan Keterjadian Risiko {{$nama_instansiunitorg->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Edit Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1monitoring.update', $monitoring->id_keterjadian)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$monitoring->id_keterjadian}}" id="id_keterjadian" name="id_keterjadian" hidden>
        </div>
      </div>

      <div class="form-group" id="data-umum">
        <label for="s_kd_jabdetail" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail" class="form-control" id="s_kd_jabdetail" autofocus>
            @foreach($s_kd_jabdetail as $key)
              <option value="{{$key->s_kd_jabdetail}}" {{$monitoring->s_kd_jabdetail == $key->s_kd_jabdetail ? 'selected' : ''}}>{{$key->s_nmjabdetail_pemilik}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_kejadian" class="col-sm-12 control-label">Nama Kejadian</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_kejadian" id="nama_kejadian">{{$monitoring->nama_kejadian}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="pilihan" class="col-sm-12 control-label" >Apakah kejadian tersebut telah ada dalam register risiko?</label>
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="risikoya" onclick="Yes1()"> Ya  </button>
          <button class="btn btn-danger" id="risikotidak" onclick="No1()"> Tidak</button>
        </div>
      </div>

      <div class="form-group" id="identifikasi-1">
        <label for="id_identifikasi" class="col-sm-12 control-label" id="label_id_identifikasi">Nama Risiko</label>
        <div class="col-sm-12">
          <select name="id_identifikasi" class="form-control" id="id_identifikasi">
          <option value="{{$risiko->id_identifikasi}}" selected="true">{{$risiko->nama_bagan_risiko}}</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="pilihan" class="col-sm-12 control-label" >Apakah kejadian tersebut telah dianalisis penyebabnya?</label>
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebabya" onclick="Yes2()"> Ya  </button>
          <button class="btn btn-danger" id="sebabtidak" onclick="No2()"> Tidak</button>
        </div>
      </div>

      <div class="form-group" id="penyebab-1">
        <label for="id_penyebab" class="col-sm-12 control-label" id="label_id_penyebab">Nama Penyebab</label>
        <div class="col-sm-12">
          <select name="id_penyebab" class="form-control" id="id_penyebab">
          <option value="{{$penyebab->id_penyebab}}" selected="true">{{$penyebab->nama_akar_penyebab}}</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="waktu_kejadian" class="col-sm-2 control-label">Waktu Kejadian</label>
        <div class="col-sm-2">
          <input type="date" class="form-control" id="waktu_kejadian" name="waktu_kejadian" value="{{$monitoring->waktu_kejadian}}">
        </div>
      </div>

      <div class="form-group">
        <label for="tempat_kejadian" class="col-sm-12 control-label">Tempat kejadian</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="tempat_kejadian" id="tempat_kejadian">{{$monitoring->tempat_kejadian}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="skor_dampak" class="col-sm-2 control-label">Skor Dampak :</label>
        <div class="col-sm-2">
          <select class="form-control" name="skor_dampak" id="skor_dampak">
            @foreach($skor_dampak as $per)
            <option value="{{$per}}" {{$monitoring->skor_dampak == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="pemicu_kejadian" class="col-sm-12 control-label">Pemicu kejadian</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="pemicu_kejadian" id="pemicu_kejadian">{{$monitoring->pemicu_kejadian}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1monitoring.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $('button[type!=submit]').click(function(){
      return false;
  });

  $(document).ready(function() {
    $("#data-umum").hide();
    $("#s_kd_jabdetail").select2();
    $("#id_identifikasi").select2();
    $("#id_penyebab").select2();
    $("#skor_dampak").select2();

  });

    function Yes1() {
    $("#identifikasi-1").show();
    var RisikoID = $('#s_kd_jabdetail').val();
        console.log(RisikoID);
        if(RisikoID) {
            $.ajax({
                url: '../lini2evaluasi/pilihRisikoUnit2/'+RisikoID,
                type: 'get',
                dataType: 'json',
                success:function(data) {
                  console.log(data);
                    $('select[name="id_identifikasi"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="id_identifikasi"]').append('<option value="'+ value.id_identifikasi +'">'+ value.nama_bagan_risiko + '</option>');
                    });

                }
            });
        } else {
            $('select[name="id_identifikasi"]').empty();
        }
    }

    function No1() {
        $("#identifikasi-1").hide();
        $('select[name="id_identifikasi"]').empty();
    }

    function Yes2() {
    $("#penyebab-1").show();
    var SebabID = $('#id_identifikasi').val();
        console.log(SebabID);
        if(SebabID) {
            $.ajax({
                url: '../lini2monitoring/pilihSebab2/'+SebabID,
                type: 'get',
                dataType: 'json',
                success:function(data) {
                  console.log(data);
                    $('select[name="id_penyebab"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="id_penyebab"]').append('<option value="'+ value.id_penyebab +'">'+ value.nama_akar_penyebab + '</option>');
                    });

                }
            });
        } else {
            $('select[name="id_penyebab"]').empty();
        }
    }

    function No2() {
        $("#penyebab-1").hide();
        $('select[name="id_penyebab"]').empty();
    }

</script>
@endpush

