<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Sistem Informasi Pengelolaan Risiko">
    <meta name="author" content="BPKP">

    <title>BPKP Wide Risk Management (Be Wise) | Login</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{URL::asset('images/favicon.png')}}" type="image/x-icon">
    <!-- Vendor css -->
    <link href="{{asset('app/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('app/css/slim.css')}}">
    <style type="text/css">
      .signin-box {width: 500px}
      .signin-box p{color:#555;font-size: 14px;text-align: justify;}
      .signin-box .signin-title-primary,.signin-title-secondary{text-align: center;}
      .signin-left {
        background: url({{asset('images/bg_new2.jpg')}});
        background-size:cover;
      }
      .signin-box .credit{text-align: center;}
      .btn-signin{background-image: linear-gradient(to right, #F60 0%, #C00 100%) !important;}
      
    </style>
  </head>
  <body>
    <form action="{{route('peran')}}" method="post">
    {{ csrf_field() }}
    <div class="d-md-flex flex-row-reverse">

      <div class="signin-right">
        <!-- <input name="_token" type="hidden" value="{{ csrf_token() }}"/> -->
          <div class="signin-box">
            <h2 class="signin-title-primary pd-r-3"><img class="logo" src="{{asset('images/logo.png')}}" width="150px"></h2>
            <h2 class="signin-title-primary">BPKP <i>Wide Risk Management</i></h2>
            <!-- <h3 class="signin-title-secondary tx-16">Sign in dengan menggunakan akun warga BPKP (SSO).</h3> -->

            <div class="form-group">
              <h3 class="signin-title-secondary tx-16 mb-2 mt-5">Pilih peran:</h3>
            </div><!-- form-group -->

            <div class="form-group mg-b-40">
              <select class="form-control select2" data-placeholder="Pilih Peran" name="peran" id="peran">
                @foreach($roles as $key => $item)
                  <option value="{{$item->id}}">{{$item->nama_role}}</option>
                @endforeach
              </select>
            </div>
            <button class="btn btn-primary btn-block btn-signin">Sign In</button>

            <p class="credit">&copy; Copyright 2019 BPKP. All Rights Reserved.</p>
          </div>
        
      </div><!-- signin-right -->
      <div class="signin-left">
        <div class="signin-box">
        </div>
      </div><!-- signin-left -->
    </div><!-- d-flex -->
    </form>

@push('js')
<script type="text/javascript">
  $(document).ready(function() {

      $('#peran').select2();
 
});

</script>
@endpush

    <script src="{{asset('app/lib/jquery/js/jquery.js')}}"></script>
    <script src="{{asset('app/lib/popper.js/js/popper.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('app/js/slim.js')}}"></script>
    <script src="{{asset('app/js/rolex.js')}}"></script>
  </body>
</html>
