@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Bagan Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Bagan Risiko</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if (Auth::user()->role_id == '1')
  <div class="card-header">
    <a href="{{url('createparamrisiko')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
  </div>
  @endif
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Kategori Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Metode Pencapaian Tujuan SPIP</th>
          @if (Auth::user()->role_id == '1')
          <th> </th>
          @endif
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($risiko as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_kategori_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_tujuan_spip}}</td>
          @if (Auth::user()->role_id == '1')
          <td>
            <a href="{{route('lini2paramrisiko.edit', $item->id)}}" class="btn btn-success btn-xs"><i class="icon ion-edit"></i> Edit</a>
          </td>
          @endif
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
