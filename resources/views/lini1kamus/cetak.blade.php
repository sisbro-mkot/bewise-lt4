<!DOCTYPE html>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Kamus Risiko</title>
    <style type="text/css">
    table {
    border-collapse: collapse;
    }
    table, th, td {
    border: 1px solid;
    }
    th, td {
    padding: 3;
    }
    .kop {width:100%; text-align: left;margin-bottom: 5px; border:none;}
    .kop tr td{border:none;}

    .kopsurat{text-align: center;}
    .kopsurat1{line-height:15px;text-align: center;font-family:sans-serif !important; font-size: 18px; margin-bottom: 6px;font-weight: bold}
    .kopsurat2{line-height:11px;text-align: center;font-family:sans-serif; font-size: 16px }
  </style>
</head><body>

<div align="center">
  <table class="kop">
    <tr>
        <td class="kopsurat" style="width:90%;">
          <p class="kopsurat1">KAMUS RISIKO</p>
        </td>
    </tr>
  </table>

</div>

   <div>
      <table width="100%" style="font-family: sans-serif; font-size: 12px;">
        <thead>
        <tr align="center">
          <th width="3%">No.</th>
          <th width="15%">Kategori Risiko</th>
          <th>Nama Risiko</th>
          <th>Metode Pencapaian Tujuan SPIP</th>
        </tr>
        <tr style="background-color: #BDBDBD; font-style: italic; font-size: 8px" align="center">
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
        </tr>
        </thead>
        <?php $no=1; ?>
        @foreach($kamus as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_kategori_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_tujuan_spip}}</td>
        </tr>
        @endforeach
      </table>
    </div>

</body></html>