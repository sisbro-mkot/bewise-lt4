@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Respons Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Realisasi Kegiatan Pengendalian Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <div class="card-header">
    @if($rca == 0)
    <p style="color: black;">Belum ada data RTP.</p> 
    <p style="color: black;">Silahkan tambah data di menu Pengelolaan Risiko - Respons Risiko (RTP).</p>
    @elseif($data > 0)
    <a href="{{url('createrealunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    @else
    <p style="color: black;">Seluruh RTP telah dilaksanakan.</p>
    @endif
<!--     <a href="{{url('lini1realisasi')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a> -->
  </div>
  @endif
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}
    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="10%">Kode Penyebab</th>
          <th style="text-align: center;">Nama Kegiatan Pengendalian</th>
          <th style="text-align: center;">Penanggung Jawab Kegiatan</th>
          <th style="text-align: center;">Periode Rencana</th>
          <th style="text-align: center;">Waktu Realisasi</th>
          <th style="text-align: center;">Nama Hambatan</th>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <th></th>
          @endif
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($realisasi as $item)
        <tr class="item{{$item->id}}">
         <!--  <td>{{$no++}}</td> -->
          <td>{{$item->kode_penyebab}}</td>
          <td>{{$item->kegiatan_pengendalian}}</td>
          <td>{{$item->s_nmjabdetail}}</td>
          <td style="text-align: center;">{{$item->nama_periode_rencana}}</td>
          <td style="text-align: center;">{{Carbon\Carbon::parse($item->realisasi_waktu)->format('d M Y')}}</td>
          <td>{{$item->nama_hambatan}}</td>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <td>
            <a href="{{route('lini1realisasi.edit', $item->id)}}" class="btn btn-success btn-xs"><i class="icon ion-edit"></i> Edit</a>
          </td>
          @endif
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "columnDefs": [ {
        "targets": 6,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@else
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@endif
