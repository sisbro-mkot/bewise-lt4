@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Respons Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
  </ol>
  <h6 class="slim-pagetitle">Realisasi Kegiatan Pengendalian {{$nama_instansiunitorg->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Edit Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1realisasi.update', $realisasi->id_pemantauan)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$realisasi->id_pemantauan}}" id="id_pemantauan" name="id_pemantauan" hidden>
        </div>
      </div>

      <div class="form-group" id="data-umum">
        <label for="id_rtp" class="col-sm-12 control-label">Nama Kegiatan Pengendalian</label>
        <div class="col-sm-12">
          <select name="id_rtp" class="form-control" id="id_rtp" autofocus>
            @foreach($id_rtp as $key => $value)
              <option value="{{$key}}" {{$realisasi->id_rtp == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_rtp_read" class="col-sm-12 control-label">Nama Kegiatan Pengendalian</label>
        <div class="col-sm-12">
          <input class="form-control" value="{{$respons->kegiatan_pengendalian}}" name="id_rtp_read" id="id_rtp_read" readonly></input>
        </div>
      </div>

      <div class="form-group">
        <label for="periode_rencana" class="col-sm-2 control-label">Periode Rencana</label>
        <div class="col-sm-2">
          <input class="form-control" value="" name="periode_rencana" id="periode_rencana" readonly></input>
        </div>
      </div>

      <div class="form-group">
        <label for="realisasi_waktu" class="col-sm-2 control-label">Tanggal Realisasi</label>
        <div class="col-sm-2">
          <input type="date" class="form-control" id="realisasi_waktu" name="realisasi_waktu" value="{{$realisasi->realisasi_waktu}}">
        </div>
      </div>

      <div class="form-group">
        <label for="nama_hambatan" class="col-sm-12 control-label">Nama Hambatan (diisi jika realisasi terlambat)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_hambatan" id="nama_hambatan">{{$realisasi->nama_hambatan}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1realisasi.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-umum").hide();
    $("#id_rtp").select2();
 
});

</script>
@endpush


@push('js1')
  <script type="text/javascript">
  $(document).ready(function() {

            var RTPID = $('#id_rtp').val();
            console.log(RTPID);
            if(RTPID) {
                $.ajax({
                    url: '../../lini1realisasi/pilihPeriodeRTP/'+RTPID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $.each(data, function(key, value) {
                          var input = value.nama_periode_rencana;
                          var text2 = document.getElementById("periode_rencana");
                        text2.value = input;
                        });

                    }
                });
            } else {
                $('#periode_rencana').val() = "";
            };

  });
  $(document).ready(function() {
        $('#id_rtp').select2().on('change', function() {
            var RTPID = $(this).val();
            console.log(RTPID);
            if(RTPID) {
                $.ajax({
                    url: '../../lini1realisasi/pilihPeriodeRTP/'+RTPID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $.each(data, function(key, value) {
                          var input = value.nama_periode_rencana;
                          var text2 = document.getElementById("periode_rencana");
                        text2.value = input;
                        });

                    }
                });
            } else {
                $('select[name="periode_rencana"]').empty();
            }


        });

        $('#periode_rencana').select2()

  });
  </script>
@endpush

