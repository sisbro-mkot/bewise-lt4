@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Data Umum</li>
  </ol>
  <h6 class="slim-pagetitle">Data Umum Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h5>Nama Unit Kerja:</h5>
    <h5 style="color: black;">{{$namaunit->s_nama_instansiunitorg}}</h5>
  <br/>
    <h5>Pemilik Risiko:</h5>
    <h5 style="color: black;">{{$dataumum->s_kd_jabdetail_pemilik}}</h5>
  <br/>
    <h5>Koordinator Pengelola Risiko:</h5>
    <h5 style="color: black;">{{$dataumum->s_kd_jabdetail_koordinator}}</h5>
  <br/>
  </div>


  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '4'|Auth::user()->role_id == '6'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1data.update', $data->id_data_umum)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="hidden" name="id_data_umum" value="{{$data->id_data_umum}}">

    <div class="box-body">
        <label for="skor_selera" class="col-sm-2 control-label">Skor Selera Risiko :</label>
        <div class="col-sm-2">
          <select class="form-control" name="skor_selera" id="skor_selera">
            @foreach($skor_selera as $per)
            <option value="{{$per}}" {{old('kskor_selera') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
      <br/>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1data.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif


</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $("#skor_selera").select2();
    });
  </script>
@endpush