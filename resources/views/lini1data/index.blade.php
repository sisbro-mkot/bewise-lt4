@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Data Umum</li>
  </ol>
  <h6 class="slim-pagetitle">Data Umum Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h5>Nama Unit Kerja:</h5>
    <h5 style="color: black;">{{$namaunit->s_nama_instansiunitorg}}</h5>
  <br/>
    <h5>Pemilik Risiko:</h5>
    <h5 style="color: black;">{{$data->s_kd_jabdetail_pemilik}}</h5>
  <br/>
    <h5>Koordinator Pengelola Risiko:</h5>
    <h5 style="color: black;">{{$data->s_kd_jabdetail_koordinator}}</h5>
  <br/>
    <h5>Skor Selera Risiko:</h5>
    <h5 style="color: black;">{{$data->skor_selera}}</h5>
  <br/>
  @if (Auth::user()->role_id == '4'|Auth::user()->role_id == '6'|Auth::user()->role_id == '10')
    <a href="{{ URL::to('lini1data/' . $data->id . '/edit') }}" class="btn btn-primary"><i class="icon ion-android-create"></i> Ubah skor selera risiko</a>
  @endif
  </div>
  
</div>
@endsection
