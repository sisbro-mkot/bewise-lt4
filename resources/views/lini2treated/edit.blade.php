@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Respons Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Respons Risiko</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6>Nama Pemilik Risiko:</h6>
    <h6 style="color: black;">{{$s_nmjabdetail->nama}}</h6>
  </div>
  <div class="card-header">
    <h6>Nama Risiko:</h6>
    <h6 style="color: black;">{{$nama_bagan_risiko->risiko}}</h6>
  </div>
  <!-- /.box-header -->
  <div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Residual Risk</h6>
  </div>
    <form class="form-horizontal mt-2">
    <div class="box-body">
      <div class="form-inline">
        <label for="kemungkinan_residual" class="col-sm-2 control-label">Kemungkinan Residual :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$residual->skor_kemungkinan}}" name="kemungkinan_residual" id="kemungkinan_residual" readonly></input>
        </div>
        <label for="dampak_residual" class="col-sm-2 control-label">Dampak Residual :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$residual->skor_dampak}}" name="dampak_residual" id="dampak_residual" readonly></input>
        </div>
        <label for="id_matriks_residual" class="col-sm-2 control-label">Level Risiko Residual :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$residual->skor_risiko}}" name="id_matriks_residual" id="id_matriks_residual" readonly></input>
        </div>
      </div>
      <br/>
    </div>
      <!-- /.box-body -->
    </form>
  </div>


  <div class="pd-20">
    <center><h5 style="color: black;">Rencana Tindak Pengendalian</h5></center>
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display responsive">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Nama Penyebab</th>
          <th style="text-align: center;">Respons Risiko</th>
          <th style="text-align: center;">Kegiatan Pengendalian</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($treated as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_akar_penyebab}}</td>
          <td>
            @if($item->respon_risiko == "K")
                    Mengurangi Kemungkinan 
            @elseif($item->respon_risiko == "D")
                    Mengurangi Dampak
            @else($item->respon_risiko == "B")
                    Mengurangi Kemungkinan dan Dampak
            @endif
          </td>
          <td>{{$item->kegiatan_pengendalian}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
</div>

<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Penilaian Treated Risk</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini2treated.update', $analisis->id_analisis)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="hidden" name="id_analisis" value="{{$analisis->id_analisis}}">

    <div class="box-body">

      <div class="form-inline">
        <label for="kemungkinan_treated" class="col-sm-2 control-label">Kemungkinan Treated :</label>
        <div class="col-sm-2">
          <select class="form-control" name="kemungkinan_treated" id="kemungkinan_treated">
            @foreach($kemungkinan_treated as $per)
            <option value="{{$per}}" {{old('kemungkinan_treated') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="dampak_treated" class="col-sm-2 control-label">Dampak Treated :</label>
        <div class="col-sm-2">
          <select class="form-control" name="dampak_treated" id="dampak_treated">
            @foreach($dampak_treated as $per)
            <option value="{{$per}}" {{old('dampak_treated') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="id_matriks_treated" class="col-sm-2 control-label">Level Risiko Treated :</label>
        <div class="col-sm-2">
          <select name="id_matriks_treated" class="form-control" id="id_matriks_treated">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Kemungkingan/Dampak Risiko Treated--</option>
          </select>
        </div>
      </div>

      <br/>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2respons.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
  </div>

</div>
@stop


@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

    $(document).ready(function() {
    $("#kemungkinan_treated").select2();
    $("#dampak_treated").select2();
    $("#id_matriks_treated").select2();

});

  </script>
@endpush

@push('js1')
  <script type="text/javascript">

  $(document).ready(function() {

        $('#kemungkinan_treated').select2().on('change', function() {
            var kemungkinanID = $(this).val();
            var dampakID = $('#dampak_treated').val();
            console.log(kemungkinanID);
            if(kemungkinanID) {
                $.ajax({
                    url: '/bewise20/lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_treated"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_treated"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_treated"]').empty();
            }
        });

        $('#dampak_treated').select2().on('change', function() {
            var dampakID = $(this).val();
            var kemungkinanID = $('#kemungkinan_treated').val();
            console.log(dampakID);
            if(dampakID) {
                $.ajax({
                    url: '/bewise20/lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_treated"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_treated"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_treated"]').empty();
            }
        });

        $('#id_matriks_treated').select2()

  });
  </script>
@endpush