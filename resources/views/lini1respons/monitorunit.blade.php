@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Respons Risiko</li>
  </ol>

  <h6 class="slim-pagetitle">Respons Risiko {{$unit->s_nama_instansiunitorg}} yang Telah Terlaksana</h6>
</div><!-- slim-pageheader -->

 
<div class="card card-table">
  <div class="card-header">
    <a href="{{url('createrealunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    <a href="{{url('lini1realisasi')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data Respons Risiko (Realisasi)</a>
  </div>
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="5%">Kode Penyebab</th>
          <th style="text-align: center;">Pernyataan Risiko</th>
          <th style="text-align: center;">Kegiatan Pengendalian</th>
          <th width="10%" style="text-align: center;">Realisasi Waktu</th>
        </tr>
      </thead>
      <tbody>
      @foreach($respons as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_penyebab}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->kegiatan_pengendalian}}</td>
          <td style="text-align: center;">{{Carbon\Carbon::parse($item->realisasi_waktu)->format('d M Y')}}</td>
        </tr>
      @endforeach
        </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';

    $('#tbl-identifikasi').DataTable({
      "order": [[ 0, "asc" ]],

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
