<!DOCTYPE html>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Analisis Risiko</title>
    <style type="text/css">
    table {
    border-collapse: collapse;
    }
    table, th, td {
    border: 1px solid;
    }
    th, td {
    padding: 3;
    }
    .kop {width:100%; text-align: left;margin-bottom: 5px; border:none;}
    .kop tr td{border:none;}

    .kopsurat{text-align: center;}
    .kopsurat1{line-height:15px;text-align: center;font-family:sans-serif !important; font-size: 18px; margin-bottom: 6px;font-weight: bold}
    .kopsurat2{line-height:11px;text-align: center;font-family:sans-serif; font-size: 16px }
  </style>
</head><body>

<div align="center">
  <table class="kop">
    <tr>
        <td class="kopsurat" style="width:90%;">
          <p class="kopsurat1">ANALISIS RISIKO</p>
        </td>
    </tr>
    <tr>
        <td>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Nama Unit Pemilik Risiko: {{$unit->s_nama_instansiunitorg}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Tahun: {{$tahun}}</p>
        </td>
    </tr>
  </table>

</div>

   <div>
      <table width="100%" style="font-family: sans-serif; font-size: 12px;">
        <thead>
        <tr align="center">
          <th width="10%" rowspan="2">Kode Risiko</th>
          <th rowspan="2">Pernyataan Risiko</th>
          <th colspan="3">Skor/Nilai Risiko yang Melekat</th>
          <th colspan="4">Pengendalian yang Ada</th>
          <th colspan="3">Skor/Nilai Risiko Residu setelah Adanya Pengendalian</th>
        </tr>
        <tr align="center">
          <th width="5%" >Skor Probabilitas</th>
          <th width="5%" >Skor Dampak</th>
          <th width="5%" >Level Risiko</th>
          <th width="5%" >Ada/ Belum Ada</th>
          <th>Uraian</th>
          <th>Klasifikasi Sub Unsur SPIP</th>
          <th width="5%" >Memadai/ Belum Memadai</th>
          <th width="5%" >Skor Probabilitas</th>
          <th width="5%" >Skor Dampak</th>
          <th width="5%" >Level Risiko</th>
        </tr>
        <tr style="background-color: #BDBDBD; font-style: italic; font-size: 8px" align="center">
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
          <th>9</th>
          <th>10</th>
          <th>11</th>
          <th>12</th>
        </tr>
        </thead>
        <?php $no=1; ?>
        @foreach($analisis as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td align="center">{{$item->skor_kemungkinan_inherent}}</td>
          <td align="center">{{$item->skor_dampak_inherent}}</td>
          <td align="center">{{$item->skor_risiko_inherent}}</td>
          <td align="center">
            @if($item->existing_control == "Y")
                    Ada 
            @else($item->existing_control == "T")
                    Belum Ada
            @endif
          </td>
          <td>{{$item->uraian_existing_control}}</td>
          <td>{{$item->nama_sub_unsur}}</td>
          <td align="center">
            @if($item->existing_control_memadai == "Y")
                    Memadai 
            @else($item->existing_control_memadai == "T")
                    Belum Memadai
            @endif
          </td>
          <td align="center">{{$item->skor_kemungkinan_residual}}</td>
          <td align="center">{{$item->skor_dampak_residual}}</td>
          <td align="center">{{$item->skor_risiko_residual}}</td>
        </tr>
        @endforeach
      </table>
    </div>

</body></html>