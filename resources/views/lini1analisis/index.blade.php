@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Analisis Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Analisis Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <div class="card-header">
    @if($risiko == 0)
    <p style="color: black;">Belum ada data identifikasi risiko.</p> 
    <p style="color: black;">Silahkan tambah data di menu Pengelolaan Risiko - Identifikasi Risiko.</p>
    @elseif($data > 0)
    <a href="{{url('createanunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    @else
    <p style="color: black;">Seluruh risiko yang teridentifikasi telah dianalisis.</p>
    @endif
    <!-- <a href="{{url('lini2analisis')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a> -->
  </div>
  @endif
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="10%" rowspan="2">Kode Risiko</th>
          <th style="text-align: center;" rowspan="2">Nama Risiko</th>
          <th style="text-align: center;" rowspan="2">Uraian Dampak</th>
          <th width="5%" style="text-align: center;" rowspan="2">Nilai Inherent Risk</th>
          <th style="text-align: center;" colspan="2">Pengendalian yang Sudah Ada&nbsp;&nbsp;<a href="{{url('lini1pengendalian')}}"><i style="color: DodgerBlue;"class="fa fa-info-circle"></i></a></th>
          <th width="5%" style="text-align: center;" rowspan="2">Nilai Residual Risk</th>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <th style="text-align: center;" rowspan="2"></th>
          @endif
        </tr>
        <tr>
          <th style="text-align: center;">Jumlah Pengendalian</th>
          <th style="text-align: center;">Memadai/Tidak Memadai</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($analisis as $item)
        <tr class="item{{$item->id}}">
          <!-- <td>{{$no++}}</td> -->
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_dampak}}</td>
          <td style="text-align: center;">{{$item->skor_risiko_inherent}}</td>
          @if($item->count_pengendalian == 0)
          <td style="text-align: center;">-&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="{{$item->id}}" data-risiko="{{$item->nama_bagan_risiko}}" onclick="showModal(this)"></i></td>
          @else
          <td style="text-align: center;">{{$item->count_pengendalian}}&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="{{$item->id}}" data-risiko="{{$item->nama_bagan_risiko}}" onclick="showModal(this)"></i></td>
          @endif
          <td style="text-align: center;">
            @if($item->count_pengendalian == 0)
                  
            @elseif($item->skor_risiko_residual <= $item->skor_selera)
                    Memadai 
            @else
                    Tidak Memadai
            @endif
          </td>
          <td style="text-align: center;">{{$item->skor_risiko_residual}}</td>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <td>
            <a href="{{route('lini1analisis.edit', $item->id)}}" class="btn btn-success btn-xs"><i class="icon ion-edit"></i> Edit</a>
          </td>
          @endif
        </tr>

      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>

        <!-- The Modal -->

        <div class="modal" tabindex="-1" id="modalTable" aria-hidden="true">
          <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                <h5 class="modal-title">Pengendalian yang sudah ada atas risiko "<span id="Risiko"></span>"</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="card card-table">
                @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
                <div class="card-header">
                <a href="" class="btn btn-primary" id="tambahdata"><i class="icon ion-plus-round"></i> Tambah Data</a>
                </div>
                @endif
                <div class="pd-20">
                <div class="table-responsive-lg">
                <div class="table-wrapper">
                <table id="mytable" class="table">
                    <thead>
                        <tr>
                            <th class="col-xs-1" data-field="nomor">No.</th>
                            <th class="col-xs-2" data-field="pengendalian">Nama Pengendalian</th>
                            <th class="col-xs-2" data-field="spip">Klasifikasi Sub Unsur SPIP</th>
                            <th class="col-xs-1" data-field="tahun">Tahun</th>
                            @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
                            <th class="col-xs-1" data-field="edit"></th>
                            @endif
                        </tr>
                     </thead>
                </table>
                </div>
                </div>
                </div>
                </div>
              </div>

            </div>
          </div>
        </div>

@endsection

@if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "columnDefs": [ {
        "targets": 6,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@else
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@endif


@if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
@push('js1')
  <script type="text/javascript">
    function showModal(ele) 
      {
          var id= $(ele).attr('data-id');
          var risiko= $(ele).attr('data-risiko');
          document.getElementById("Risiko").innerHTML = risiko;
          console.log(id);
          document.getElementById("tambahdata").href = "lini1pengendalian/" + id +"/tambah";
          

          $.ajax({
              url: '../../lini1analisis/showECedit/'+id,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#mytable').DataTable({
                    "columnDefs": [ {
                                      "searchable": false,
                                      "orderable": false,
                                      "targets": [0,4],
                                    }],

                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'pengendalian' ,"fnRender": function( oObj ) { return oObj.aData[3].pengendalian }},
                        { mData: 'spip',"fnRender": function( oObj ) { return oObj.aData[3].spip }},
                        { mData: 'tahun',"fnRender": function( oObj ) { return oObj.aData[3].tahun }},
                        { mData: 'edit',"mRender": function(data, type, full) {
                          return '<a class="btn btn-info btn-sm" href="/lini1pengendalian/' + data + '/edit">' + 'Edit' + '</a>';
                        }}
                              ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

  </script>
@endpush
@else
@push('js1')
  <script type="text/javascript">
    function showModal(ele) 
      {
          var id= $(ele).attr('data-id');
          var risiko= $(ele).attr('data-risiko');
          document.getElementById("Risiko").innerHTML = risiko;
          console.log(id);

          $.ajax({
              url: '../../lini1analisis/showEC/'+id,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#mytable').DataTable({
                    "columnDefs": [ {
                                      "searchable": false,
                                      "orderable": false,
                                      "targets": [0,3],
                                    }],

                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'pengendalian' ,"fnRender": function( oObj ) { return oObj.aData[3].pengendalian }},
                        { mData: 'spip',"fnRender": function( oObj ) { return oObj.aData[3].spip }},
                        { mData: 'tahun',"fnRender": function( oObj ) { return oObj.aData[3].tahun }}
                             ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

  </script>
@endpush
@endif