@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Identifikasi Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Identifikasi Risiko Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>


  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini2identifikasi.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <label for="tahun" class="col-sm-2 control-label">Tahun</label>
        <div class="col-sm-2">
          <select class="form-control" name="tahun" id="tahun">
            @foreach((array) $tahun as $key)
            <option value="{{$key}}" {{old('tahun') == $key ? 'selected' : ''}}>{{$key}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="s_kd_jabdetail" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail" class="form-control" id="s_kd_jabdetail" autofocus>
            @foreach($s_kd_jabdetail as $key => $value)
              <option value="{{$key}}" {{old('s_kd_jabdetail') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_penetapan_konteks" class="col-sm-2 control-label">Konteks</label>
        <div class="col-sm-12">
          <select name="id_penetapan_konteks" class="form-control" id="id_penetapan_konteks">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Pemilik Risiko --</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_bagan_risiko" class="col-sm-2 control-label">Nama Risiko</label>
        <div class="col-sm-12">
          <select name="id_bagan_risiko" class="form-control" id="id_bagan_risiko" autofocus>
            @foreach($id_bagan_risiko as $key => $value)
              <option value="{{$key}}" {{old('id_bagan_risiko') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_dampak" class="col-sm-2 control-label">Dampak</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_dampak">{{old('nama_dampak')}}</textarea>
          </div>
      </div>



      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2identifikasi.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#tahun").select2();
    $("#s_kd_jabdetail").select2();
    $("#id_penetapan_konteks").select2();
    $("#id_bagan_risiko").select2();
});
</script>
@endpush


@push('js1')
  <script type="text/javascript">
  $(document).ready(function() {
        $('#s_kd_jabdetail').select2().on('change', function() {
            var konteksID = $(this).val();
            console.log(konteksID);
            if(konteksID) {
                $.ajax({
                    url: '/bewise20/lini2identifikasi/pilihKonteksUnit/'+konteksID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_penetapan_konteks"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_penetapan_konteks"]').append('<option value="'+ value.id_penetapan_konteks +'">'+ value.nama_jns_konteks + ': ' + value.nama_konteks + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="id_penetapan_konteks"]').empty();
            }
        });

        $('#id_penetapan_konteks').select2()

  });
  </script>
@endpush