<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    	<li><a href="{{route('adminpemda')}}"><i class="fa fa-home"></i> <span> Home</span></a></li>
        <li class="treeview">
        	<a href="#">
            <i class="fa fa-tasks"></i> <span>Administrasi</span>
            <span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            </span>
          	</a>
          	<ul class="treeview-menu">
            	<li><a href="{{route('manajemen-user.index')}}"><i class="fa fa-check-circle-o"></i> Manajemen User</a></li>
            	<li><a href="#"><i class="fa fa-check-circle-o"></i> Ganti Password</a></li>
            	<li><a href="#"><i class="fa fa-check-circle-o"></i> Log User</a></li>
          	</ul>
        </li>
        <li class="treeview">
        	<a href="#">
            <i class="fa fa-tasks"></i> <span>Parameter</span>
            <span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            </span>
          	</a>
          	<ul class="treeview-menu">
            	<li><a href="{{route('pemda.index')}}"><i class="fa fa-check-circle-o"></i> Data umum Pemerintah Daerah</a></li>
            	<li><a href="{{route('opd.index')}}"><i class="fa fa-check-circle-o"></i> Unit Organisasi (OPD)</a></li>
            	<li><a href="{{route('kategori.index')}}"><i class="fa fa-check-circle-o"></i> Kategori dan Selera Risiko</a></li>
            	<li><a href="{{route('baganrisiko.index')}}"><i class="fa fa-check-circle-o"></i> Bagan Risiko Standar</a></li>
          	</ul>
        </li>
        <li class="treeview">
        	<a href="#">
            <i class="fa fa-tasks"></i> <span>Perencanaan Kinerja</span>
            <span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            </span>
          	</a>
          	<ul class="treeview-menu">
            	<li><a href="{{route('visi.index')}}"><i class="fa fa-check-circle-o"></i> Visi</a></li>
            	<li><a href="{{route('misi.index')}}"><i class="fa fa-check-circle-o"></i> Misi</a></li>
            	<li><a href="{{route('tujuan.index')}}"><i class="fa fa-check-circle-o"></i> Tujuan</a></li>
            	<li><a href="{{route('sasaran.index')}}"><i class="fa fa-check-circle-o"></i> Sasaran</a></li>
            	<li><a href="{{route('program.index')}}"><i class="fa fa-check-circle-o"></i> Program</a></li>
            	<li><a href="{{route('mapping.index')}}"><i class="fa fa-check-circle-o"></i> Mapping</a></li>
          	</ul>
        </li>
</ul>
