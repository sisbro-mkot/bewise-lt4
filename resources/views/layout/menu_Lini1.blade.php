<li class="nav-item {{ Request::is('heatmapunit') ? 'active' : null }}">
  <a class="nav-link" href="{{route('heatmapunit')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>

<li class="nav-item with-sub 
{{ Request::is('lini1data') ? 'active' : null }}
{{ Request::is('lini1kamus') ? 'active' : null }}
{{ Request::is('lini1panduan') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-filing-outline"></i>
    <span>Parameter</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('lini1data.index')}}"><i class="fa fa-check-circle-o"></i> Data Umum Unit Kerja</a></li>
      <li><a href="{{route('lini1kamus')}}"><i class="fa fa-check-circle-o"></i> Kamus Risiko</a></li>
      <li><a href="{{route('lini1panduan')}}"><i class="fa fa-check-circle-o"></i> Panduan Penggunaan Aplikasi</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('lini1tetapkonteks') ? 'active' : null }} 
{{ Request::is('lini1identifikasi') ? 'active' : null }}
{{ Request::is('lini1analisis') ? 'active' : null }} 
{{ Request::is('lini1evaluasi') ? 'active' : null }}
{{ Request::is('lini1respons') ? 'active' : null }}
{{ Request::is('lini1realisasi') ? 'active' : null }}
{{ Request::is('lini1monitoring') ? 'active' : null }}
{{ Request::is('lini1laporan') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
    	<li><a href="{{route('lini1tetapkonteks.index')}}"><i class="fa fa-check-circle-o"></i> Penetapan Konteks</a></li>
      <li><a href="{{route('lini1identifikasi.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko</a></li>
      <li><a href="{{route('lini1analisis.index')}}"><i class="fa fa-check-circle-o"></i> Analisis Risiko</a></li>
      <li><a href="{{route('lini1evaluasi.index')}}"><i class="fa fa-check-circle-o"></i> Evaluasi Risiko</a></li>
      <li><a href="{{route('lini1respons.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (RTP)</a></li>
      <li><a href="{{route('lini1realisasi.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (Realisasi)</a></li>
      <li><a href="{{route('lini1monitoring.index')}}"><i class="fa fa-check-circle-o"></i> Pemantauan Risiko</a></li>
      <li><a href="{{route('lini1laporan.index')}}"><i class="fa fa-check-circle-o"></i> Pelaporan Risiko</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>