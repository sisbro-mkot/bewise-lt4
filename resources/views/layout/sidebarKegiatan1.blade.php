<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    	<li><a href="{{route('opdkegiatan1')}}"><i class="fa fa-home"></i> <span> Home</span></a></li>
        <li class="treeview">
        	<a href="#">
            <i class="fa fa-tasks"></i> <span>Pengelolaan Risiko</span>
            <span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            </span>
          	</a>
          	<ul class="treeview-menu">
            	<li><a href="{{route('identifikasikegiatan1.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Kegiatan</a></li>
            	<li><a href="{{route('analisiskegiatan1.index')}}"><i class="fa fa-check-circle-o"></i> Analisis Kegiatan</a></li>
            	<li><a href="{{route('rtpkegiatan1.index')}}"><i class="fa fa-check-circle-o"></i> RTP Kegiatan</a></li>
          	</ul>
        </li>
</ul>
