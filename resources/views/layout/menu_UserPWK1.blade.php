<li class="nav-item {{ Request::is('useropd1/home') ? 'active' : null }}">
  <a class="nav-link" href="{{route('useropd1')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>
<li class="nav-item with-sub {{ Request::is('useropd1/identifikasiopd1') ? 'active' : null }} {{ Request::is('useropd1/analisisopd1') ? 'active' : null }} {{ Request::is('useropd1/rtpopd1') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
    	<li><a href="{{route('identifikasiopd1.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko Unit Kerja</a></li>
    	<li><a href="{{route('analisisopd1.index')}}"><i class="fa fa-check-circle-o"></i> Analisis &amp; Evaluasi Risiko Unit Kerja</a></li>
    	<li><a href="{{route('rtpopd1.index')}}"><i class="fa fa-check-circle-o"></i> RTP Risiko Unit Kerja</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>