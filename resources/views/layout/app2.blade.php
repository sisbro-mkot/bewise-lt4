<!DOCTYPE html>
<html lang="en">
<!--#######################################################################################################
 * @application   <BPKP Wide Risk Management>
 * @author    Iwan Priyanto, S.Kom. (Ketua Tim)
 *            Yustinus Santo Nugroho, S.Kom. (Anggota Tim)
 *            Debrian Ruhut Saragih, S.Tr.Ak <debrian.saragih@bpkp.go.id> (Anggota Tim)
 *            Septian Adi Nugraha, S.Tr.Ak <septian.adi.nugraha@gmail.com> (Anggota Tim)
 *            Mas Muhamad Dzulfikar, S.Tr.Ak (Anggota Tim)
 *            Iwan Setiawan (Anggota Tim)
 *            Dwi Putri Lestari (Anggota Tim)
 * @department   BPKP RI <Financial and Development Supervisory Board> / Badan Pengawasan Keuangan dan Pembangunan
 * @version     1.0
 * @copyright    Biro SDM BPKP Copyright ©2019
 * @Framework  PHP Laravel Framework 5.4
############################################################################################################-->
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo.jpg')}}">
    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>BPKP Wide Risk Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{URL::asset('images/favicon.png')}}" type="image/x-icon">
    <!-- Datatables -->
    <link href="{{asset('app/lib/datatables/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/select2/css/select2.min.css')}}" rel="stylesheet">
    <!-- Sweetalert -->
    <!-- Sweetalert Css -->
    <!-- <link href="{{ asset('app/lib/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" /> -->
    <link href="{{URL::asset('sweetalert2/dist/sweetalert.min.css')}}" rel="stylesheet" type="text/css" >
    <!-- vendor css -->
    <link href="{{asset('app/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/rickshaw/css/rickshaw.min.css')}}" rel="stylesheet">
    <link href="{{asset('app/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('app/css/slim.css')}}">
    <script src="{{asset('app/lib/jquery/js/jquery.js')}}"></script>
    <!-- SweetAlert Plugin Js -->
    <!-- <script src="{{ asset('app/lib/sweetalert2/sweetalert2.all.min.js') }}"></script> -->
    <script src="{{URL::asset('sweetalert2/dist/sweetalert.min.js')}}" type="text/javascript" ></script>
    @include('sweet::alert')
    <script type="text/javascript">
        $.fn.modal.Constructor.prototype.enforceFocus = function() {
          modal_this = this
          $(document).on('focusin.modal', function (e) {
            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length 
            && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') 
            && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
              modal_this.$element.focus()
            }
          })
        };
        function remove(id) {
          swal({
            title: 'Konfirmasi',
            text: 'Anda yakin akan menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Batal',
            confirmButtonText: 'Hapus',
            reverseButtons: false,
            // closeOnConfirm: false,
            closeOnCancel: true
          }).then(result => {
            if (result.value) {
              // handle Confirm button click
              document.getElementById('listform' + id).submit();
            } else {
              // handle dismissals
              // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
            }
          });
        };
        function konfirmasi(id,e) {
          e.preventDefault();
          var originLink = document.getElementById('btnkonfirmasi' + id).href;
          swal({
            title: 'Konfirmasi',
            text: 'Anda yakin akan melakukan proses ini? (tidak bisa diulangi)',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Batal',
            confirmButtonText: 'Lakukan',
            reverseButtons: false,
            // closeOnConfirm: false,
            closeOnCancel: true
          }).then(result => {
            if (result.value) {
              // handle Confirm button click
              // window.location.href = originLink;
              window.open(originLink,'_blank');
            } else {
              // handle dismissals
              // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
            }
          });
        };



    </script>
    <style type="text/css">
      #loading-image {
        display:none;
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url({{url('images/preloader.gif')}}) center no-repeat #fff;
        /*opacity:.9;*/
      }
      #loading-image::after {
        content: 'Harap menunggu, sistem sedang mengambil data...';
        position: fixed;
        top: 0;
        left: 0;
        background-color: rgba(0,0,0,.7);
        color: white;
        height: 100%
        width: 100%
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 5px;
      }
      .select2-container--default{width: 100% !important}
      .slim-navbar .nav-item.active .nav-link {background-image: linear-gradient(to right, #F60 0%, #C00 100%) !important;}
    </style>
  </head>
  <body class="dashboard-3">
    <div id="loading-image"></div> 
    <script type="text/javascript">
      $(document).ajaxStart(function() {
        // $("#loading-image").show();
        $("#loading-image").fadeIn("fast");
        // $(this).css("display", "inline");
      });

      $(document).ajaxStop(function() {
        // $("#loading-image").hide();
        $("#loading-image").fadeOut("fast");
        // $(this).css("display", "none");
      });
    </script>
    @if (session('status'))
    <?php echo "<script>swal('Sukses!', '".session('status')."', 'success');</script>"; ?>
    @endif  
    @if (session('status2'))
    <?php echo "<script>swal('Gagal!', '".session('status2')."', 'error');</script>"; ?>
    @endif
    <div class="slim-header">
      <div class="container-fluid">
        <div class="slim-header-left">
          <h2 class="slim-logo"><a href="{{url('#')}}" style="color: #FF6600 !important;"><img class="logo" height="45px" src="{{asset('images/logo.png')}}"> BPKP Wide Risk Management</a></h2>
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
          <div class="dropdown dropdown-c">
            <a href="#" class="logged-user" data-toggle="dropdown">
              <!-- <img src="{{asset('images/photo.jpg')}}" alt=""> -->
              <!-- <img src="http://10.10.1.14:9081/servlet/api/foto/{{Auth::user()->user_nip}}" alt=""> -->
              <object style="border-radius: 100% !important;" data="http://10.10.1.14:9082/servlet/api/foto/{{Auth::user()->user_nip}}" type="image/png" width="45" height="45" class="img-circle" alt=""><img src="http://118.97.51.140:10001/map/public/foto/{{Auth::user()->user_nip}}.gif" width="45" height="45" class="img-circle" alt=""/></object>
              <!-- <object data="http://10.10.1.14:9081/servlet/api/foto/{{Auth::user()->user_nip}}" type="image/png" width="45" height="45"><img src="./public/foto/{{Auth::user()->user_nip}}.gif" width="45" height="45"/></object> -->
              <span>{{Auth::user()->name}} ({{Auth::user()->role->nama_role}} - {{Auth::user()->unit->unitkerja_skt}})</span>
              
              <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <nav class="nav">
<!--                 <a href="page-profile.html" class="nav-link"><i class="icon ion-person"></i> View Profile</a>
                <a href="page-edit-profile.html" class="nav-link"><i class="icon ion-compose"></i> Edit Profile</a>
                <a href="page-activity.html" class="nav-link"><i class="icon ion-ios-bolt"></i> Activity Log</a>
                <a href="page-settings.html" class="nav-link"><i class="icon ion-ios-gear"></i> Account Settings</a> -->
                <!-- <a href="page-signin.html" class="nav-link"><i class="icon ion-forward"></i> Sign Out</a> -->
                <a href="logout" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutForm').submit();"><i class="icon ion-forward"></i> Keluar</a>
                <form action="{{ URL::to('logout') }}" method="post" id="logoutForm" style="display:none">
                    {{ csrf_field() }}
                </form>
              </nav>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </div><!-- header-right -->
      </div><!-- container-fluid -->
    </div><!-- slim-header -->

    @include('layout.mainmenu')

    <div class="slim-mainpanel">
      <div class="container-fluid">
        @yield('isi')
      </div><!-- container-fluid -->
    </div><!-- slim-mainpanel -->

    <div class="slim-footer">
      <div class="container-fluid">
        <p>Copyright 2019 &copy; BPKP. All Rights Reserved.</p>
      </div><!-- container-fluid -->
    </div><!-- slim-footer -->

    <!-- <script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script> -->
    <script src="{{asset('app/lib/jquery/js/jquery.js')}}"></script>

    <script src="{{asset('app/lib/popper.js/js/popper.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('app/lib/jquery.cookie/js/jquery.cookie.js')}}"></script>
    <script src="{{asset('app/lib/d3/js/d3.js')}}"></script>
    <script src="{{asset('app/lib/rickshaw/js/rickshaw.min.js')}}"></script>
    <script src="{{asset('app/lib/Flot/js/jquery.flot.js')}}"></script>
    <script src="{{asset('app/lib/Flot/js/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('app/lib/peity/js/jquery.peity.js')}}"></script>

    <script src="{{asset('app/js/slim.js')}}"></script>
    <script src="{{asset('app/js/ResizeSensor.js')}}"></script>

<!--     <script src="{{URL::asset('datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('datatable/DataTables-1.10.16/js/dataTables.bootstrap.min.js')}}"></script> -->

    <script src="{{asset('app/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('app/lib/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('app/lib/select2/js/select2.min.js')}}"></script>
    @stack('js')
    @stack('js1')
  </body>
</html>
