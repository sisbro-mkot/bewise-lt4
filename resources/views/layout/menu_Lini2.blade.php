<li class="nav-item {{ Request::is('peta') ? 'active' : null }}">
  <a class="nav-link" href="{{route('peta')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>

<li class="nav-item with-sub 
{{ Request::is('lini2data') ? 'active' : null }}
{{ Request::is('lini2paramkonteks') ? 'active' : null }}
{{ Request::is('lini2paramrisiko') ? 'active' : null }}
{{ Request::is('lini2paramrole') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-filing-outline"></i>
    <span>Parameter</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('lini2data.index')}}"><i class="fa fa-check-circle-o"></i> Data Umum Unit Kerja</a></li>
      
      @if (Auth::user()->role_id == '1')
      <li><a href="{{route('lini2paramkonteks.index')}}"><i class="fa fa-check-circle-o"></i> Konteks</a></li>
      <li><a href="{{route('lini2paramrisiko.index')}}"><i class="fa fa-check-circle-o"></i> Bagan Risiko</a></li>
      <li><a href="{{route('lini2paramrole.index')}}"><i class="fa fa-check-circle-o"></i> Peran Pengguna</a></li>
      <li><a href="{{route('lini2paramuser.index')}}"><i class="fa fa-check-circle-o"></i> Manajemen Pengguna</a></li>
      @endif
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('lini2tetapkonteks') ? 'active' : null }} 
{{ Request::is('lini2identifikasi') ? 'active' : null }}
{{ Request::is('lini2analisis') ? 'active' : null }} 
{{ Request::is('lini2evaluasi') ? 'active' : null }}
{{ Request::is('lini2respons') ? 'active' : null }}
{{ Request::is('lini2realisasi') ? 'active' : null }}
{{ Request::is('lini2monitoring') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
    	<li><a href="{{route('lini2tetapkonteks.index')}}"><i class="fa fa-check-circle-o"></i> Penetapan Konteks</a></li>
      <li><a href="{{route('lini2identifikasi.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko</a></li>
      <li><a href="{{route('lini2analisis.index')}}"><i class="fa fa-check-circle-o"></i> Analisis Risiko</a></li>
      <li><a href="{{route('lini2evaluasi.index')}}"><i class="fa fa-check-circle-o"></i> Evaluasi Risiko</a></li>
      <li><a href="{{route('lini2respons.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (RTP)</a></li>
      <li><a href="{{route('lini2realisasi.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (Realisasi)</a></li>
      <li><a href="{{route('lini2monitoring.index')}}"><i class="fa fa-check-circle-o"></i> Monitoring Risiko</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>