<li class="nav-item {{ Request::is('useropdkegiatan2/home') ? 'active' : null }}">
  <a class="nav-link" href="{{route('opdkegiatan2')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>
<li class="nav-item with-sub {{ Request::is('useropdkegiatan2/pemantauankegiatan2') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('pemantauankegiatan2.index')}}"><i class="fa fa-check-circle-o"></i> Pemantauan Risiko Kegiatan</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>