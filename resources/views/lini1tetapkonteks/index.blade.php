@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Penetapan Konteks</li>
  </ol>
  <h6 class="slim-pagetitle">Penetapan Konteks {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <div class="card-header">
    <a href="{{url('createtkunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
   <!--  <a href="{{url('lini1tetapkonteks')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a> -->
  </div>
  @endif
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Tahun</th>
          <th style="text-align: center;">Pemilik Risiko</th>
          <th style="text-align: center;">Jenis Konteks</th>
          <th style="text-align: center;">Konteks</th>
          <th style="text-align: center;">Keterangan</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($konteks as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->tahun}}</td>
          <td>{{$item->s_nmjabdetail_pemilik}}</td>
          <td>{{$item->nama_jns_konteks}}</td>
          <td>{{$item->nama_konteks}}</td>
          <td>{{$item->ket_konteks}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
