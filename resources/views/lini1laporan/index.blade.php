@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pelaporan Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Pelaporan Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h5 style="color: black;">Identifikasi Risiko</h5>
    <a href="cetakidunit" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak PDF</a>
<!--     <a href="excelidunit" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel</a> -->
  </div>
  <div class="card-header">
    <h5 style="color: black;">Analisis Risiko</h5>
    <a href="cetakanunit" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak PDF</a>
    <!-- <a href="excelidunit" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel</a> -->
  </div>
  <div class="card-header">
    <h5 style="color: black;">Daftar Risiko Prioritas Unit Kerja</h5>
    <a href="cetakevunit" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak PDF</a>
    <!-- <a href="excelidunit" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel</a> -->
  </div>
  <div class="card-header">
    <h5 style="color: black;">Analisis Akar Masalah <span style="font-style: italic;">(Root Cause Analysis)</span></h5>
    <a href="cetakrcaunit" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak PDF</a>
    <!-- <a href="excelidunit" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel</a> -->
  </div>
  <div class="card-header">
    <h5 style="color: black;">Rencana Tindak Pengendalian</h5>
    <a href="cetakresunit" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak PDF</a>
    <!-- <a href="excelidunit" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel</a> -->
  </div>
  <div class="card-header">
    <h5 style="color: black;">Daftar Pemantauan Kegiatan Pengendalian</h5>
    <a href="cetakrealunit" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak PDF</a>
    <!-- <a href="excelidunit" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel</a> -->
  </div>
  <div class="card-header">
    <h5 style="color: black;">Pemantauan Terhadap Keterjadian Risiko</h5>
    <a href="cetakmonunit" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak PDF</a>
    <!-- <a href="excelidunit" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel</a> -->
  </div>
</div>
@endsection


