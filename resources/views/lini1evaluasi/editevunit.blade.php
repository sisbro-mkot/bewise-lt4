@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Evaluasi Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Evaluasi Risiko {{$nama_instansiunitorg->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1evaluasi.update', $evaluasi->id_penyebab)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$evaluasi->id_penyebab}}" id="id_penyebab" name="id_penyebab" hidden>
        </div>
      </div>

      <div class="form-group">
        <label for="id_identifikasi" class="col-sm-2 control-label">Kode, Nama, dan Dampak Risiko</label>
        <div class="col-sm-12">
          <select name="id_identifikasi" class="form-control" id="id_identifikasi" autofocus>
            @foreach($id_identifikasi as $key)
              <option value="{{$key->id_identifikasi}}" {{$evaluasi->id_identifikasi == $key->id_identifikasi ? 'selected' : ''}}>{{$key->kode_identifikasi_risiko}} - {{$key->nama_bagan_risiko}} - {{$key->nama_dampak}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_1" class="col-sm-12 control-label">Nama Penyebab (Why 1)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_1" id="nama_penyebab_1">{{$evaluasi->nama_penyebab_1}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab1ya" onclick="Yes1()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab1tidak" onclick="No1()"> Bukan Akar Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_2" class="col-sm-12 control-label" id="label_nama_penyebab_2">Nama Penyebab (Why 2)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_2" id="nama_penyebab_2">{{$evaluasi->nama_penyebab_2}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab2ya" onclick="Yes2()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab2tidak" onclick="No2()"> Bukan Akar Penyebab</button>
          <button class="btn btn-success" id="sebab2hapus" onclick="Hapus2()"> Hapus Nama Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_3" class="col-sm-12 control-label" id="label_nama_penyebab_3">Nama Penyebab (Why 3)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_3" id="nama_penyebab_3">{{$evaluasi->nama_penyebab_3}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab3ya" onclick="Yes3()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab3tidak" onclick="No3()"> Bukan Akar Penyebab</button>
          <button class="btn btn-success" id="sebab3hapus" onclick="Hapus3()"> Hapus Nama Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_4" class="col-sm-12 control-label" id="label_nama_penyebab_4">Nama Penyebab (Why 4)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_4" id="nama_penyebab_4">{{$evaluasi->nama_penyebab_4}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab4ya" onclick="Yes4()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab4tidak" onclick="No4()"> Bukan Akar Penyebab</button>
          <button class="btn btn-success" id="sebab4hapus" onclick="Hapus4()"> Hapus Nama Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_5" class="col-sm-12 control-label" id="label_nama_penyebab_5">Nama Penyebab (Why 5)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_5" id="nama_penyebab_5">{{$evaluasi->nama_penyebab_5}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab5ya" onclick="Yes5()"> Akar Penyebab</button>
          <button class="btn btn-success" id="sebab5hapus" onclick="Hapus5()"> Hapus Nama Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_akar_penyebab" class="col-sm-12 control-label" id="label_nama_akar_penyebab">Nama Akar Penyebab</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_akar_penyebab" id="nama_akar_penyebab">{{$evaluasi->nama_akar_penyebab}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="id_jns_sebab" class="col-sm-2 control-label">Jenis Penyebab</label>
        <div class="col-sm-12">
          <select name="id_jns_sebab" class="form-control" id="id_jns_sebab" autofocus>
            @foreach($id_jns_sebab as $key => $value)
              <option value="{{$key}}" {{$evaluasi->id_jns_sebab == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1evaluasi.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $('button[type!=submit]').click(function(){
        return false;
    });

  $(document).ready(function() {

    var input1 = document.getElementById("nama_penyebab_1").value;
    var input2 = document.getElementById("nama_penyebab_2").value;
    var input3 = document.getElementById("nama_penyebab_3").value;
    var input4 = document.getElementById("nama_penyebab_4").value;
    var input5 = document.getElementById("nama_penyebab_5").value;
    $("#id_identifikasi").select2();
    $("#id_jns_sebab").select2();
    if (input1.length === 0){
      $("#label_nama_penyebab_2").hide();
      $("#nama_penyebab_2").hide();
      $("#label_nama_penyebab_3").hide();
      $("#nama_penyebab_3").hide();
      $("#label_nama_penyebab_4").hide();
      $("#nama_penyebab_4").hide();
      $("#label_nama_penyebab_5").hide();
      $("#nama_penyebab_5").hide();
      $("#label_nama_akar_penyebab").hide();
      $("#nama_akar_penyebab").hide();
      $("#sebab2ya").hide();
      $("#sebab2tidak").hide();
      $("#sebab2hapus").hide();
      $("#sebab3ya").hide();
      $("#sebab3tidak").hide();
      $("#sebab3hapus").hide();
      $("#sebab4ya").hide();
      $("#sebab4tidak").hide();
      $("#sebab4hapus").hide();
      $("#sebab5ya").hide();
      $("#sebab5hapus").hide();
    } else if (input2.length === 0){
      $("#label_nama_penyebab_2").hide();
      $("#nama_penyebab_2").hide();
      $("#label_nama_penyebab_3").hide();
      $("#nama_penyebab_3").hide();
      $("#label_nama_penyebab_4").hide();
      $("#nama_penyebab_4").hide();
      $("#label_nama_penyebab_5").hide();
      $("#nama_penyebab_5").hide();
      $("#sebab2ya").hide();
      $("#sebab2tidak").hide();
      $("#sebab2hapus").hide();
      $("#sebab3ya").hide();
      $("#sebab3tidak").hide();
      $("#sebab3hapus").hide();
      $("#sebab4ya").hide();
      $("#sebab4tidak").hide();
      $("#sebab4hapus").hide();
      $("#sebab5ya").hide();
      $("#sebab5hapus").hide();
    } else if (input3.length === 0){
      $("#label_nama_penyebab_3").hide();
      $("#nama_penyebab_3").hide();
      $("#label_nama_penyebab_4").hide();
      $("#nama_penyebab_4").hide();
      $("#label_nama_penyebab_5").hide();
      $("#nama_penyebab_5").hide();
      $("#sebab1ya").hide();
      $("#sebab1tidak").hide();
      $("#sebab3ya").hide();
      $("#sebab3tidak").hide();
      $("#sebab3hapus").hide();
      $("#sebab4ya").hide();
      $("#sebab4tidak").hide();
      $("#sebab4hapus").hide();
      $("#sebab5ya").hide();
      $("#sebab5hapus").hide();
    } else if(input4.length === 0) {
      $("#label_nama_penyebab_4").hide();
      $("#nama_penyebab_4").hide();
      $("#label_nama_penyebab_5").hide();
      $("#nama_penyebab_5").hide();
      $("#sebab1ya").hide();
      $("#sebab1tidak").hide();
      $("#sebab2ya").hide();
      $("#sebab2tidak").hide();
      $("#sebab2hapus").hide();
      $("#sebab4ya").hide();
      $("#sebab4tidak").hide();
      $("#sebab4hapus").hide();
      $("#sebab5ya").hide();
      $("#sebab5hapus").hide();
    } else if(input5.length === 0) {
      $("#label_nama_penyebab_5").hide();
      $("#nama_penyebab_5").hide();
      $("#sebab1ya").hide();
      $("#sebab1tidak").hide();
      $("#sebab2ya").hide();
      $("#sebab2tidak").hide();
      $("#sebab2hapus").hide();
      $("#sebab3ya").hide();
      $("#sebab3tidak").hide();
      $("#sebab3hapus").hide();
      $("#sebab5ya").hide();
      $("#sebab5hapus").hide();
    } else {
      $("#sebab1ya").hide();
      $("#sebab1tidak").hide();
      $("#sebab2ya").hide();
      $("#sebab2tidak").hide();
      $("#sebab2hapus").hide();
      $("#sebab3ya").hide();
      $("#sebab3tidak").hide();
      $("#sebab3hapus").hide();
      $("#sebab4ya").hide();
      $("#sebab4tidak").hide();
      $("#sebab4hapus").hide();
    }
    
   
});

function Yes1() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_1").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 1)");
            return;
        }
    }

function No1() {
        $("#sebab1ya").hide();
        $("#sebab1tidak").hide();
        $("#label_nama_penyebab_2").show();
        $("#nama_penyebab_2").show();
        $("#sebab2ya").show();
        $("#sebab2tidak").show();
        $("#sebab2hapus").show();
    }

function Yes2() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_2").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 2)");
            return;
        }
    }

function No2() {
        $("#sebab2ya").hide();
        $("#sebab2tidak").hide();
        $("#sebab2hapus").hide();
        $("#label_nama_penyebab_3").show();
        $("#nama_penyebab_3").show();
        $("#sebab3ya").show();
        $("#sebab3tidak").show();
        $("#sebab3hapus").show();
    }

function Hapus2() {
        $("#label_nama_penyebab_2").hide();
        $("#nama_penyebab_2").hide();
        $("#sebab2ya").hide();
        $("#sebab2tidak").hide();
        $("#sebab2hapus").hide();
        $("#sebab1ya").show();
        $("#sebab1tidak").show();
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_1").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        $('#nama_penyebab_2').val('');
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 1)");
            return;
        }
    }

function Yes3() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_3").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 3)");
            return;
        }
    }

function No3() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        $("#sebab3ya").hide();
        $("#sebab3tidak").hide();
        $("#sebab3hapus").hide();
        $("#label_nama_penyebab_4").show();
        $("#nama_penyebab_4").show();
        $("#sebab4ya").show();
        $("#sebab4tidak").show();
        $("#sebab4hapus").show();
    }

function Hapus3() {
        $("#label_nama_penyebab_3").hide();
        $("#nama_penyebab_3").hide();
        $("#sebab3ya").hide();
        $("#sebab3tidak").hide();
        $("#sebab3hapus").hide();
        $("#sebab2ya").show();
        $("#sebab2tidak").show();
        $("#sebab2hapus").show();
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_2").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        $('#nama_penyebab_3').val('');
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 2)");
            return;
        }
    }

function Yes4() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_4").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 4)");
            return;
        }
    }

function No4() {
        $("#sebab4ya").hide();
        $("#sebab4tidak").hide();
        $("#sebab4hapus").hide();
        $("#label_nama_penyebab_5").show();
        $("#nama_penyebab_5").show();
        $("#sebab5ya").show();
        $("#sebab5hapus").show();

    }

function Hapus4() {
        $("#label_nama_penyebab_4").hide();
        $("#nama_penyebab_4").hide();
        $("#sebab4ya").hide();
        $("#sebab4tidak").hide();
        $("#sebab4hapus").hide();
        $("#sebab3ya").show();
        $("#sebab3tidak").show();
        $("#sebab3hapus").show();
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_3").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        $('#nama_penyebab_4').val('');
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 3)");
            return;
        }
    }

function Yes5() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_5").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 5)");
            return;
        }
    }

function Hapus5() {
        $("#label_nama_penyebab_5").hide();
        $("#nama_penyebab_5").hide();
        $("#sebab5ya").hide();
        $("#sebab5hapus").hide();
        $("#sebab4ya").show();
        $("#sebab4tidak").show();
        $("#sebab4hapus").show();
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_4").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        $('#nama_penyebab_5').val('');
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab (why 4)");
            return;
        }
    }

</script>
@endpush


@push('js1')
  <script type="text/javascript">
    $(document).on('click', '#button1', function(){
       $("#nama_akar_penyebab").val($("#nama_penyebab_1").val());
      });
  </script>
@endpush

