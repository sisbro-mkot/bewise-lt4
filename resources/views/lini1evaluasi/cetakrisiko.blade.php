<!DOCTYPE html>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Evaluasi Risiko</title>
    <style type="text/css">
    table {
    border-collapse: collapse;
    }
    table, th, td {
    border: 1px solid;
    }
    th, td {
    padding: 3;
    }
    .kop {width:100%; text-align: left;margin-bottom: 5px; border:none;}
    .kop tr td{border:none;}

    .kopsurat{text-align: center;}
    .kopsurat1{line-height:15px;text-align: center;font-family:sans-serif !important; font-size: 18px; margin-bottom: 6px;font-weight: bold}
    .kopsurat2{line-height:11px;text-align: center;font-family:sans-serif; font-size: 16px }
  </style>
</head><body>

<div align="center">
  <table class="kop">
    <tr>
        <td class="kopsurat" style="width:90%;">
          <p class="kopsurat1">DAFTAR RISIKO PRIORITAS UNIT KERJA</p>
        </td>
    </tr>
    <tr>
        <td>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Nama Unit Pemilik Risiko: {{$unit->s_nama_instansiunitorg}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Tahun: {{$tahun}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Selera Risiko Pemilik Risiko: {{$selera->skor_selera}}</p>
        </td>
    </tr>
  </table>

</div>

   <div>
      <table width="100%" style="font-family: sans-serif; font-size: 12px;">
        <thead>
        <tr align="center">
          <th width="10%" rowspan="2">Kode Risiko</th>
          <th rowspan="2">Pernyataan Risiko</th>
          <th colspan="3">Skor/Nilai Risiko Residu setelah Pengendalian yang Ada</th>
        </tr>
        <tr align="center">
          <th width="5%" >Skor Kemungkinan Terjadi</th>
          <th width="5%" >Skor Dampak</th>
          <th width="5%" >Level Risiko</th>
        </tr>
        <tr style="background-color: #BDBDBD; font-style: italic; font-size: 8px" align="center">
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
        </tr>
        </thead>
        <?php $no=1; ?>
        @foreach($evaluasi as $item)
        <tr class="item{{$item->id}}">
          <td align="center">{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td align="center">{{$item->skor_kemungkinan_residual}}</td>
          <td align="center">{{$item->skor_dampak_residual}}</td>
          <td align="center">{{$item->skor_risiko_residual}}</td>
        </tr>
        @endforeach
      </table>
    </div>

</body></html>