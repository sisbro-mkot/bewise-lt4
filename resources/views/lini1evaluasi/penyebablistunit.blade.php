@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Evaluasi Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Evaluasi Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->

 
<div class="card card-table">
  <div class="card-header">
    <a href="{{url('createevunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    <a href="{{url('lini1evaluasi')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data Evaluasi Risiko</a>
  </div>
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="5%">Kode Penyebab</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Nama Penyebab</th>
          <th style="text-align: center;">Kegiatan Pengendalian</th>
        </tr>
      </thead>
      <tbody>
      @foreach($evaluasi as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_penyebab}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_akar_penyebab}}</td>
          <td>{{$item->kegiatan_pengendalian}}</td>
        </tr>
      @endforeach
        </tbody>
    </table>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "order": [[ 3, "desc" ]],

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
