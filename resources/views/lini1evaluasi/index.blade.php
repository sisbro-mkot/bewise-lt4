@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Evaluasi Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Evaluasi Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <div class="card-header">
    @if($risiko == 0)
    <p style="color: black;">Belum ada data identifikasi risiko.</p> 
    <p style="color: black;">Silahkan tambah data di menu Pengelolaan Risiko - Identifikasi Risiko.</p>
    @else
    <a href="{{url('createevunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    @endif
    <!-- <a href="{{url('lini2evaluasi')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a> -->
  </div>
  @endif
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}
    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="10%">Kode Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Why 1</th>
          <th style="text-align: center;">Why 2</th>
          <th style="text-align: center;">Why 3</th>
          <th style="text-align: center;">Why 4</th>
          <th style="text-align: center;">Why 5</th>
          <th style="text-align: center;">Akar Penyebab</th>
          <th style="text-align: center;">Jenis Penyebab</th>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <th></th>
          @endif
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($penyebab as $item)
        <tr class="item{{$item->id}}">
<!--           <td>{{$no++}}</td> -->
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_penyebab_1}}</td>
          <td>{{$item->nama_penyebab_2}}</td>
          <td>{{$item->nama_penyebab_3}}</td>
          <td>{{$item->nama_penyebab_4}}</td>
          <td>{{$item->nama_penyebab_5}}</td>
          <td>{{$item->nama_akar_penyebab}}</td>
          <td>{{$item->nama_jns_sebab}}</td>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <td>
            <a href="{{route('lini1evaluasi.edit', $item->id)}}" class="btn btn-success btn-xs"><i class="icon ion-edit"></i> Edit</a>
          </td>
          @endif
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "columnDefs": [ {
        "targets": 9,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@else
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@endif