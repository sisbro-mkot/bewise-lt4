@extends('layout.app')
@section('isi')
<div class="slim-pageheader">
<!--   <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Beranda</li>
  </ol> -->
  <!-- <h6 class="slim-pagetitle">Selamat datang, {{Auth::user()->name}}</h6> -->
  @include('home._greeting')
</div><!-- slim-pageheader -->


@include('dashboard.sidebarunit')
    


<div class="col-lg-9">
  <div class="box1 row">
    <div class="box2 col-sm-6">
      <a href="{{url('penyebablistunit/1')}}">
      <div class="box3">
        <h1 class="ex1">Man</h1>
      </div><!-- box3 -->
      </a>
    </div><!-- box2 col-sm-6-->
    <div class="box2 col-sm-1">
    </div>
    <div class="box2 col-sm-2">
      <div class="box4">
        <text class="ex1">{{$mnid}}</text>
        <br/>
        <text class="ex2">Teridentifikasi</text>
      </div><!-- box4 -->
    </div><!-- box2 col-sm-2-->
    <div class="box2 col-sm-2">
      <div class="box5">
        <text class="ex1">{{$mnmt}}</text>
        <br/>
        <text class="ex3">Termitigasi</text>
      </div><!-- box5 -->
    </div><!-- box2 col-sm-2-->
  </div><!-- row -->

  
  <br/>
  <div class="box1 row">
    <div class="box2 col-sm-6">
      <a href="{{url('penyebablistunit/2')}}">
      <div class="box3">
        <h1 class="ex1">Money</h1>
      </div><!-- box3 -->
      </a>  
    </div><!-- box2 col-sm-6-->
    <div class="box2 col-sm-1">
    </div>
    <div class="box2 col-sm-2">
      <div class="box4">
        <text class="ex1">{{$myid}}</text>
        <br/>
        <text class="ex2">Teridentifikasi</text>
      </div><!-- box4 -->
    </div><!-- box2 col-sm-2-->
    <div class="box2 col-sm-2">
      <div class="box5">
        <text class="ex1">{{$mymt}}</text>
        <br/>
        <text class="ex3">Termitigasi</text>
      </div><!-- box5 -->
    </div><!-- box2 col-sm-2-->
  </div><!-- row -->

  <br/>
  <div class="box1 row">
    <div class="box2 col-sm-6">
      <a href="{{url('penyebablistunit/3')}}">
      <div class="box3">
        <h1 class="ex1">Method</h1>
      </div><!-- box3 -->
      </a>
    </div><!-- box2 col-sm-6-->
    <div class="box2 col-sm-1">
    </div>
    <div class="box2 col-sm-2">
      <div class="box4">
        <text class="ex1">{{$mdid}}</text>
        <br/>
        <text class="ex2">Teridentifikasi</text>
      </div><!-- box4 -->
    </div><!-- box2 col-sm-2-->
    <div class="box2 col-sm-2">
      <div class="box5">
        <text class="ex1">{{$mdmt}}</text>
        <br/>
        <text class="ex3">Termitigasi</text>
      </div><!-- box5 -->
    </div><!-- box2 col-sm-2-->
  </div><!-- row -->

  <br/>
  <div class="box1 row">
    <div class="box2 col-sm-6">
      <a href="{{url('penyebablistunit/4')}}">
      <div class="box3">
        <h1 class="ex1">Material</h1>
      </div><!-- box3 -->
      </a>
    </div><!-- box2 col-sm-6-->
    <div class="box2 col-sm-1">
    </div>
    <div class="box2 col-sm-2">
      <div class="box4">
        <text class="ex1">{{$mrid}}</text>
        <br/>
        <text class="ex2">Teridentifikasi</text>
      </div><!-- box4 -->
    </div><!-- box2 col-sm-2-->
    <div class="box2 col-sm-2">
      <div class="box5">
        <text class="ex1">{{$mrmt}}</text>
        <br/>
        <text class="ex3">Termitigasi</text>
      </div><!-- box5 -->
    </div><!-- box2 col-sm-2-->
  </div><!-- row -->

  <br/>
  <div class="box1 row">
    <div class="box2 col-sm-6">
      <a href="{{url('penyebablistunit/5')}}">
      <div class="box3">
        <h1 class="ex1">Machine</h1>
      </div><!-- box3 -->
      </a>
    </div><!-- box2 col-sm-6-->
    <div class="box2 col-sm-1">
    </div>
    <div class="box2 col-sm-2">
      <div class="box4">
        <text class="ex1">{{$mcid}}</text>
        <br/>
        <text class="ex2">Teridentifikasi</text>
      </div><!-- box4 -->
    </div><!-- box2 col-sm-2-->
    <div class="box2 col-sm-2">
      <div class="box5">
        <text class="ex1">{{$mcmt}}</text>
        <br/>
        <text class="ex3">Termitigasi</text>
      </div><!-- box5 -->
    </div><!-- box2 col-sm-2-->
  </div><!-- row -->

  <br/>
  <div class="box1 row">
    <div class="box2 col-sm-6">
      <a href="{{url('penyebablistunit/6')}}">
      <div class="box3">
        <h1 class="ex1">External</h1>
      </div><!-- box3 -->
      </a>
    </div><!-- box2 col-sm-6-->
    <div class="box2 col-sm-1">
    </div>
    <div class="box2 col-sm-2">
      <div class="box4">
        <text class="ex1">{{$exid}}</text>
        <br/>
        <text class="ex2">Teridentifikasi</text>
      </div><!-- box4 -->
    </div><!-- box2 col-sm-2-->
    <div class="box2 col-sm-2">
      <div class="box5">
        <text class="ex1">{{$exmt}}</text>
        <br/>
        <text class="ex3">Termitigasi</text>
      </div><!-- box5 -->
    </div><!-- box2 col-sm-2-->
  </div><!-- row -->

</div><!-- col-lg-9 -->



</div><!-- row -->
</div><!-- container -->

@endsection