@extends('layout.app')
@section('isi')
<div class="slim-pageheader">
<!--   <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Beranda</li>
  </ol> -->
  <!-- <h6 class="slim-pagetitle">Selamat datang, {{Auth::user()->name}}</h6> -->
  @include('home._greeting')
</div><!-- slim-pageheader -->


@include('dashboard.sidebar')
    


<div class="col-lg-9">
  <div class="card card-status">
    <div class="col-sm-6 col-lg-3">
    </div>
    <div class="col-sm-6 col-lg-9">
      <svg viewBox="0 0 10000 10000">
        <metadata id="CorelCorpID_0Corel-Layer"/>
        <polygon fill="#CCCCFF" points="5000,0 10000,5000 5000,10000 0,5000 "/>
        <a href="{{url('rtplist')}}">
          <rect fill="darkblue" stroke="black" stroke-width="20" x="250" y="250" width="4500" height="4500" rx="450" ry="450"/>
          <text y="5%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="bold" font-size="650px" font-family="Arial">
            <tspan x="25%" dy=".6em">Terjadwal</tspan>
            <tspan x="24%" dy="1.2em">Tahun Ini</tspan>
          </text>
          <text x="25%" y="30%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="800px" font-family="Arial Rounded MT Bold">{{$rtpjadwal}}</text>
        </a>
        <a href="{{url('rtplistcurrent')}}">
          <rect fill="darkblue" stroke="black" stroke-width="20" x="5250" y="250" width="4500" height="4500" rx="450" ry="450"/>
          <text y="5%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="bold" font-size="650px" font-family="Arial">
            <tspan x="75%" dy=".6em">Terjadwal</tspan>
            <tspan x="74%" dy="1.2em">Triwulan Ini</tspan>
          </text>
          <text x="75%" y="30%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="800px" font-family="Arial Rounded MT Bold">{{$tricount}}</text>
        </a>
        <a href="{{url('monitor')}}">
          <rect fill="darkblue" stroke="black" stroke-width="20" x="250" y="5250" width="4500" height="4500" rx="450" ry="450"/>
          <text x="25%" y="65%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="bold" font-size="650px" font-family="Arial">Terealisasi</text>
          <text x="25%" y="80%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="800px" font-family="Arial Rounded MT Bold">{{$rtprealisasi}}</text>
        </a>
        <a href="{{url('responslambat')}}">
          <rect fill="maroon" stroke="black" stroke-width="20" x="5250" y="5250" width="4500" height="4500" rx="450" ry="450"/>
          <text x="75%" y="65%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="bold" font-size="650px" font-family="Arial">Terlambat</text>
          <text x="75%" y="80%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="800px" font-family="Arial Rounded MT Bold">{{$terlambat}}</text>
        </a>
      </svg>
    </div>
  </div><!-- card card-status -->
</div><!-- col-lg-9 -->



</div><!-- row -->
</div><!-- container -->

@endsection