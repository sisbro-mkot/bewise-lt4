@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Respons Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Respons Risiko Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini2respons.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <label for="s_kd_jabdetail" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail" class="form-control" id="s_kd_jabdetail" autofocus>
            @foreach($s_kd_jabdetail as $key => $value)
              <option value="{{$key}}" {{old('s_kd_jabdetail') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_penyebab" class="col-sm-2 control-label">Nama Penyebab</label>
        <div class="col-sm-12">
          <select name="id_penyebab" class="form-control" id="id_penyebab">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Pemilik Risiko --</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="respon_risiko" class="col-sm-2 control-label">Respons Risiko</label>
        <div class="col-sm-12">
          <select name="respon_risiko" class="form-control" id="respon_risiko">
          <option value="" disabled="true" selected="true"></option>
          <option value="K"> Mengurangi Kemungkinan</option>
          <option value="D"> Mengurangi Dampak</option>
          <option value="B"> Mengurangi Kemungkinan dan Dampak</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="kegiatan_pengendalian" class="col-sm-12 control-label">Nama Kegiatan Pengendalian</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="kegiatan_pengendalian" id="kegiatan_pengendalian">{{old('kegiatan_pengendalian')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="id_sub_unsur" class="col-sm-2 control-label">Sub Unsur SPIP</label>
        <div class="col-sm-12">
          <select name="id_sub_unsur" class="form-control" id="id_sub_unsur" autofocus>
            @foreach($id_sub_unsur as $key => $value)
              <option value="{{$key}}" {{old('id_sub_unsur') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="s_kd_jabdetail_pic" class="col-sm-12 control-label">Penanggung Jawab Kegiatan Pengendalian</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail_pic" class="form-control" id="s_kd_jabdetail_pic" autofocus>
            @foreach($s_kd_jabdetail_pic as $key => $value)
              <option value="{{$key}}" {{old('s_kd_jabdetail_pic') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_output" class="col-sm-2 control-label">Indikator Output</label>
        <div class="col-sm-12">
          <select name="id_output" class="form-control" id="id_output" autofocus>
            @foreach($id_output as $key => $value)
              <option value="{{$key}}" {{old('id_output') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_periode" class="col-sm-2 control-label">Target Waktu</label>
        <div class="col-sm-12">
          <select name="id_periode" class="form-control" id="id_periode">
            @foreach($id_periode as $key => $value)
              <option value="{{$key}}" {{old('id_periode') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>


      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2respons.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#s_kd_jabdetail").select2();
    $("#id_penyebab").select2();
    $("#respon_risiko").select2();
    $("#id_sub_unsur").select2();
    $("#s_kd_jabdetail_pic").select2();
    $("#id_output").select2();
    $("#id_periode").select2();
  
});

</script>
@endpush


@push('js1')
  <script type="text/javascript">
  $(document).ready(function() {
        $('#s_kd_jabdetail').select2().on('change', function() {
            var sebabID = $(this).val();
            console.log(sebabID);
            if(sebabID) {
                $.ajax({
                    url: '/bewise20/lini2respons/pilihSebab/'+sebabID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_penyebab"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_penyebab"]').append('<option value="'+ value.id_penyebab +'">'+ value.nama_akar_penyebab + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="id_penyebab"]').empty();
            }
        });

        $('#id_penyebab').select2()

  });
  </script>
@endpush

