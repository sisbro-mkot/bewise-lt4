@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Respons Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Respons Risiko Unit Kerja </h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
<!--   <div class="card-header">
    <a href="{{url('createrespons')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah</a>
    <a href="{{url('lini2respons')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a>
  </div> -->
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}
    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Nama Penyebab</th>
          <th style="text-align: center;">Respons Risiko</th>
          <th style="text-align: center;">Kegiatan Pengendalian</th>
          <th style="text-align: center;">Klasifikasi Sub Unsur SPIP</th>
          <th style="text-align: center;">Penanggung Jawab Kegiatan</th>
          <th style="text-align: center;">Indikator Output</th>
          <th style="text-align: center;">Target Waktu</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($rtp as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_akar_penyebab}}</td>
          <td>
            @if($item->respon_risiko == "K")
                    Mengurangi Kemungkinan 
            @elseif($item->respon_risiko == "D")
                    Mengurangi Dampak
            @else($item->respon_risiko == "B")
                    Mengurangi Kemungkinan dan Dampak
            @endif
          </td>
          <td>{{$item->kegiatan_pengendalian}}</td>
          <td>{{$item->nama_sub_unsur}}</td>
          <td>{{$item->s_nmjabdetail}}</td>
          <td>{{$item->nama_output}}</td>
          <td style="text-align: center;">{{$item->nama_periode}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
