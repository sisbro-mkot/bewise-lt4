@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Evaluasi Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Evaluasi Risiko Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini2evaluasi.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <label for="s_kd_jabdetail" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail" class="form-control" id="s_kd_jabdetail" autofocus>
            @foreach($s_kd_jabdetail as $key => $value)
              <option value="{{$key}}" {{old('s_kd_jabdetail') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_identifikasi" class="col-sm-2 control-label">Nama Risiko</label>
        <div class="col-sm-12">
          <select name="id_identifikasi" class="form-control" id="id_identifikasi">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Pemilik Risiko --</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_1" class="col-sm-12 control-label">Nama Penyebab (Why 1)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_1" id="nama_penyebab_1">{{old('nama_penyebab_1')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab1ya" onclick="Yes1()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab1tidak" onclick="No1()"> Bukan Akar Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_2" class="col-sm-12 control-label" id="label_nama_penyebab_2">Nama Penyebab (Why 2)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_2" id="nama_penyebab_2">{{old('nama_penyebab_2')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab2ya" onclick="Yes2()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab2tidak" onclick="No2()"> Bukan Akar Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_3" class="col-sm-12 control-label" id="label_nama_penyebab_3">Nama Penyebab (Why 3)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_3" id="nama_penyebab_3">{{old('nama_penyebab_3')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab3ya" onclick="Yes3()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab3tidak" onclick="No3()"> Bukan Akar Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_4" class="col-sm-12 control-label" id="label_nama_penyebab_4">Nama Penyebab (Why 4)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_4" id="nama_penyebab_4">{{old('nama_penyebab_4')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab4ya" onclick="Yes4()"> Akar Penyebab</button>
          <button class="btn btn-danger" id="sebab4tidak" onclick="No4()"> Bukan Akar Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_penyebab_5" class="col-sm-12 control-label" id="label_nama_penyebab_5">Nama Penyebab (Why 5)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_penyebab_5" id="nama_penyebab_5">{{old('nama_penyebab_5')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="sebab5ya" onclick="Yes5()"> Akar Penyebab</button>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_akar_penyebab" class="col-sm-12 control-label" id="label_nama_akar_penyebab">Nama Akar Penyebab</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_akar_penyebab" id="nama_akar_penyebab"></textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="id_jns_sebab" class="col-sm-2 control-label">Jenis Penyebab</label>
        <div class="col-sm-12">
          <select name="id_jns_sebab" class="form-control" id="id_jns_sebab" autofocus>
            @foreach($id_jns_sebab as $key => $value)
              <option value="{{$key}}" {{old('id_jns_sebab') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2evaluasi.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $('button[type!=submit]').click(function(){
        return false;
    });

  $(document).ready(function() {
    $("#s_kd_jabdetail").select2();
    $("#id_identifikasi").select2();
    $("#id_jns_sebab").select2();
    $("#label_nama_penyebab_2").hide();
    $("#nama_penyebab_2").hide();
    $("#label_nama_penyebab_3").hide();
    $("#nama_penyebab_3").hide();
    $("#label_nama_penyebab_4").hide();
    $("#nama_penyebab_4").hide();
    $("#label_nama_penyebab_5").hide();
    $("#nama_penyebab_5").hide();
    $("#label_nama_akar_penyebab").hide();
    $("#nama_akar_penyebab").hide();
    $("#sebab2ya").hide();
    $("#sebab2tidak").hide();
    $("#sebab3ya").hide();
    $("#sebab3tidak").hide();
    $("#sebab4ya").hide();
    $("#sebab4tidak").hide();
    $("#sebab5ya").hide();
  
});

function Yes1() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_1").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab");
            return;
        }
    }

function No1() {
        $("#sebab1ya").hide();
        $("#sebab1tidak").hide();
        $("#label_nama_penyebab_2").show();
        $("#nama_penyebab_2").show();
        $("#sebab2ya").show();
        $("#sebab2tidak").show();
    }

function Yes2() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_2").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab");
            return;
        }
    }

function No2() {
        $("#sebab2ya").hide();
        $("#sebab2tidak").hide();
        $("#label_nama_penyebab_3").show();
        $("#nama_penyebab_3").show();
        $("#sebab3ya").show();
        $("#sebab3tidak").show();
    }

function Yes3() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_3").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab");
            return;
        }
    }

function No3() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        $("#sebab3ya").hide();
        $("#sebab3tidak").hide();
        $("#label_nama_penyebab_4").show();
        $("#nama_penyebab_4").show();
        $("#sebab4ya").show();
        $("#sebab4tidak").show();
    }

function Yes4() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_4").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab");
            return;
        }
    }

function No4() {
        $("#sebab4ya").hide();
        $("#sebab4tidak").hide();
        $("#label_nama_penyebab_5").show();
        $("#nama_penyebab_5").show();
        $("#sebab5ya").show();

    }
function Yes5() {
        $("#label_nama_akar_penyebab").show();
        $("#nama_akar_penyebab").show();
        var input = document.getElementById("nama_penyebab_5").value;
        var text2 = document.getElementById("nama_akar_penyebab");
        text2.value = input;
        if (input.length === 0) {
            alert("Silahkan isi nama penyebab");
            return;
        }
    }

</script>
@endpush


@push('js1')
  <script type="text/javascript">
    $(document).on('click', '#button1', function(){
       $("#nama_akar_penyebab").val($("#nama_penyebab_1").val());
      });


  $(document).ready(function() {
        $('#s_kd_jabdetail').select2().on('change', function() {
            var risikoID = $(this).val();
            console.log(risikoID);
            if(risikoID) {
                $.ajax({
                    url: '/bewise20/lini2evaluasi/pilihRisikoUnit2/'+risikoID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_identifikasi"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_identifikasi"]').append('<option value="'+ value.id_identifikasi +'">'+ value.nama_bagan_risiko + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="id_identifikasi"]').empty();
            }
        });

        $('#id_identifikasi').select2()

  });
  </script>
@endpush

