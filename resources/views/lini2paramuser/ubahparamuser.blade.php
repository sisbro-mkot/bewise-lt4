@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Manajemen Pengguna</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Manajemen Pengguna</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Ubah Data</h6>
  </div>
 

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('simpanparamuser')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <label for="nip" class="col-sm-2 control-label">Nama Pengguna</label>
        <div class="col-sm-12">
          <select name="nip" class="form-control" id="nip" autofocus>
            @foreach($nip as $key)
              <option value="{{$key->niplama}}">{{$key->nipbaru}} - {{$key->s_nama_lengkap}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Peran Pengguna</label>
        <div class="col-sm-12">
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="1">
            <label class="form-check-label">
              Admin BPKP
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="2">
            <label class="form-check-label">
              Pemilik Risiko BPKP
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="3">
            <label class="form-check-label">
              Pengelola Risiko BPKP
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="4">
            <label class="form-check-label">
              Pemilik Risiko Eselon I
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="5">
            <label class="form-check-label">
              Pengelola Risiko Eselon I
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="6">
            <label class="form-check-label">
              Pemilik Risiko Eselon II
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="7">
            <label class="form-check-label">
              Pengelola Risiko Eselon II
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="8">
            <label class="form-check-label">
              Pengawas Intern
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="9">
            <label class="form-check-label">
              Pegawai
            </label>
          </div>
          <div class="form-check">
            <input type="checkbox" name="roles[]" value="10">
            <label class="form-check-label">
              Tester
            </label>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2paramuser.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {

    $("#nip").select2();

});
</script>
@endpush