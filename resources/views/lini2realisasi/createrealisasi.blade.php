@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Respons Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Realisasi Kegiatan Pengendalian Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini2realisasi.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <label for="s_kd_jabdetail" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail" class="form-control" id="s_kd_jabdetail" autofocus>
            @foreach($s_kd_jabdetail as $key => $value)
              <option value="{{$key}}" {{old('s_kd_jabdetail') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_rtp" class="col-sm-12 control-label">Nama Kegiatan Pengendalian</label>
        <div class="col-sm-12">
          <select name="id_rtp" class="form-control" id="id_rtp">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Pemilik Risiko --</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="realisasi_waktu" class="col-sm-2 control-label">Tanggal Realisasi</label>
        <div class="col-sm-2">
          <input type="date" class="form-control" id="realisasi_waktu" name="realisasi_waktu">
        </div>
      </div>

      <div class="form-group">
        <label for="nama_hambatan" class="col-sm-12 control-label">Nama Hambatan</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_hambatan" id="nama_hambatan">{{old('nama_hambatan')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2realisasi.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#s_kd_jabdetail").select2();
    $("#id_rtp").select2();
 
});

</script>
@endpush


@push('js1')
  <script type="text/javascript">
  $(document).ready(function() {
        $('#s_kd_jabdetail').select2().on('change', function() {
            var RTPID = $(this).val();
            console.log(RTPID);
            if(RTPID) {
                $.ajax({
                    url: '/bewise20/lini2realisasi/pilihRTP/'+RTPID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_rtp"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_rtp"]').append('<option value="'+ value.id_rtp +'">'+ value.kegiatan_pengendalian + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="id_rtp"]').empty();
            }


        });

        $('#id_rtp').select2()

  });
  </script>
@endpush

