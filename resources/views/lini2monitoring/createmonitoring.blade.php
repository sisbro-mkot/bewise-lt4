@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Monitoring Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Monitoring Keterjadian Risiko {{$nama_instansiunitorg->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini1monitoring.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <label for="s_kd_jabdetail" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail" class="form-control" id="s_kd_jabdetail" autofocus>
            @foreach($s_kd_jabdetail as $key => $value)
              <option value="{{$key}}" {{old('s_kd_jabdetail') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_kejadian" class="col-sm-12 control-label">Nama Kejadian</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_kejadian" id="nama_kejadian">{{old('nama_kejadian')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="pilihan" class="col-sm-12 control-label" >Apakah kejadian tersebut telah ada dalam register risiko?</label>
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="ya" onclick="Yes1()"> Ya  </button>
          <button class="btn btn-danger" id="tidak" onclick="No1()"> Tidak</button>
        </div>
      </div>

      <div class="form-group" id="identifikasi-1">
        <label for="id_identifikasi" class="col-sm-12 control-label" id="label_id_identifikasi">Nama Risiko</label>
        <div class="col-sm-12">
          <select name="id_identifikasi" class="form-control" id="id_identifikasi">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Nama risiko tidak perlu diisi jika risiko belum teridentifikasi --</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="pilihan" class="col-sm-12 control-label" >Apakah kejadian tersebut telah dianalisis penyebabnya?</label>
        <div class="col-sm-offset-2 col-sm-12">
          <button class="btn btn-primary" id="ya" onclick="Yes2()"> Ya  </button>
          <button class="btn btn-danger" id="tidak" onclick="No2()"> Tidak</button>
        </div>
      </div>

      <div class="form-group" id="penyebab-1">
        <label for="id_penyebab" class="col-sm-12 control-label" id="label_id_penyebab">Nama Penyebab</label>
        <div class="col-sm-12">
          <select name="id_penyebab" class="form-control" id="id_penyebab">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Nama penyebab tidak perlu diisi jika risiko belum teridentifikasi --</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="waktu_kejadian" class="col-sm-2 control-label">Waktu Kejadian</label>
        <div class="col-sm-2">
          <input type="date" class="form-control" id="waktu_kejadian" name="waktu_kejadian">
        </div>
      </div>

      <div class="form-group">
        <label for="tempat_kejadian" class="col-sm-12 control-label">Tempat kejadian</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="tempat_kejadian" id="tempat_kejadian">{{old('tempat_kejadian')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="skor_dampak" class="col-sm-2 control-label">Skor Dampak :</label>
        <div class="col-sm-2">
          <select class="form-control" name="skor_dampak" id="skor_dampak">
            @foreach($skor_dampak as $per)
            <option value="{{$per}}" {{old('skor_dampak') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="pemicu_kejadian" class="col-sm-12 control-label">Pemicu kejadian</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="pemicu_kejadian" id="pemicu_kejadian">{{old('pemicu_kejadian')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2monitoring.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $('button[type!=submit]').click(function(){
      return false;
  });

  $(document).ready(function() {
    $("#s_kd_jabdetail").select2();
    $("#id_identifikasi").select2();
    $("#id_penyebab").select2();
    $("#skor_dampak").select2();

  });

    function Yes1() {
    $("#identifikasi-1").show();
    var RisikoID = $('#s_kd_jabdetail').val();
        console.log(RisikoID);
        if(RisikoID) {
            $.ajax({
                url: '/bewise20/lini2evaluasi/pilihRisikoUnit2/'+RisikoID,
                type: 'get',
                dataType: 'json',
                success:function(data) {
                  console.log(data);
                    $('select[name="id_identifikasi"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="id_identifikasi"]').append('<option value="'+ value.id_identifikasi +'">'+ value.nama_bagan_risiko + '</option>');
                    });

                }
            });
        } else {
            $('select[name="id_identifikasi"]').empty();
        }
    }

    function No1() {
        $("#identifikasi-1").hide();
        $('select[name="id_identifikasi"]').empty();
    }

    function Yes2() {
    $("#penyebab-1").show();
    var SebabID = $('#id_identifikasi').val();
        console.log(SebabID);
        if(SebabID) {
            $.ajax({
                url: '/bewise20/lini2monitoring/pilihSebab2/'+SebabID,
                type: 'get',
                dataType: 'json',
                success:function(data) {
                  console.log(data);
                    $('select[name="id_penyebab"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="id_penyebab"]').append('<option value="'+ value.id_penyebab +'">'+ value.nama_akar_penyebab + '</option>');
                    });

                }
            });
        } else {
            $('select[name="id_penyebab"]').empty();
        }
    }

    function No2() {
        $("#penyebab-1").hide();
        $('select[name="id_penyebab"]').empty();
    }

</script>
@endpush


