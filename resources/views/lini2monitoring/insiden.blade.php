@extends('layout.app')
@section('isi')
<div class="slim-pageheader">
<!--   <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Beranda</li>
  </ol> -->
  <!-- <h6 class="slim-pagetitle">Selamat datang, {{Auth::user()->name}}</h6> -->
  @include('home._greeting')
</div><!-- slim-pageheader -->


@include('dashboard.sidebar')
    
<div class="col-lg-1">
  <br/>
</div><!-- col-lg-1 -->

<div class="col-lg-3">
  <div class="row">
    <div class="col-sm-6">
    </div><!-- col-sm-6 -->
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidenrisiko/1')}}">
      <div class="insiden bencana">
        <center><h3>Bencana</h3></center>
        <br/>
        <center><h2>{{$r1}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-6">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidenrisiko/2')}}">
      <div class="insiden kebijakan">
        <center><h3>Kebijakan</h3></center>
        <br/>
        <center><h2>{{$r2}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-6">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidenrisiko/3')}}">
      <div class="insiden fraud">
        <center><h3>Fraud</h3></center>
        <br/>
        <center><h2>{{$r3}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-6">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidenrisiko/4')}}">
      <div class="insiden kepatuhan">
        <center><h3>Kepatuhan</h3></center>
        <br/>
        <center><h2>{{$r4}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-6">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidenrisiko/5')}}">
      <div class="insiden operasional">
        <center><h3>Operasional</h3></center>
        <br/>
        <center><h2>{{$r5}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-6">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidenrisiko/6')}}">
      <div class="insiden stakeholder">
        <center><h3>Stakeholder</h3></center>
        <br/>
        <center><h2>{{$r6}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
</div><!-- col-lg-3 -->

<div class="col-lg-1">
  <br/>
</div><!-- col-lg-1 -->

<div class="col-lg-3">
  <div class="row">
    <div class="col-sm-1">
    </div><!-- col-sm-1 -->
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidensebab/1')}}">
      <div class="insiden man">
        <center><h3>Man</h3></center>
        <br/>
        <center><h2>{{$p1}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidensebab/2')}}">
      <div class="insiden money">
        <center><h3>Money</h3></center>
        <br/>
        <center><h2>{{$p2}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidensebab/3')}}">
      <div class="insiden method">
        <center><h3>Method</h3></center>
        <br/>
        <center><h2>{{$p3}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidensebab/4')}}">
      <div class="insiden material">
        <center><h3>Material</h3></center>
        <br/>
        <center><h2>{{$p4}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidensebab/5')}}">
      <div class="insiden machine">
        <center><h3>Machine</h3></center>
        <br/>
        <center><h2>{{$p5}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
<br/>
  <div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-6 col-lg-12">
      <a href="{{url('insidensebab/6')}}">
      <div class="insiden external">
        <center><h3>External</h3></center>
        <br/>
        <center><h2>{{$p6}}</h2></center>
      </div><!-- insiden -->
      </a>
    </div><!-- col-sm-6 col-lg-12 -->
  </div><!-- row -->
</div><!-- col-lg-3 -->

</div><!-- row -->
</div><!-- container -->

@endsection