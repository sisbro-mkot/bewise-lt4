@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('insiden')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Monitoring Risiko</li>
  </ol>

  <h6 class="slim-pagetitle">Monitoring Risiko</h6>
</div><!-- slim-pageheader -->

 
<div class="card card-table">

  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display responsive">
      <thead align="center">
        <tr>
          <th width="5%">No.</th>
          <th style="text-align: center;">Nama Kejadian</th>
          <th style="text-align: center;">Kode Risiko</th>
          <th style="text-align: center;">Pemicu Keterjadian</th>
          <th style="text-align: center;">Kode Penyebab</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
      @foreach($monitoring as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_kejadian}}</td>
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->pemicu_kejadian}}</td>
          <td>{{$item->kode_penyebab}}</td>
        </tr>
      @endforeach
        </tbody>
    </table>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "order": [[ 0, "asc" ]],
      responsive: true,
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
