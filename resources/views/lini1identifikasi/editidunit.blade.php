@extends('layout.app')
  
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Identifikasi Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
  </ol>
  <h6 class="slim-pagetitle">Identifikasi Risiko {{$nama_instansiunitorg->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Edit Data</h6>
  </div>


  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1identifikasi.update', $identifikasi->id_identifikasi)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$identifikasi->id_identifikasi}}" id="id_identifikasi" name="id_identifikasi" hidden>
        </div>
      </div>

      <div class="form-group" id="data-umum">
        <label for="tahun" class="col-sm-2 control-label">Tahun</label>
        <div class="col-sm-2">
          <select class="form-control" name="tahun" id="tahun">
            @foreach((array) $tahun as $key)
            <option value="{{$key}}" {{$identifikasi->tahun == $key ? 'selected' : ''}}>{{$key}}</option>
            @endforeach
          </select>
        </div>

        <label for="s_kd_jabdetail" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="s_kd_jabdetail" class="form-control" id="s_kd_jabdetail" autofocus>
            @foreach($s_kd_jabdetail as $key)
              <option value="{{$key->s_kd_jabdetail}}" {{$identifikasi->s_kd_jabdetail == $key->s_kd_jabdetail ? 'selected' : ''}}>{{$key->s_nmjabdetail_pemilik}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_penetapan_konteks" class="col-sm-2 control-label">Konteks</label>
        <div class="col-sm-12">
          <select name="id_penetapan_konteks" class="form-control" id="id_penetapan_konteks" autofocus>
            @foreach($id_penetapan_konteks as $key)
              <option value="{{$key->id_penetapan_konteks}}" {{$identifikasi->id_penetapan_konteks == $key->id_penetapan_konteks ? 'selected' : ''}}>{{$key->nama_konteks}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_bagan_risiko" class="col-sm-2 control-label">Nama Risiko</label>
        <div class="col-sm-12">
          <select name="id_bagan_risiko" class="form-control" id="id_bagan_risiko" autofocus>
            @foreach($id_bagan_risiko as $key => $value)
              <option value="{{$key}}" {{$identifikasi->id_bagan_risiko == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_dampak" class="col-sm-2 control-label">Dampak</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_dampak">{{$identifikasi->nama_dampak}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1identifikasi.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-umum").hide();
    $("#tahun").select2();
    $("#s_kd_jabdetail").select2();
    $("#id_penetapan_konteks").select2();
    $("#id_bagan_risiko").select2();
});
</script>
@endpush

