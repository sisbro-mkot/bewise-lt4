@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Identifikasi Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Identifikasi Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
 
 
<div class="card card-table">
  <div class="card-header">
<!--     <a href="{{url('createtkunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Konteks</a>
    <a href="{{url('lini1tetapkonteks')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data Konteks</a> -->

    <a href="{{url('createidunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    <a href="{{url('lini1identifikasi')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data Identifikasi Risiko</a>

<!--     <a href="{{url('createanunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Analisis</a>
    <a href="{{url('lini1analisis')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data Analisis</a>
    </div> -->
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="" class="table display">
      <thead align="center">
        <tr>
          <th width="10%">Kode Risiko</th>
          <th style="text-align: center;">Pernyataan Risiko</th>
          <th style="text-align: center;">Level Risiko (Residual Risk)</th>
          <th style="text-align: center;">Level Risiko (Treated Risk)</th>
          <th style="text-align: center;">Jumlah Penyebab</th>
          <th style="text-align: center;">Jumlah Kegiatan Pengendalian</th>
          
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($identifikasi as $item)
        <tr class="item{{$item->id}}">
<!--           <td>{{$no++}}</td> -->
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td style="text-align: center;">{{$item->skor_risiko_residual}}</td>
          <td style="text-align: center;">{{$item->skor_risiko_treated}}</td>
          <td style="text-align: center;">{{$item->count_penyebab}}</td>
          <td style="text-align: center;">{{$item->count_rtp}}</td>
         
        </tr>
      @endforeach
        </tbody>
    </table>
    </div>
    </div>
  </div>
</div>

@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('table.display').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
