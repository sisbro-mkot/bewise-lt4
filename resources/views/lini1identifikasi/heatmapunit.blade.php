@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
<!--   <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Beranda</li>
  </ol> -->
  <!-- <h6 class="slim-pagetitle">Selamat datang, {{Auth::user()->name}}</h6> -->
  @include('home._greeting')
</div><!-- slim-pageheader -->

<div class="container">
  <div class="row">
    @include('dashboard.sidebarunit')


<div class="col-lg-9">

  <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;color:#000000}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:2px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:2px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-tllg{background-color:#9aff99;border-style: solid none none solid;border-color:inherit;text-align:center;vertical-align:center;font-size:25px;}
    .tg .tg-trlg{background-color:#9aff99;border-style: solid solid none none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-bllg{background-color:#9aff99;border-style: none none solid solid;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-brlg{background-color:#9aff99;border-style: none solid solid none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-tlgr{background-color:#32cb00;border-style: solid none none solid;border-color:inherit;text-align:center;vertical-align:center;font-size:25px;}
    .tg .tg-trgr{background-color:#32cb00;border-style: solid solid none none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-blgr{background-color:#32cb00;border-style: none none solid solid;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-brgr{background-color:#32cb00;border-style: none solid solid none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-tlyl{background-color:#fcff2f;border-style: solid none none solid;border-color:inherit;text-align:center;vertical-align:center;font-size:25px;}
    .tg .tg-tryl{background-color:#fcff2f;border-style: solid solid none none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-blyl{background-color:#fcff2f;border-style: none none solid solid;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-bryl{background-color:#fcff2f;border-style: none solid solid none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-tlor{background-color:#ffc702;border-style: solid none none solid;border-color:inherit;text-align:center;vertical-align:center;font-size:25px;}
    .tg .tg-tror{background-color:#ffc702;border-style: solid solid none none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-blor{background-color:#ffc702;border-style: none none solid solid;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-bror{background-color:#ffc702;border-style: none solid solid none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-tlrd{background-color:#fd6864;border-style: solid none none solid;border-color:inherit;text-align:center;vertical-align:center;font-size:25px;}
    .tg .tg-trrd{background-color:#fd6864;border-style: solid solid none none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-blrd{background-color:#fd6864;border-style: none none solid solid;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}
    .tg .tg-brrd{background-color:#fd6864;border-style: none solid solid none;border-color:inherit;text-align:center;vertical-align:center;font-size:10px;}

    .tg .tg-vswx{background-color:#fd6864;border-color:inherit;text-align:center;vertical-align:center}
    .tg .tg-yj5y{background-color:white;border-color:inherit;text-align:center;vertical-align:center;}
    .tg .tg-frek{background-color:white;border-color:inherit;text-align:center;vertical-align:center;}
    .tg .tg-fsme{background-color:white;border-color:inherit;text-align:center;vertical-align:middle;}
  </style>

<div class="row">
  <div class="card card-status col-sm-12">
<center>
  <h3 style="color: black;">Peta Risiko</h3>
  <h3 style="color: black;">{{$unit->s_nama_instansiunitorg}}</h3>
  <br/>
  <table class="tg" style="table-layout: fixed; width: 647px; empty-cells: hide">
    <colgroup>
      <col style="width: 75px">
      <col style="width: 32px">
      <col style="width: 60px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
      <col style="width: 32px">
    </colgroup>
    <tr>
      <th class="tg-fsme" colspan="3" rowspan="3">peta risiko</th>
      <th class="tg-yj5y" colspan="15">tingkat dampak</th>
    </tr>
    <tr>
      <td class="tg-yj5y" colspan="3">1</td>
      <td class="tg-yj5y" colspan="3">2</td>
      <td class="tg-yj5y" colspan="3">3</td>
      <td class="tg-yj5y" colspan="3">4</td>
      <td class="tg-yj5y" colspan="3">5</td>
    </tr>
    <tr>
      <td class="tg-yj5y" colspan="3">tidak signifikan</td>
      <td class="tg-yj5y" colspan="3">minor</td>
      <td class="tg-yj5y" colspan="3">moderat</td>
      <td class="tg-yj5y" colspan="3">signifikan</td>
      <td class="tg-yj5y" colspan="3">sangat signifikan</td>
    </tr>
    <tr>
      <td class="tg-frek" rowspan="15">tingkat frekuensi</td>
      <td class="tg-yj5y" rowspan="3">5</td>
      <td class="tg-yj5y" rowspan="3">hampir pasti terjadi</td>
      @if($l9 == 0)
      <td class="tg-tllg" id="s9-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tllg" id="s9-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/9')}}"><span style="background-color:white;color:black;">{{$l9}}</span></a></td>
      @endif
      <td class="tg-trlg" id="s9-2" rowspan="2"></td>
      @if($l15 == 0)
      <td class="tg-tlyl" id="s15-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlyl" id="s15-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/15')}}"><span style="background-color:white;color:black;">{{$l15}}</span></a></td>
      @endif
      <td class="tg-tryl" id="s15-2" rowspan="2"></td>
      @if($l18 == 0)
      <td class="tg-tlor" id="s18-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlor" id="s18-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/18')}}"><span style="background-color:white;color:black;">{{$l18}}</span></a></td>
      @endif
      <td class="tg-tror" id="s18-2" rowspan="2"></td>
      @if($l23 == 0)
      <td class="tg-tlrd" id="s23-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlrd" id="s23-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/23')}}"><span style="background-color:white;color:black;">{{$l23}}</span></a></td>
      @endif
      <td class="tg-trrd" id="s23-2" rowspan="2"></td>
      @if($l25 == 0)
      <td class="tg-tlrd" id="s25-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlrd" id="s25-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/25')}}"><span style="background-color:white;color:black;">{{$l25}}</span></a></td>
      @endif
      <td class="tg-trrd" id="s25-2" rowspan="2"></td>
    </tr>
    <tr>
    </tr>
    <tr>
      <td class="tg-bllg" colspan="2"></td>
      <td class="tg-brlg" id="s9-3">9</td>
      <td class="tg-blyl" colspan="2"></td>
      <td class="tg-bryl" id="s15-3">15</td>
      <td class="tg-blor" colspan="2"></td>
      <td class="tg-bror" id="s18-3">18</td>
      <td class="tg-blrd" colspan="2"></td>
      <td class="tg-brrd" id="s23-3">23</td>
      <td class="tg-blrd" colspan="2"></td>
      <td class="tg-brrd" id="s25-3">25</td>
    </tr>
    <tr>
      <td class="tg-yj5y" rowspan="3">4</td>
      <td class="tg-yj5y" rowspan="3">sering terjadi</td>
      @if($l6 == 0)
      <td class="tg-tlgr" id="s6-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlgr" id="s6-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/6')}}"><span style="background-color:white;color:black;">{{$l6}}</span></a></td>
      @endif
      <td class="tg-trgr" id="s6-2" rowspan="2"></td>
      @if($l12 == 0)
      <td class="tg-tllg" id="s12-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tllg" id="s12-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/12')}}"><span style="background-color:white;color:black;">{{$l12}}</span></a></td>
      @endif
      <td class="tg-trlg" id="s12-2" rowspan="2"></td>
      @if($l16 == 0)
      <td class="tg-tlyl" id="s16-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlyl" id="s16-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/16')}}"><span style="background-color:white;color:black;">{{$l16}}</span></a></td>
      @endif
      <td class="tg-tryl" id="s16-2" rowspan="2"></td>
      @if($l19 == 0)
      <td class="tg-tlor" id="s19-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlor" id="s19-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/19')}}"><span style="background-color:white;color:black;">{{$l19}}</span></a></td>
      @endif
      <td class="tg-tror" id="s19-2" rowspan="2"></td>
      @if($l24 == 0)
      <td class="tg-tlrd" id="s24-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlrd" id="s24-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/24')}}"><span style="background-color:white;color:black;">{{$l24}}</span></a></td>
      @endif
      <td class="tg-trrd" id="s24-2" rowspan="2"></td>
    </tr>
    <tr>
    </tr>
    <tr>
      <td class="tg-blgr" colspan="2"></td>
      <td class="tg-brgr" id="s6-3" >6</td>
      <td class="tg-bllg" colspan="2"></td>
      <td class="tg-brlg" id="s12-3">12</td>
      <td class="tg-blyl" colspan="2"></td>
      <td class="tg-bryl" id="s16-3">16</td>
      <td class="tg-blor" colspan="2"></td>
      <td class="tg-bror" id="s19-3">19</td>
      <td class="tg-blrd" colspan="2"></td>
      <td class="tg-brrd" id="s24-3">24</td>
    </tr>
    <tr>
      <td class="tg-yj5y" rowspan="3">3</td>
      <td class="tg-yj5y" rowspan="3">kadang terjadi</td>
      @if($l4 == 0)
      <td class="tg-tlgr" id="s4-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlgr" id="s4-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/4')}}"><span style="background-color:white;color:black;">{{$l4}}</span></a></td>
      @endif
      <td class="tg-trgr" id="s4-2" rowspan="2"></td>
      @if($l10 == 0)
      <td class="tg-tllg" id="s10-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tllg" id="s10-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/10')}}"><span style="background-color:white;color:black;">{{$l10}}</span></a></td>
      @endif
      <td class="tg-trlg" id="s10-2" rowspan="2"></td>
      @if($l14 == 0)
      <td class="tg-tlyl" id="s14-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlyl" id="s14-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/14')}}"><span style="background-color:white;color:black;">{{$l14}}</span></a></td>
      @endif
      <td class="tg-tryl" id="s14-2" rowspan="2"></td>
      @if($l17 == 0)
      <td class="tg-tlor" id="s17-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlor" id="s17-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/17')}}"><span style="background-color:white;color:black;">{{$l17}}</span></a></td>
      @endif
      <td class="tg-tror" id="s17-2" rowspan="2"></td>
      @if($l22 == 0)
      <td class="tg-tlrd" id="s22-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlrd" id="s22-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/22')}}"><span style="background-color:white;color:black;">{{$l22}}</span></a></td>
      @endif
      <td class="tg-trrd" id="s22-2" rowspan="2"></td>
    </tr>
    <tr>
    </tr>
    <tr>
      <td class="tg-blgr" colspan="2"></td>
      <td class="tg-brgr" id="s4-3">4</td>
      <td class="tg-bllg" colspan="2"></td>
      <td class="tg-brlg" id="s10-3">10</td>
      <td class="tg-blyl" colspan="2"></td>
      <td class="tg-bryl" id="s14-3">14</td>
      <td class="tg-blor" colspan="2"></td>
      <td class="tg-bror" id="s17-3">17</td>
      <td class="tg-blrd" colspan="2"></td>
      <td class="tg-brrd" id="s22-3">22</td>
    </tr>
    <tr>
      <td class="tg-yj5y" rowspan="3">2</td>
      <td class="tg-yj5y" rowspan="3">jarang terjadi</td>
      @if($l2 == 0)
      <td class="tg-tlgr" id="s2-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlgr" id="s2-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/2')}}"><span style="background-color:white;color:black;">{{$l2}}</span></a></td>
      @endif
      <td class="tg-trgr" id="s2-2" rowspan="2"></td>
      @if($l7 == 0)
      <td class="tg-tlgr" id="s7-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlgr" id="s7-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/7')}}"><span style="background-color:white;color:black;">{{$l7}}</span></a></td>
      @endif
      <td class="tg-trgr" id="s7-2" rowspan="2"></td>
      @if($l11 == 0)
      <td class="tg-tllg" id="s11-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tllg" id="s11-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/11')}}"><span style="background-color:white;color:black;">{{$l11}}</span></a></td>
      @endif
      <td class="tg-trlg" id="s11-2" rowspan="2"></td>
      @if($l13 == 0)
      <td class="tg-tlyl" id="s13-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlyl" id="s13-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/13')}}"><span style="background-color:white;color:black;">{{$l13}}</span></a></td>
      @endif
      <td class="tg-tryl" id="s13-2" rowspan="2"></td>
      @if($l21 == 0)
      <td class="tg-tlor" id="s21-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlor" id="s21-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/21')}}"><span style="background-color:white;color:black;">{{$l21}}</span></a></td>
      @endif
      <td class="tg-tror" id="s21-2" rowspan="2"></td>
    </tr>
    <tr>
    </tr>
    <tr>
      <td class="tg-blgr" colspan="2"></td>
      <td class="tg-brgr" id="s2-3">2</td>
      <td class="tg-blgr" colspan="2"></td>
      <td class="tg-brgr" id="s7-3">7</td>
      <td class="tg-bllg" colspan="2"></td>
      <td class="tg-brlg" id="s11-3">11</td>
      <td class="tg-blyl" colspan="2"></td>
      <td class="tg-bryl" id="s13-3">13</td>
      <td class="tg-blor" colspan="2"></td>
      <td class="tg-bror" id="s21-3">21</td>
    </tr>
    <tr>
      <td class="tg-yj5y" rowspan="3">1</td>
      <td class="tg-yj5y" rowspan="3">hampir tidak terjadi</td>
      @if($l1 == 0)
      <td class="tg-tlgr" id="s1-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlgr" id="s1-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/1')}}"><span style="background-color:white;color:black;">{{$l1}}</span></a></td>
      @endif
      <td class="tg-trgr" id="s1-2" rowspan="2"></td>
      @if($l3 == 0)
      <td class="tg-tlgr" id="s3-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlgr" id="s3-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/3')}}"><span style="background-color:white;color:black;">{{$l3}}</span></a></td>
      @endif
      <td class="tg-trgr" id="s3-2" rowspan="2"></td>
      @if($l5 == 0)
      <td class="tg-tlgr" id="s5-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlgr" id="s5-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/5')}}"><span style="background-color:white;color:black;">{{$l5}}</span></a></td>
      @endif
      <td class="tg-trgr" id="s5-2" rowspan="2"></td>
      @if($l8 == 0)
      <td class="tg-tllg" id="s8-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tllg" id="s8-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/8')}}"><span style="background-color:white;color:black;">{{$l8}}</span></a></td>
      @endif
      <td class="tg-trlg" id="s8-2" rowspan="2"></td>
      @if($l20 == 0)
      <td class="tg-tlor" id="s20-1" colspan="2" rowspan="2"></td>
      @else
      <td class="tg-tlor" id="s20-1" colspan="2" rowspan="2"><a href="{{url('dashboardregisterunit/20')}}"><span style="background-color:white;color:black;">{{$l20}}</span></a></td>
      @endif
      <td class="tg-tror" id="s20-2" rowspan="2"></td>
    </tr>
    <tr>
    </tr>
    <tr>
      <td class="tg-blgr" colspan="2"></td>
      <td class="tg-brgr" id="s1-3">1</td>
      <td class="tg-blgr" colspan="2"></td>
      <td class="tg-brgr" id="s3-3">3</td>
      <td class="tg-blgr" colspan="2"></td>
      <td class="tg-brgr" id="s5-3">5</td>
      <td class="tg-bllg" colspan="2"></td>
      <td class="tg-brlg" id="s8-3">8</td>
      <td class="tg-blor" colspan="2"></td>
      <td class="tg-bror" id="s20-3">20</td>
    </tr>
  </table>
  <input class="form-control" value="{{$selera->skor_selera}}" name="skor_selera" id="skor_selera" hidden></input>
</center>
</div><!-- col-sm-12 -->
</div><!-- row -->

</div><!-- col-lg-9 -->
</div><!-- row -->
</div><!-- container -->

@endsection

@push('js')

<script type="text/javascript">
  $(document).ready(function() {
    var seleraID = $('#skor_selera').val();
    console.log(seleraID);
    if (seleraID == 1){
      $("#s1-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s1-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s1-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 2){
      $("#s2-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s2-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s2-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s1-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s1-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 4){
      $("#s2-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s2-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s2-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s3-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s3-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s3-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 4){
      $("#s2-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s2-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s3-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s3-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s3-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s4-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s4-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s4-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 5){
      $("#s2-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s2-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s3-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s3-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s4-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s4-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s4-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s5-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s5-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s5-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 6){
      $("#s2-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s2-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s3-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s3-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s4-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s4-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s5-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s5-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s5-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s6-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s6-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s6-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 7){
      $("#s4-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s4-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s5-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s5-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s5-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s6-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s6-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s6-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s7-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s7-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s7-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 8){
      $("#s4-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s4-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s5-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s5-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s6-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s6-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s6-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s7-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s7-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s7-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s8-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 9){
      $("#s4-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s4-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s5-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s5-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s6-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s6-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s7-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s7-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s7-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s8-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 10){
      $("#s5-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s5-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s6-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s6-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s7-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s7-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s8-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s10-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s10-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s10-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 11){
      $("#s6-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s6-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s8-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s10-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s10-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s10-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s11-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s11-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s11-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 12){
      $("#s8-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s8-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s10-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s10-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s11-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s11-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s11-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s12-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s12-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s12-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 13){
      $("#s8-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s10-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s10-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s11-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s11-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s12-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s12-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s12-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s13-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 14){
      $("#s8-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s9-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s12-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s12-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s12-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s13-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s14-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s14-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s14-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 15){
      $("#s8-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s12-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s12-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s13-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s14-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s14-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s14-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s15-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s15-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 16){
      $("#s8-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s13-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s14-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s14-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s15-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s15-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s16-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s16-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s16-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 17){
      $("#s8-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s15-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s15-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s16-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s16-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s16-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s17-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s17-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s17-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 18){
      $("#s8-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s16-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s16-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s17-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s17-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s17-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 19){
      $("#s8-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s8-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s17-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s17-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s19-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s19-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s19-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 20){
      $("#s13-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s13-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s17-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s17-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s19-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s19-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s19-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s20-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s20-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
    } else if (seleraID == 21){
      $("#s17-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s17-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s19-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s19-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s19-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s21-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s21-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
    } else if (seleraID == 22){
      $("#s18-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s18-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s19-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s19-2").css( {borderWidth: '7px 7px', borderColor: 'red'} );
      $("#s19-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s22-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s22-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
    } else if (seleraID == 23){
      $("#s19-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s19-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s22-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s22-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s23-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s23-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
    } else if (seleraID == 24){
      $("#s23-2").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s23-3").css( {borderWidth: '2px 7px', borderColor: 'grey red'} );
      $("#s24-1").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
      $("#s24-2").css( {borderWidth: '7px 2px', borderColor: 'red grey'} );
    } else {
    
    };
    
});
</script>

@endpush